package com.zipr.clg.util

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import com.google.android.gms.location.places.ui.PlacePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import com.zipr.clg.R
import com.zipr.clg.data.entity.Location

fun Fragment.startImageChooser(requestCode: Int) {
    Dexter.withActivity(activity)
        .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        .withListener(object : PermissionListener {

            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                val intent = Intent(Intent.ACTION_GET_CONTENT).setType("image/*")
                startActivityForResult(Intent.createChooser(intent, "Select an image"), requestCode)
            }

            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                AlertDialog.Builder(context)
                    .setTitle("Storage permission")
                    .setMessage("Storage permission is needed to select an image")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        token?.continuePermissionRequest()
                    }
                    .show()
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                if (response?.isPermanentlyDenied == true) {
                    SnackbarOnDeniedPermissionListener.Builder
                        .with(view, "Storage permission is needed to select an image")
                        .withOpenSettingsButton(R.string.settings)
                        .build().onPermissionDenied(response)
                }
            }

        })
        .check()
}

fun Fragment.startVideoChooser(requestCode: Int) {
    Dexter.withActivity(activity)
        .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        .withListener(object : PermissionListener {

            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                val intent = Intent(Intent.ACTION_GET_CONTENT).setType("video/*")
                startActivityForResult(Intent.createChooser(intent, "Select a video"), requestCode)
            }

            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                AlertDialog.Builder(context)
                    .setTitle("Storage permission")
                    .setMessage("Storage permission is needed to select a video")
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        token?.continuePermissionRequest()
                    }
                    .show()
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                SnackbarOnDeniedPermissionListener.Builder
                    .with(view, "Storage permission is needed to select a video")
                    .withOpenSettingsButton(R.string.settings)
                    .build().onPermissionDenied(response)
            }

        })
        .check()
}

fun Fragment.startLocationChooser(requestCode: Int) {
    val intent = PlacePicker.IntentBuilder().build(activity)
    startActivityForResult(intent, requestCode)
}

fun Fragment.openLocation(location: Location) {
    val intent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("geo:0,0?q=${location.geoPoint?.latitude},${location.geoPoint?.longitude}(${location.name}, ${location.address})")
    )
    startActivity(intent)
}