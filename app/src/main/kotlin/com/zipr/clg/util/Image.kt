package com.zipr.clg.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ThumbnailUtils
import android.net.Uri
import android.support.media.ExifInterface
import com.facebook.imagepipeline.image.ImageInfo

const val DEFAULT_ASPECT_RATIO = 1.777f

fun Bitmap.toOriginalOrientation(context: Context, bitmapUri: Uri): Bitmap {
    context.contentResolver.openInputStream(bitmapUri).use {
        val ei = ExifInterface(it)
        val orientation =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
    
        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate(270f)
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flip(true, false)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> flip(false, true)
            else -> this
        }
    }
}

fun Bitmap.rotate(degrees: Float): Bitmap {
    val matrix = Matrix().apply { postRotate(degrees) }
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun Bitmap.flip(horizontal: Boolean, vertical: Boolean): Bitmap {
    val matrix = Matrix().apply {
        preScale(
            (if (horizontal) -1 else 1).toFloat(),
            (if (vertical) -1 else 1).toFloat()
        )
    }
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun Bitmap.center(width: Int, height: Int): Bitmap {
    return ThumbnailUtils.extractThumbnail(
        this,
        height,
        width,
        ThumbnailUtils.OPTIONS_RECYCLE_INPUT
    )
}

val ImageInfo.aspectRatio get() = width.toFloat() / height
