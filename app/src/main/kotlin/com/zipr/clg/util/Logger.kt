package com.zipr.clg.util

import com.orhanobut.logger.Logger

fun debugLog(message: Any?) = Logger.d(message)