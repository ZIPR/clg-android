package com.zipr.clg.util

import android.view.Menu
import android.view.MenuItem

fun Menu.getByTitle(title: String): MenuItem? {
    for (i in 0 until size()) {
        val item = getItem(i)
        if (item.title == title) {
            return item
        }
    }
    return null
}