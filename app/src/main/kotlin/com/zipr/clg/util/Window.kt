package com.zipr.clg.util

import android.view.Window
import android.view.WindowManager

fun Window.setDefaultSoftInputMode() =
    this.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)