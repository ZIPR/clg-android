package com.zipr.clg.util

import android.content.res.Resources

private const val FETCH_FACTOR = 4

fun Int.toPx() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Int.toDp() = (this / Resources.getSystem().displayMetrics.density).toInt()

fun calculateFetchLimit(itemViewHeightDp: Int) =
    Resources.getSystem().displayMetrics.heightPixels.toDp().let {
        (FETCH_FACTOR * it / itemViewHeightDp).toLong()
    }
