package com.zipr.clg.util

import android.content.Context
import android.content.res.ColorStateList
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import com.zipr.clg.ClgApplication
import com.zipr.clg.R

private const val VALIDATION_TOAST_OFFSET_DP = 120

fun showToast(context: Context?, message: String?, configuration: (Toast.() -> Unit)? = null) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).apply {
        configuration?.invoke(this)
    }.show()
}

fun showValidationToast(context: Context?, message: String) {
    showToast(context, message) {
        setGravity(
            Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM,
            0,
            VALIDATION_TOAST_OFFSET_DP.toPx()
        )
        view?.backgroundTintList = ColorStateList.valueOf(
            ClgApplication.getColor(R.color.colorAccent)
        )
        view?.findViewById<TextView>(android.R.id.message)?.setTextColor(
            ClgApplication.getColor(android.R.color.white)
        )
    }
}
