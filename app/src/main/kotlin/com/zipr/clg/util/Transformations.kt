package com.zipr.clg.util

inline fun <reified T, reified S> letTyped(crossinline block: (S) -> T?): (Any?) -> T? = {
    (it as? S)?.let(block)
}
