package com.zipr.clg.util

import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView

fun RecyclerView?.runOrPostIfComputingLayout(action: () -> Unit) {
    if (this?.isComputingLayout != true) action()
    else handler?.post(action) ?: Handler(Looper.getMainLooper()).post(action)
}
