package com.zipr.clg.util

fun Int?.orZero() = this ?: 0

fun Long?.orZero() = this ?: 0