package com.zipr.clg.util

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

val firebaseAuth get() = FirebaseAuth.getInstance()!!
val firebaseUser get() = firebaseAuth.currentUser

private const val PROVIDER_GOOGLE = "google.com"
private const val PROVIDER_FACEBOOK = "facebook.com"
private const val DEFAULT_PHOTO_SIZE_DP = 96

val FirebaseUser.defaultPhotoUrl: String
    get() {
        if (photoUrl == null) {
            return ""
        }
        
        val photoUrlString = photoUrl.toString()
        val photoSize = DEFAULT_PHOTO_SIZE_DP.toPx()
        
        return when (providerId) {
            PROVIDER_GOOGLE -> photoUrlString.replace(
                "/s96-c/",
                "/s$photoSize-c/"
            )
            
            PROVIDER_FACEBOOK -> "https://graph.facebook.com/" + providerData[1].uid +
                    "/image?height=$photoSize&width=$photoSize"
            
            else -> photoUrlString
        }
    }
