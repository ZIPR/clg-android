package com.zipr.clg.util

inline operator fun <K, V, reified T> Map<K, V?>?.invoke(key: K) =
    this?.get(key) as? T

inline operator fun <K, V, reified S, T> Map<K, V?>?.invoke(key: K, transform: (S) -> T): T? =
    (this?.get(key) as? S)?.let(transform)

inline operator fun <K, V, reified T> Map<K, V?>?.invoke(key: K, defaultValue: V) =
    this?.getOrDefault(key, defaultValue) as? T

fun <T, V> Map<T, V>.mapEquals(map: Map<T, V>) =
    size == map.size && all { it.value == map[it.key] }
