package com.zipr.clg.util

import android.support.design.widget.TextInputEditText

fun TextInputEditText.trimTextAndSelection() {
    var currentSelectionStart = selectionStart
    var currentSelectionEnd = selectionEnd
    
    text.trimStart().also {
        if (it.length < text.length) {
            currentSelectionStart -= text.length - it.length
            currentSelectionEnd -= text.length - it.length
            if (currentSelectionStart < 0) currentSelectionStart = 0
            if (currentSelectionEnd < 0) currentSelectionEnd = 0
            setText(it)
        }
    }
    
    text.trimEnd().also {
        if (it.length < text.length) {
            if (currentSelectionStart > it.length) currentSelectionStart = it.length
            if (currentSelectionEnd > it.length) currentSelectionEnd = it.length
            setText(it)
        }
    }
    
    setSelection(currentSelectionStart, currentSelectionEnd)
}

fun StringBuilder.set(str: String?) {
    setLength(0)
    append(str)
}

fun StringBuilder.set(str: CharSequence?) {
    setLength(0)
    append(str)
}
