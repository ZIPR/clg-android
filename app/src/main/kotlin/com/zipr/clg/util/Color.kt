package com.zipr.clg.util

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.support.annotation.ColorInt
import android.support.v7.graphics.Palette

private const val FACTOR_DARK = 0.8f

@ColorInt
fun darkenColor(@ColorInt color: Int): Int {
    val hsv = color.toHSV()
    hsv[2] *= FACTOR_DARK
    return Color.HSVToColor(hsv)
}

fun Int.toHSV(): FloatArray {
    val hsv = FloatArray(3)
    Color.colorToHSV(this, hsv)
    return hsv
}

fun Drawable.overrideColor(@ColorInt colorInt: Int) {
    when (this) {
        is GradientDrawable -> setColor(colorInt)
        is ShapeDrawable -> paint.color = colorInt
        is ColorDrawable -> color = colorInt
    }
}

val Palette.vibrantOrMutedSwatch get() = run {
    vibrantSwatch ?:
    darkVibrantSwatch ?:
    mutedSwatch ?:
    darkMutedSwatch ?:
    lightMutedSwatch ?:
    lightVibrantSwatch ?:
    dominantSwatch
}
