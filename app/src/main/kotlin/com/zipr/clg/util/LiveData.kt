package com.zipr.clg.util

import android.arch.lifecycle.*
import android.os.Handler
import android.os.Looper

fun <S, T> LiveData<S>.map(func: (S?) -> T?): LiveData<T> =
    Transformations.map(this, func)

fun <S, T> LiveData<S>.switchMap(func: (S?) -> LiveData<T>?): LiveData<T> =
    Transformations.switchMap(this, func)

fun <S> LiveData<S>.observeSingleEvent(onEvent: (S?) -> Unit) =
    SingleEventObserver(null, this, onEvent)

fun <S> LiveData<S>.observeSingleEvent(lifecycleOwner: LifecycleOwner, onEvent: (S?) -> Unit) =
    SingleEventObserver(lifecycleOwner, this, onEvent)

fun <S> LiveData<S>.observeSingleEventNonNull(onEvent: (S) -> Unit) =
    SingleNonNullEventObserver(null, this, onEvent)

fun <S> LiveData<S>.observeSingleEventNonNull(lifecycleOwner: LifecycleOwner, onEvent: (S) -> Unit) =
    SingleNonNullEventObserver(lifecycleOwner, this, onEvent)

inline fun <S> LiveData<S>.observeNonNull(owner: LifecycleOwner, crossinline observer: (S) -> Unit) = observe(
    owner, Observer {
        it?.also(observer)
    }
)

inline fun <S> LiveData<S>.observeForeverNonNull(crossinline observer: (S) -> Unit) = observeForever({
    it?.also(observer)
})

class SingleEventObserver<T, L: LiveData<T>>(
    lifecycleOwner: LifecycleOwner?,
    val liveData: L,
    private val onEvent: (T?) -> Unit
) {

    var isDone = false
        private set
    
    private val observer = Observer<T> {
        cancel()
        onEvent(it)
    }
    
    init {
        lifecycleOwner?.also {
            liveData.observe(it, observer)
        } ?: apply {
            liveData.observeForever(observer)
        }
    }
    
    fun cancel(): Boolean = (!isDone).also {
        if (it) {
            isDone = true
            liveData.removeObserver(observer)
        }
    }

}

class SingleNonNullEventObserver<T, L: LiveData<T>>(
    lifecycleOwner: LifecycleOwner?,
    val liveData: L,
    private val onEvent: (T) -> Unit
) {

    var isDone = false
        private set

    private val observer = Observer<T> {
        it?.also {
            cancel()
            onEvent(it)
        }
    }

    init {
        lifecycleOwner?.also {
            liveData.observe(it, observer)
        } ?: apply {
            liveData.observeForever(observer)
        }
    }

    fun cancel(): Boolean = (!isDone).also {
        if (it) {
            isDone = true
            liveData.removeObserver(observer)
        }
    }

}

class BackpressureData<T>(
    private val timePeriodMillis: Long
) {

    private val observers = mutableMapOf<LifecycleOwner, MutableSet<Observer<T>>>()

    private var value: T? = null

    private val handler = Handler(Looper.getMainLooper())
    private val setValueAction = Runnable {
        observers.values.forEach {
            it.forEach {
                it.onChanged(value)
            }
        }
    }

    fun observe(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
        if (lifecycleOwner.lifecycle.currentState != Lifecycle.State.DESTROYED) {
            observers.getOrPut(lifecycleOwner, {
                lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {

                    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
                    fun removeObservers() {
                       observers.remove(lifecycleOwner)
                    }

                })
                mutableSetOf()
            }).add(observer)
        }
    }

    fun postValue(value: T?) {
        this.value = value
        handler.cancelAndPostDelayed(setValueAction, timePeriodMillis)
    }

}