package com.zipr.clg.util

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentManager.BackStackEntry

val FragmentManager.topFragment: BackStackEntry? get() =
    if (backStackEntryCount > 0) getBackStackEntryAt(backStackEntryCount - 1) else null

fun FragmentManager.clearBackStack() {
    for (i in 0 until backStackEntryCount) {
        popBackStack()
    }
}