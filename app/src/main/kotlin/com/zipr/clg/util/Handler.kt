package com.zipr.clg.util

import android.os.Handler

fun Handler.cancelAndPost(action: Runnable) {
    removeCallbacksAndMessages(null)
    post(action)
}

fun Handler.cancelAndPostDelayed(action: Runnable, delayMillis: Long) {
    removeCallbacksAndMessages(null)
    postDelayed(action, delayMillis)
}