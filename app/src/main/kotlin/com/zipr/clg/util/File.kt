package com.zipr.clg.util

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.BaseColumns
import android.provider.DocumentsContract
import android.provider.MediaStore

private const val AUTHORITY_EXTERNAL_STORAGE = "com.android.externalstorage.documents"
private const val AUTHORITY_DOWNLOADS = "com.android.providers.downloads.documents"
private const val AUTHORITY_MEDIA = "com.android.providers.media.documents"
private const val AUTHORITY_GOOGLE_PHOTOS = "com.google.android.apps.photos.content"
private const val MEDIA_TYPE_IMAGE = "image"
private const val MEDIA_TYPE_VIDEO = "video"
private const val MEDIA_TYPE_AUDIO = "audio"
private const val URI_TYPE_CONTENT = "content"
private const val URI_TYPE_FILE = "file"
private const val URI_DOWNLOADS = "content://downloads/public_downloads"
private const val STORAGE_TYPE_PRIMARY = "primary"
private const val DELIMITER_DOCUMENT_TYPE = ":"

private fun isExternalStorageDocument(uri: Uri) = uri.authority == AUTHORITY_EXTERNAL_STORAGE
private fun isDownloadsDocument(uri: Uri) = uri.authority == AUTHORITY_DOWNLOADS
private fun isMediaDocument(uri: Uri) = uri.authority == AUTHORITY_MEDIA
private fun isGooglePhotosUri(uri: Uri) = uri.authority == AUTHORITY_GOOGLE_PHOTOS

private fun getDataColumn(
    context: Context,
    uri: Uri,
    selection: String?,
    selectionArgs: Array<String>?
) = context.contentResolver.query(
    uri,
    arrayOf(MediaStore.MediaColumns.DATA),
    selection,
    selectionArgs,
    null
).use {
    if (it?.moveToFirst() == true) it.getString(it.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA))
    else null
}

fun Uri.getContentPath(context: Context) = when {
    DocumentsContract.isDocumentUri(context, this) -> {
        val docId = DocumentsContract.getDocumentId(this)
        when {
            isExternalStorageDocument(this) -> {
                val split = docId.split(DELIMITER_DOCUMENT_TYPE)
                val type = split[0]
                if (STORAGE_TYPE_PRIMARY.equals(type, ignoreCase = true)) {
                    Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                } else {
                    null
                }
            }
            
            isDownloadsDocument(this) -> getDataColumn(
                context,
                ContentUris.withAppendedId(
                    Uri.parse(URI_DOWNLOADS),
                    docId.substringAfter(DELIMITER_DOCUMENT_TYPE).toLong()
                ),
                null,
                null
            )
            
            isMediaDocument(this) -> {
                val split = docId.split(DELIMITER_DOCUMENT_TYPE)
                val type = split[0]
                val contentUri = when (type) {
                    MEDIA_TYPE_IMAGE -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    MEDIA_TYPE_VIDEO -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    MEDIA_TYPE_AUDIO -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    else -> throw IllegalArgumentException()
                }
                getDataColumn(context, contentUri, BaseColumns._ID + "=?", arrayOf(split[1]))
            }
            
            else -> null
        }
    }
    
    URI_TYPE_CONTENT.equals(scheme, ignoreCase = true) ->
        if (isGooglePhotosUri(this)) lastPathSegment
        else getDataColumn(context, this, null, null)
    
    URI_TYPE_FILE.equals(scheme, ignoreCase = true) -> path
    
    else -> null
}?.let {
    Uri.Builder()
        .scheme("file")
        .appendEncodedPath(it)
        .build()
} ?: this
