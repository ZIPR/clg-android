package com.zipr.clg.data.entity

import com.zipr.clg.util.invoke

data class ChallengeParticipating(
    val challenge: Challenge?,
    val participating: Participating?
) {
    
    constructor(map: DataMap): this(
        map(ChallengeParticipating::challenge.name, ::Challenge),
        map(ChallengeParticipating::participating.name, ::Participating)
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        challenge?.also { dataMap[ChallengeParticipating::challenge.name] = it.toDataMap() }
        participating?.also { dataMap[ChallengeParticipating::participating.name] = it.toDataMap() }
        return dataMap
    }
    
}