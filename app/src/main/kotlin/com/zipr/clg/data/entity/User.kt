package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.zipr.clg.util.invoke

// TODO: make photo url as Image

@IgnoreExtraProperties
data class User(
    val username: String? = null,
    val fullName: String? = null,
    val bio: String? = null,
    val photoUrl: String? = null,
    val followersCount: Long? = null,
    val followingCount: Long? = null,
    @get:Exclude val id: String? = null
) {

    constructor(map: DataMap): this(
        map(User::username.name),
        map(User::fullName.name),
        map(User::bio.name),
        map(User::photoUrl.name),
        map(User::followersCount.name),
        map(User::followingCount.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        username?.also { dataMap[User::username.name] = it }
        fullName?.also { dataMap[User::fullName.name] = it }
        bio?.also { dataMap[User::bio.name] = it }
        photoUrl?.also { dataMap[User::photoUrl.name] = it }
        followersCount?.also { dataMap[User::followersCount.name] = it }
        followingCount?.also { dataMap[User::followingCount.name] = it }
        return dataMap
    }

}