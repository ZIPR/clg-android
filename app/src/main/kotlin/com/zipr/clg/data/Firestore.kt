package com.zipr.clg.data

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import java.util.Random

val firestore get() = FirebaseFirestore.getInstance()

private const val AUTO_ID_LENGTH = 20
private const val AUTO_ID_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
private const val AUTO_ID_ALPHABET_LENGTH = AUTO_ID_ALPHABET.length

fun documentAutoId() = StringBuilder().apply {
    val random = Random()
    for (i in 0 until AUTO_ID_LENGTH) {
        append(AUTO_ID_ALPHABET[random.nextInt(AUTO_ID_ALPHABET_LENGTH)])
    }
}.toString()

fun getFirestoreNotFoundMessage(path: String) = "Not found: $path"
fun getFirestoreUnavailableMessage() = "Firestore is unavailable"

val FirebaseFirestore.users: UsersRef get () = UsersRef(collection("users"))
val FirebaseFirestore.challenges: ChallengesRef get() = ChallengesRef(collection("challenges"))

private val DocumentReference.followers: CollectionReference get() = collection("followers")
private val DocumentReference.following: CollectionReference get() = collection("following")
private val DocumentReference.votedPosts: CollectionReference get() = collection("votedPosts")
private val DocumentReference.participants: CollectionReference get() = collection("participants")
private val DocumentReference.participating: CollectionReference get() = collection("participating")
private val DocumentReference.posts: CollectionReference get() = collection("posts")
private val DocumentReference.voters: CollectionReference get() = collection("voters")
private val DocumentReference.comments: CollectionReference get() = collection("comments")

class UsersRef(private val usersReference: CollectionReference) {

    fun query() = usersReference as Query

    fun user(userId: String) = usersReference.document(userId)

    fun followers(userId: String) = FollowersRef(this, userId)

    fun following(userId: String) = FollowingRef(this, userId)

    fun participating(userId: String) = ParticipatingRef(this, userId)

    fun posts(userId: String) = PostsRef(this, userId)

    fun votedPosts(userId: String) = VotedPostsRef(this, userId)

    class FollowersRef(usersReference: UsersRef, id: String) {

        private val followersReference = usersReference.user(id).followers

        fun query() = followersReference as Query

        fun user(userId: String) = followersReference.document(userId)

    }

    class FollowingRef(usersReference: UsersRef, id: String) {

        private val followingReference = usersReference.user(id).following

        fun query() = followingReference as Query

        fun user(userId: String) = followingReference.document(userId)

    }

    class ParticipatingRef(usersReference: UsersRef, id: String) {

        private val participatingReference = usersReference.user(id).participating

        fun query() = participatingReference as Query

        fun challenge(challengeId: String) = participatingReference.document(challengeId)

    }

    class PostsRef(usersReference: UsersRef, id: String) {

        private val postsReference = usersReference.user(id).posts

        fun query() = postsReference as Query

        fun post(postId: String) = postsReference.document(postId)

    }

    class VotedPostsRef(usersReference: UsersRef, id: String) {

        private val votedPostsReference = usersReference.user(id).votedPosts

        fun query() = votedPostsReference as Query

        fun post(postId: String) = votedPostsReference.document(postId)

    }

}

class ChallengesRef(private val challengesReference: CollectionReference) {

    fun query() = challengesReference as Query

    fun challenge(challengeId: String) = challengesReference.document(challengeId)

    fun participants(challengeId: String) = ParticipantsRef(this, challengeId)

    fun posts(challengeId: String) = PostsRef(this, challengeId)

    class ParticipantsRef(challengeReference: ChallengesRef, challengeId: String) {

        private val participantsReference = challengeReference.challenge(challengeId).participants

        fun query() = participantsReference as Query

        fun user(participantId: String) = participantsReference.document(participantId)

    }

    class PostsRef(challengeReference: ChallengesRef, id: String) {

        private val postsReference = challengeReference.challenge(id).posts

        fun query() = postsReference as Query

        fun post(postId: String) = postsReference.document(postId)

        fun voters(postId: String) = VotersRef(this, postId)

        fun comments(postId: String) = CommentsRef(this, postId)

        class VotersRef(postReference: PostsRef, id: String) {

            private val votersReference = postReference.post(id).voters

            fun query() = votersReference as Query

            fun voter(voterId: String) = votersReference.document(voterId)

        }

        class CommentsRef(postReference: PostsRef, id: String) {

            private val commentsReference = postReference.post(id).comments

            fun query() = commentsReference as Query

            fun comment(commentId: String) = commentsReference.document(commentId)

        }

    }

}