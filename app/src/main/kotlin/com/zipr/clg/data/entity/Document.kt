package com.zipr.clg.data.entity

import com.zipr.clg.data.DataException

data class Document<out T>(
    val data: T? = null,
    val metadata: Metadata? = null,
    val exception: DataException?
) {

    fun <S> map(func: (T) -> S) = clone(if (data != null) func(data) else null)

    fun <S> clone(
        data: S? = null,
        metadata: Metadata? = this.metadata,
        exception: DataException? = this.exception
    ) = Document(data, metadata, exception)

    data class Metadata(val isFromCache: Boolean, val hasPendingWrites: Boolean)

}