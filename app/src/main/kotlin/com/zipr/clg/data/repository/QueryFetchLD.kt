package com.zipr.clg.data.repository

import com.google.firebase.firestore.*
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.DataMap
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.id
import com.zipr.clg.data.getFirestoreNotFoundMessage
import com.zipr.clg.data.getFirestoreUnavailableMessage

class QueryFetchLD(
    private val query: Query,
    private val isRealtime: Boolean,
    private val includeMetadataChanges: Boolean = false
) : FetchLD<List<DataMap>, QuerySnapshot>() {

    private lateinit var listenerRegistration: ListenerRegistration
    private var currentSnapshot: QuerySnapshot? = null

    override val snapshotListener =
        EventListener<QuerySnapshot> { snapshot: QuerySnapshot?, exception: FirebaseFirestoreException? ->
            if (currentSnapshot == null || currentSnapshot != snapshot) {
                currentSnapshot = snapshot
            } else {
                return@EventListener
            }

            value = snapshot.let {
                val data = it?.documents?.mapNotNull { it.data?.apply { id = it.id } }
                val metadata = it?.metadata?.let { Document.Metadata(it.isFromCache, it.hasPendingWrites()) }
                if (exception == null && data?.isNotEmpty() == true) {
                    Document(data, metadata, null)
                } else {
                    Document(
                        null,
                        null,
                        when {
                            exception != null -> DataException.fromFirebaseFirestoreException(exception)

                            metadata?.isFromCache == true -> DataException(
                                DataException.Type.UNAVAILABLE,
                                getFirestoreUnavailableMessage()
                            )

                            else -> DataException(
                                DataException.Type.NOT_FOUND,
                                getFirestoreNotFoundMessage(query.toString())
                            )
                        }
                    )
                }
            }
        }

    init {
        if (!isRealtime) {
            observeFetchTask(query.get())
        }
    }

    override fun onActive() {
        if (isRealtime) {
            listenerRegistration = query.addSnapshotListener(
                if (includeMetadataChanges) MetadataChanges.INCLUDE else MetadataChanges.EXCLUDE,
                snapshotListener
            )
        }
    }

    override fun onInactive() {
        if (isRealtime) {
            listenerRegistration.remove()
        }
    }

}