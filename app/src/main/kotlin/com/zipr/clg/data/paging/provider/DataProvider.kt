package com.zipr.clg.data.paging.provider

import com.zipr.clg.data.DataException

interface DataProvider<out E> {

    fun getItemCount(): Int

    fun getItem(position: Int): E

    fun retryFetching()

    fun refreshData()

    fun registerDataListener(dataListener: DataProvider.DataListener)

    fun registerErrorListener(errorListener: DataProvider.ErrorListener)

    fun registerStatusListener(statusListener: DataProvider.StatusListener)

    fun unregisterDataListener(dataListener: DataProvider.DataListener)

    fun unregisterErrorListener(errorListener: DataProvider.ErrorListener)

    fun unregisterStatusListener(statusListener: DataProvider.StatusListener)

    interface DataListener {

        fun onDataInserted(positionStart: Int, itemCount: Int)

        fun onDataRemoved(positionStart: Int, itemCount: Int)

        fun onDataChanged(positionStart: Int, itemCount: Int)

    }

    interface ErrorListener {

        fun onFetchError(exception: DataException)

    }

    interface StatusListener {

        fun onStatusChange(
            isFetching: Boolean,
            isRefreshing: Boolean,
            isFullResult: Boolean,
            isEmpty: Boolean,
            isError: Boolean
        )

    }

}

interface QueryDataProvider<out E> : DataProvider<E> {
    
    var query: String?
    
}