package com.zipr.clg.data.entity

typealias DataMap = MutableMap<String, Any?>

val DataMap.FIELD_ID: String get() = "__ID__"

var DataMap.id: String
    get() = get(FIELD_ID) as String
    set(value) {
        put(FIELD_ID, value)
    }
