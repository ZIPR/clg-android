package com.zipr.clg.data.entity

import com.zipr.clg.util.invoke

data class UserFollower(
    val user: User?,
    val follower: Follower?
) {
    
    constructor(map: DataMap): this(
        map(UserFollower::user.name, ::User),
        map(UserFollower::follower.name, ::Follower)
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        user?.also { dataMap[UserFollower::user.name] = it.toDataMap() }
        follower?.also { dataMap[UserFollower::follower.name] = it.toDataMap() }
        return dataMap
    }
    
}