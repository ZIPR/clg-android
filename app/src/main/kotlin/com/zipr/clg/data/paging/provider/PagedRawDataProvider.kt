package com.zipr.clg.data.paging.provider

import com.zipr.clg.data.paging.source.PagedDataSource

open class PagedRawDataProvider<E>(
    fetchLimit: Long,
    isDynamicFetchEnabled: Boolean,
    dataSource: PagedDataSource<E>?
) : PagedDataProvider<E, E>(fetchLimit, isDynamicFetchEnabled, dataSource) {

    final override fun transformItem(item: E, position: Int) = item

}