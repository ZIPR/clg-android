package com.zipr.clg.data.entity

import com.zipr.clg.util.invoke

data class VotedPost(
    val post: Post?,
    val voting: Voting?
) {

    constructor(map: DataMap): this(
        map(VotedPost::post.name, ::Post),
        map(VotedPost::voting.name, ::Voting)
    )

}