package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.Date

@IgnoreExtraProperties
data class Voter(
    val user: User?,
    @ServerTimestamp val voteDate: Date?,
    @get:Exclude val id: String?
) {
    
    constructor(map: DataMap): this(
        map(Voter::user.name, ::User),
        map(Voter::voteDate.name),
        map.id
    )
    
}