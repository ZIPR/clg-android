package com.zipr.clg.data.interactor

import android.arch.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.zipr.clg.data.*
import com.zipr.clg.data.entity.*
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.data.paging.provider.PagedLiveDataProvider
import com.zipr.clg.data.paging.source.FirestoreLiveDataSource
import com.zipr.clg.data.paging.source.Order
import com.zipr.clg.data.repository.DocumentFetchLD
import com.zipr.clg.data.repository.SyncFetchLD
import com.zipr.clg.data.repository.by
import com.zipr.clg.ui.common.SourceImage
import com.zipr.clg.ui.common.SourceVideo
import com.zipr.clg.ui.common.SyncMediatorLiveData
import com.zipr.clg.util.*
import kotlin.collections.set

class PostInteractor {

    fun getPost(challengeId: String, postId: String, voterId: String): LiveData<Document<VotedPost>> =
        DocumentFetchLD(
            firestore.challenges.posts(challengeId).post(postId),
            false
        ).switchMap { document ->
            SyncFetchLD(
                listOf(
                    VotedPost::post.name by {
                        SyncFetchLD(
                            listOf(
                                Post::author.name by letTyped { documentReference: DocumentReference ->
                                    DocumentFetchLD(documentReference, false).map {
                                        it?.data
                                    }
                                }
                            ),
                            document?.data
                        )
                    },
                    VotedPost::voting.name by {
                        DocumentFetchLD(
                            firestore.users.votedPosts(voterId).post(postId),
                            true
                        ).map {
                            it?.data
                        }
                    }
                ),
                mutableMapOf()
            ).map {
                document?.clone(
                    it?.let(::VotedPost)
                )
            }
        }

    fun upRating(challengeId: String, postId: String, userId: String) {
        firestore.batch()
            .set(
                firestore.challenges.posts(challengeId).voters(postId).voter(userId),
                mapOf(
                    Voter::user.name to firestore.users.user(userId),
                    Voter::voteDate.name to FieldValue.serverTimestamp()
                )
            ).set(
                firestore.users.votedPosts(userId).post(postId),
                mapOf(
                    Voting::post.name to firestore.challenges.posts(challengeId).voters(postId).voter(userId),
                    Voting::voteDate.name to FieldValue.serverTimestamp()
                )
            ).commit()
    }

    fun downRating(challengeId: String, postId: String, userId: String) {
        firestore.batch()
            .delete(firestore.challenges.posts(challengeId).voters(postId).voter(userId))
            .delete(firestore.users.votedPosts(userId).post(postId))
            .commit()
    }

    fun create(
        challengeId: String,
        text: String,
        sourceImageLDList: List<LiveData<AsyncResult<SourceImage, Throwable>>>,
        sourceVideoList: List<SourceVideo>,
        location: Location?
    ) = object : LiveData<AsyncResult<String, Throwable>>() {

        private val id = documentAutoId()
        private val images = arrayOfNulls<Image>(sourceImageLDList.size)
        private val videos = arrayOfNulls<Video>(sourceVideoList.size)

        private var isUploaded = false
        private var isCancelled = false

        private val eventObserver = SingleEventObserver(
            null,
            object : SyncMediatorLiveData<Nothing>() {

                override val syncValue: Nothing? = null

            }
        ) {
            isUploaded = true
            create(
                challengeId,
                Post(
                    contentText = text,
                    contentImages = images.filterNotNull(),
                    contentVideos = videos.filterNotNull(),
                    location = location,
                    id = id
                )
            ).addOnCompleteListener {
                value = if (it.isSuccessful) {
                    AsyncResult(result = id)
                } else {
                    cancel()
                    AsyncResult(
                        error = DataException.fromFirebaseFirestoreException(it.exception as? FirebaseFirestoreException)
                    )
                }
            }
        }

        override fun onActive() {
            if (isUploaded || isCancelled) {
                return
            }

            sourceImageLDList.forEachIndexed { index, liveData ->
                eventObserver.liveData.addSource(
                    liveData.switchMap {
                        if (it?.result != null) {
                            images[index] = Image(null, it.result.aspectRatio)
                            uploadBitmap(
                                firebaseStorage.challenge(challengeId).post(id).image(index),
                                it.result.bitmap
                            )
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.map {
                        if (it?.result != null) {
                            images[index] = Image(
                                it.result.toString(),
                                images[index]?.aspectRatio
                            )
                        } else if (it?.error != null) {
                            cancel()
                            value = AsyncResult(error = it.error)
                        }
                    }
                )
            }

            sourceVideoList.forEachIndexed { index, video ->
                eventObserver.liveData.addSource(
                    video.thumbnailLD.switchMap {
                        if (it?.result != null) {
                            videos[index] = Video(null, Image(null, it.result.aspectRatio))
                            uploadBitmap(
                                firebaseStorage.challenge(challengeId).post(id).videoThumbnail(index),
                                it.result.bitmap
                            )
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.map {
                        if (it?.result != null) {
                            videos[index] = Video(
                                null,
                                Image(
                                    it.result.toString(),
                                    videos[index]?.thumbnail?.aspectRatio
                                )
                            )
                            video
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.switchMap {
                        it?.uri?.let {
                            uploadFile(firebaseStorage.challenge(challengeId).post(id).video(index), it)
                        }
                    }.map {
                        if (it?.result != null) {
                            videos[index] = Video(
                                it.result.toString(),
                                videos[index]?.thumbnail
                            )
                        } else if (it?.error != null) {
                            cancel()
                            value = AsyncResult(error = it.error)
                        }
                    }
                )
            }

            eventObserver.liveData.sync()
        }

        override fun onInactive() {
            if (!(isUploaded || isCancelled)) {
                cancel()
            }
        }

        private fun cancel() {
            isCancelled = true
            eventObserver.cancel()
            sourceImageLDList.forEachIndexed { index, _ ->
                firebaseStorage.challenge(challengeId).post(id).image(index).delete()
            }
            sourceVideoList.forEachIndexed { index, _ ->
                firebaseStorage.challenge(challengeId).post(id).videoThumbnail(index).delete()
                firebaseStorage.challenge(challengeId).post(id).video(index).delete()
            }
        }

    }

    private fun create(challengeId: String, post: Post) = firebaseUser?.uid.let { userId ->
        firestore.batch()
            .set(
                firestore.challenges.posts(challengeId).post(post.id!!),
                post.toDataMap().also {
                    it[Post::author.name] = firestore.users.user(userId!!)
                    it[Post::rating.name] = 0
                    it[Post::commentsCount.name] = 0
                    it[Post::creationDate.name] = FieldValue.serverTimestamp()
                }
            )
            .set(
                firestore.users.posts(userId!!).post(post.id),
                mapOf(
                    VotedPost::post.name to firestore.challenges.posts(challengeId).post(post.id),
                    Post::creationDate.name to FieldValue.serverTimestamp()
                )
            )
            .commit()
    }


    fun getComments(challengeId: String, postId: String, fetchLimit: Long): DataProvider<Comment> =
        PagedLiveDataProvider(
            fetchLimit,
            true,
            FirestoreLiveDataSource(
                firestore.challenges.posts(challengeId).comments(postId).query(),
                listOf(
                    Order(Comment::creationDate.name, Query.Direction.ASCENDING)
                ),
                {
                    SyncFetchLD(
                        listOf(
                            Comment::author.name by letTyped { documentReference: DocumentReference ->
                                DocumentFetchLD(documentReference, false).map {
                                    it?.data
                                }
                            }
                        ),
                        it
                    ).map {
                        it?.let(::Comment)
                    }
                }
            )
        )

    fun addComment(challengeId: String, postId: String, comment: Comment) {
        firestore.challenges.posts(challengeId).comments(postId).comment(documentAutoId()).set(
            comment.toDataMap().also {
                it[Comment::author.name] = firestore.users.user(comment.author!!.id!!)
                it[Comment::creationDate.name] = FieldValue.serverTimestamp()
            }
        )
    }

}