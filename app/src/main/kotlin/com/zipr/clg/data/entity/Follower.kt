package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.*

@IgnoreExtraProperties
data class Follower (
    val user: User?,
    @ServerTimestamp val date: Date?,
    @get:Exclude val id: String? = null
) {
    
    constructor(map: DataMap): this(
        map(Follower::user.name, ::User),
        map(Follower::date.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        user?.also { dataMap[Follower::user.name] = it.toDataMap() }
        date?.also { dataMap[Follower::date.name] = it }
        return dataMap
    }
    
}
