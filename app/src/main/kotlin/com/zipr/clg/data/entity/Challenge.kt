package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.Date

private const val SHARE_DESCRIPTION_MAX_LENGTH = 256

@IgnoreExtraProperties
data class Challenge(
    val title: String? = null,
    val image: Image? = null,
    val imageColor: Int? = null,
    val descriptionText: String? = null,
    val descriptionImages: List<Image>? = null,
    val descriptionVideos: List<Video>? = null,
    val author: User? = null,
    val tags: List<String>? = null,
    val participantsCount: Long? = null,
    val participantsPreview: List<Participant>? = null,
    @get:ServerTimestamp val creationDate: Date? = null,
    @get:Exclude val id: String? = null
) {
    
    constructor(map: DataMap): this(
        map(Challenge::title.name),
        map(Challenge::image.name, ::Image),
        map(Challenge::imageColor.name) { it: Long -> it.toInt() },
        map(Challenge::descriptionText.name),
        map(Challenge::descriptionImages.name) { it: List<DataMap> -> it.map(::Image) },
        map(Challenge::descriptionVideos.name) { it: List<DataMap> -> it.map(::Video) },
        map(Challenge::author.name, ::User),
        map(Challenge::tags.name),
        map(Challenge::participantsCount.name),
        map(Challenge::participantsPreview.name) { it: List<DataMap> -> it.map(::Participant) },
        map(Challenge::creationDate.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        title?.also { dataMap[Challenge::title.name] = it }
        image?.also { dataMap[Challenge::image.name] = it.toDataMap() }
        imageColor?.also { dataMap[Challenge::imageColor.name] = it }
        descriptionText?.also { dataMap[Challenge::descriptionText.name] = it }
        descriptionImages?.also { dataMap[Challenge::descriptionImages.name] = it.map { it.toDataMap() } }
        descriptionVideos?.also { dataMap[Challenge::descriptionVideos.name] = it.map { it.toDataMap() } }
        author?.also { dataMap[Challenge::author.name] = it }
        tags?.also { dataMap[Challenge::tags.name] = it }
        participantsCount?.also { dataMap[Challenge::participantsCount.name] = it}
        participantsPreview?.also { dataMap[Challenge::participantsPreview.name] = it.map { it.toDataMap() } }
        creationDate?.also { dataMap[Challenge::creationDate.name] = it }
        return dataMap
    }

    fun toShareString() = "Challenge: $title\n" +
            "Description: ${ descriptionText?.let {
                if (it.length >= SHARE_DESCRIPTION_MAX_LENGTH) it.substring(0, SHARE_DESCRIPTION_MAX_LENGTH-1) + "..."
                else it
            }}\n" +
            "Participants count: $participantsCount"

}