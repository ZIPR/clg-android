package com.zipr.clg.data.interactor

import android.arch.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.zipr.clg.data.*
import com.zipr.clg.data.entity.*
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.data.paging.provider.PagedLiveDataProvider
import com.zipr.clg.data.paging.provider.QueryDataProvider
import com.zipr.clg.data.paging.source.FirestoreLiveDataSource
import com.zipr.clg.data.paging.source.Order
import com.zipr.clg.data.repository.*
import com.zipr.clg.ui.common.SourceImage
import com.zipr.clg.ui.common.SourceVideo
import com.zipr.clg.ui.common.SyncMediatorLiveData
import com.zipr.clg.util.*

private const val PARTICIPANTS_PREVIEW_LIMIT = 3L

class ChallengeInteractor {

    fun getChallenge(challengeId: String): LiveData<Document<ChallengeParticipating>> =
        DocumentFetchLD(
            firestore.challenges.challenge(challengeId),
            true
        ).switchMap { document ->
            SyncFetchLD(
                listOf(
                    ChallengeParticipating::challenge.name by {
                        SyncFetchLD(
                            listOf(
                                Challenge::author.name by letTyped { documentReference: DocumentReference ->
                                    DocumentFetchLD(documentReference, true).map {
                                        it?.data
                                    }
                                },
                                Challenge::participantsPreview.name by {
                                    QueryFetchLD(
                                        firestore.challenges.participants(challengeId).query()
                                            .orderBy(Participant::joinDate.name)
                                            .limit(PARTICIPANTS_PREVIEW_LIMIT),
                                        false
                                    ).switchMap {
                                        SyncFetchListLD(
                                            listOf(
                                                Participant::user.name by letTyped { documentReference: DocumentReference ->
                                                    DocumentFetchLD(documentReference, false).map {
                                                        it?.data
                                                    }
                                                }
                                            ),
                                            it?.data
                                        )
                                    }
                                }
                            ),
                            document?.data
                        )
                    },
                    ChallengeParticipating::participating.name by {
                        DocumentFetchLD(
                            firestore.users.participating(firebaseUser!!.uid).challenge(document?.data?.id.orEmpty()),
                            true,
                            listOf(
                                Participating::joinDate.name,
                                Participating::completionDate.name
                            )
                        ).map {
                            it?.data
                        }
                    }
                ),
                if (document?.data != null) mutableMapOf() else null
            ).map {
                document?.clone(
                    it?.let(::ChallengeParticipating)
                )
            }
        }

    fun getChallenges(fetchLimit: Long): QueryDataProvider<ChallengeParticipating> =
        object : QueryDataProvider<ChallengeParticipating>, PagedLiveDataProvider<ChallengeParticipating>(fetchLimit, false, null) {

            override var query: String? = null
                set(value) {
                    field = value
                    setDataSource(
                        FirestoreLiveDataSource(
                            firestore.challenges.query().run {
                                if (value?.isNotBlank() == true) whereEqualTo(Challenge::title.name, value)
                                else this
                            },
                            listOf(
                                Order(
                                    Challenge::participantsCount.name,
                                    Query.Direction.DESCENDING
                                )
                            ),
                            { dataMap ->
                                SyncFetchLD(
                                    listOf(
                                        ChallengeParticipating::challenge.name by {
                                            SyncFetchLD(
                                                listOf(
                                                    Challenge::author.name by letTyped { documentReference: DocumentReference ->
                                                        DocumentFetchLD(documentReference, true).map {
                                                            it?.data
                                                        }
                                                    }
                                                ),
                                                dataMap
                                            )
                                        },
                                        ChallengeParticipating::participating.name by {
                                            DocumentFetchLD(
                                                firestore.users.participating(firebaseUser!!.uid).challenge(dataMap.id),
                                                true
                                            ).map {
                                                it?.data
                                            }
                                        }
                                    ),
                                    mutableMapOf()
                                ).map {
                                    it?.let(::ChallengeParticipating)
                                }
                            }
                        )
                    )
                }

        }

    fun getParticipants(
        fetchLimit: Long,
        challengeId: String,
        followStatusUserId: String,
        isUserInfoRealtime: Boolean,
        isFollowStatusRealtime: Boolean
    ): DataProvider<UserFollower> =
        PagedLiveDataProvider(
            fetchLimit,
            false,
            FirestoreLiveDataSource(
                firestore.challenges.participants(challengeId).query(),
                listOf(
                    Order(Participant::joinDate.name, Query.Direction.DESCENDING)
                ),
                {
                    SyncFetchLD(
                        listOf(
                            (Participant::user.name to UserFollower::user.name).by(letTyped { documentReference: DocumentReference ->
                                DocumentFetchLD(documentReference, isUserInfoRealtime).map {
                                    it?.data
                                }
                            }),
                            (it.FIELD_ID to UserFollower::follower.name).by(letTyped { participantId: String ->
                                DocumentFetchLD(
                                    firestore.users.following(followStatusUserId).user(participantId),
                                    isFollowStatusRealtime,
                                    listOf(Follower::date.name)
                                ).map {
                                    it?.data
                                }
                            })
                        ),
                        it
                    ).map {
                        it?.let(::UserFollower)
                    }
                }
            )
        )

    fun join(challengeId: String, userId: String) {
        firestore.batch()
            .set(
                firestore.challenges.participants(challengeId).user(userId),
                mapOf(
                    Participant::user.name to firestore.users.user(userId),
                    Participant::joinDate.name to FieldValue.serverTimestamp(),
                    Participant::isComplete.name to false,
                    Participant::completionDate.name to null
                )
            )
            .set(
                firestore.users.participating(userId).challenge(challengeId),
                mapOf(
                    Participating::challenge.name to firestore.challenges.challenge(challengeId),
                    Participating::joinDate.name to FieldValue.serverTimestamp(),
                    Participating::isComplete.name to false,
                    Participating::completionDate.name to null
                )
            )
            .commit()
    }

    fun leave(challengeId: String, userId: String) {
        firestore.batch()
            .delete(
                firestore.challenges.participants(challengeId).user(userId)
            )
            .delete(
                firestore.users.participating(userId).challenge(challengeId)
            )
            .commit()
    }

    fun checkTitleAvailability(title: String) = object : LiveData<AsyncResult<Boolean, Exception>>() {

        init {
            firestore.challenges.query()
                .whereEqualTo(Challenge::title.name, title)
                .get()
                .addOnCompleteListener {
                    value =
                            if (it.result != null) {
                                AsyncResult(result = it.result.isEmpty)
                            } else {
                                AsyncResult(
                                    error = DataException.fromFirebaseFirestoreException(
                                        it.exception as? FirebaseFirestoreException
                                    )
                                )
                            }
                }
        }

    }

    fun create(
        title: String,
        descText: String,
        tags: List<String>,
        imageColor: Int,
        imageLD: LiveData<AsyncResult<SourceImage, Throwable>>,
        descSourceImageLDList: List<LiveData<AsyncResult<SourceImage, Throwable>>>,
        descSourceVideoList: List<SourceVideo>
    ) = object : LiveData<AsyncResult<String, Throwable>>() {

        private val id = documentAutoId()
        private lateinit var image: Image
        private val descriptionImages = arrayOfNulls<Image>(descSourceImageLDList.size)
        private val descriptionVideos = arrayOfNulls<Video>(descSourceVideoList.size)

        private var isUploaded = false
        private var isCancelled = false

        private val eventObserver = SingleEventObserver(
            null,
            object : SyncMediatorLiveData<Nothing>() {

                override val syncValue: Nothing? = null

            }
        ) {
            checkTitleAvailability(title).observeSingleEventNonNull {
                if (isCancelled) {
                    return@observeSingleEventNonNull
                } else {
                    isUploaded = true
                }
                if (it.result != null) {
                    if (it.result) {
                        create(
                            Challenge(
                                title = title,
                                descriptionText = descText,
                                tags = tags,
                                imageColor = imageColor,
                                image = image,
                                descriptionImages = descriptionImages.filterNotNull(),
                                descriptionVideos = descriptionVideos.filterNotNull(),
                                id = id
                            )
                        ).addOnCompleteListener {
                            value = if (it.isSuccessful) {
                                AsyncResult(result = id)
                            } else {
                                cancel()
                                AsyncResult(
                                    error = DataException.fromFirebaseFirestoreException(it.exception as? FirebaseFirestoreException)
                                )
                            }
                        }
                    } else {
                        cancel()
                        value = AsyncResult(
                            error = DataException(DataException.Type.ALREADY_EXISTS, "title should be unique")
                        )
                    }
                } else {
                    cancel()
                    value = AsyncResult(error = it.error)
                }
            }
        }

        override fun onActive() {
            if (isUploaded || isCancelled) {
                return
            }
            eventObserver.liveData.addSource(
                imageLD.switchMap {
                    if (it?.result != null) {
                        image = Image(null, it.result.aspectRatio)
                        uploadBitmap(firebaseStorage.challenge(id).image, it.result.bitmap)
                    } else {
                        if (it?.error != null) {
                            cancel()
                            value = AsyncResult(error = it.error)
                        }
                        null
                    }
                }.map {
                    if (it?.result != null) {
                        image = Image(it.result.toString(), image.aspectRatio)
                    } else if (it?.error != null) {
                        cancel()
                        value = AsyncResult(error = it.error)
                    }
                }
            )

            descSourceImageLDList.forEachIndexed { index, liveData ->
                eventObserver.liveData.addSource(
                    liveData.switchMap {
                        if (it?.result != null) {
                            descriptionImages[index] = Image(null, it.result.aspectRatio)
                            uploadBitmap(
                                firebaseStorage.challenge(id).descriptionImage(index),
                                it.result.bitmap
                            )
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.map {
                        if (it?.result != null) {
                            descriptionImages[index] = Image(
                                it.result.toString(),
                                descriptionImages[index]?.aspectRatio
                            )
                        } else if (it?.error != null) {
                            cancel()
                            value = AsyncResult(error = it.error)
                        }
                    }
                )
            }

            descSourceVideoList.forEachIndexed { index, video ->
                eventObserver.liveData.addSource(
                    video.thumbnailLD.switchMap {
                        if (it?.result != null) {
                            descriptionVideos[index] = Video(null, Image(null, it.result.aspectRatio))
                            uploadBitmap(
                                firebaseStorage.challenge(id).descriptionVideoThumbnail(index),
                                it.result.bitmap
                            )
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.map {
                        if (it?.result != null) {
                            descriptionVideos[index] = Video(
                                null,
                                Image(
                                    it.result.toString(),
                                    descriptionVideos[index]?.thumbnail?.aspectRatio
                                )
                            )
                            video
                        } else {
                            if (it?.error != null) {
                                cancel()
                                value = AsyncResult(error = it.error)
                            }
                            null
                        }
                    }.switchMap {
                        it?.uri?.let {
                            uploadFile(firebaseStorage.challenge(id).descriptionVideo(index), it)
                        }
                    }.map {
                        if (it?.result != null) {
                            descriptionVideos[index] = Video(
                                it.result.toString(),
                                descriptionVideos[index]?.thumbnail
                            )
                        } else if (it?.error != null) {
                            cancel()
                            value = AsyncResult(error = it.error)
                        }
                    }
                )
            }

            checkTitleAvailability(title).observeSingleEventNonNull {
                if (it.result != null) {
                    if (it.result) {
                        eventObserver.liveData.sync()
                    } else {
                        cancel()
                        value = AsyncResult(
                            error = DataException(DataException.Type.ALREADY_EXISTS, "title should be unique")
                        )
                    }
                } else {
                    cancel()
                    value = AsyncResult(error = it.error)
                }
            }
        }

        override fun onInactive() {
            if (!(isUploaded || isCancelled)) {
                cancel()
            }
        }

        private fun cancel() {
            isCancelled = true
            eventObserver.cancel()
            firebaseStorage.challenge(id).image.delete()
            descSourceImageLDList.forEachIndexed { index, _ ->
                firebaseStorage.challenge(id).descriptionImage(index).delete()
            }
            descSourceVideoList.forEachIndexed { index, _ ->
                firebaseStorage.challenge(id).descriptionVideoThumbnail(index).delete()
                firebaseStorage.challenge(id).descriptionVideo(index).delete()
            }
        }

    }

    private fun create(challenge: Challenge) = firebaseUser?.uid.let { userId ->
        firestore.batch()
            .set(
                firestore.challenges.challenge(challenge.id!!),
                challenge.toDataMap().also {
                    it[Challenge::author.name] = firestore.users.user(userId!!)
                    it[Challenge::creationDate.name] = FieldValue.serverTimestamp()
                    it[Challenge::participantsCount.name] = 0

                }
            )
            .set(
                firestore.challenges.participants(challenge.id).user(userId!!),
                mapOf(
                    Participant::user.name to firestore.users.user(userId),
                    Participant::joinDate.name to FieldValue.serverTimestamp(),
                    Participant::isComplete.name to false,
                    Participant::completionDate.name to null
                )
            )
            .set(
                firestore.users.participating(userId).challenge(challenge.id),
                mapOf(
                    Participating::challenge.name to firestore.challenges.challenge(challenge.id),
                    Participating::joinDate.name to FieldValue.serverTimestamp(),
                    Participating::isComplete.name to false,
                    Participating::completionDate.name to null
                )
            )
            .commit()
    }

    fun getPosts(
        fetchLimit: Long,
        challengeId: String,
        voterUserId: String
    ): DataProvider<VotedPost> =
        PagedLiveDataProvider(
            fetchLimit,
            false,
            FirestoreLiveDataSource(
                firestore.challenges.posts(challengeId).query(),
                listOf(
                    Order(Post::creationDate.name, Query.Direction.DESCENDING)
                ),
                { dataMap ->
                    SyncFetchLD(
                        listOf(
                            VotedPost::post.name by {
                                SyncFetchLD(
                                    listOf(
                                        Post::author.name by letTyped { documentReference: DocumentReference ->
                                            DocumentFetchLD(documentReference, false).map {
                                                it?.data
                                            }
                                        }
                                    ),
                                    dataMap
                                )
                            },
                            (dataMap.FIELD_ID to VotedPost::voting.name).by(letTyped { postId: String ->
                                DocumentFetchLD(
                                    firestore.users.votedPosts(voterUserId).post(postId),
                                    true
                                ).map {
                                    it?.data
                                }
                            })
                        ),
                        dataMap
                    ).map {
                        it?.let(::VotedPost)
                    }
                }
            )
        )

}