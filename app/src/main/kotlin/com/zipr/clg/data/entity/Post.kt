package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import com.zipr.clg.util.orZero
import java.util.Date

private const val SHARE_TEXT_MAX_LENGTH = 256

@IgnoreExtraProperties
data class Post(
    val author: User? = null,
    val contentText: String? = null,
    val contentImages: List<Image>? = null,
    val contentVideos: List<Video>? = null,
    val location: Location? = null,
    val rating: Long? = null,
    val commentsCount: Long? = null,
    @get:ServerTimestamp val creationDate: Date? = null,
    @get:Exclude val id: String? = null
) {

    constructor(map: DataMap): this(
        map(Post::author.name, ::User),
        map(Post::contentText.name),
        map(Post::contentImages.name, { it: List<DataMap> -> it.map(::Image) }),
        map(Post::contentVideos.name, { it: List<DataMap> -> it.map(::Video) }),
        map(Post::location.name, {it: DataMap -> Location(it)}),
        map(Post::rating.name),
        map(Post::commentsCount.name),
        map(Post::creationDate.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        author?.also { dataMap[Post::author.name] = it.toDataMap() }
        contentText?.also { dataMap[Post::contentText.name] = it }
        contentImages?.also { dataMap[Post::contentImages.name] = it.map { it.toDataMap() } }
        contentVideos?.also { dataMap[Post::contentVideos.name] = it.map { it.toDataMap() } }
        location?.also { dataMap[Post::location.name] = it.toDataMap() }
        rating?.also { dataMap[Post::rating.name] = it }
        commentsCount?.also { dataMap[Post::commentsCount.name] = it }
        creationDate?.also { dataMap[Post::creationDate.name] = it }
        return dataMap
    }

    fun toShareString() = "Post from ${author?.username}\n" +
            "${contentText?.let {
                if (it.length >= SHARE_TEXT_MAX_LENGTH) it.substring(0, SHARE_TEXT_MAX_LENGTH - 1) + "..."
                else it
            }}\n" +
            "Rating: ${rating.orZero()}\n" +
            "Comments count: ${commentsCount.orZero()}"

}