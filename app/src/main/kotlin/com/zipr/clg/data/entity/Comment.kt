package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.Date

data class Comment(
    val author: User? = null,
    val contentText: String? = null,
    @ServerTimestamp val creationDate: Date? = null,
    @get:Exclude val id: String? = null
) {

    constructor(map: DataMap): this(
        map(Comment::author.name, ::User),
        map(Comment::contentText.name),
        map(Comment::creationDate.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        author?.also { dataMap[Comment::author.name] = it.toDataMap() }
        contentText?.also { dataMap[Comment::contentText.name] = it }
        creationDate?.also { dataMap[Comment::creationDate.name] = it }
        return dataMap
    }

}