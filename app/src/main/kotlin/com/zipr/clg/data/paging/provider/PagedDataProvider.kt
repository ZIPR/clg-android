package com.zipr.clg.data.paging.provider

import android.support.annotation.CallSuper
import com.zipr.clg.data.DataException
import com.zipr.clg.data.paging.source.PagedDataSource

abstract class PagedDataProvider<out E, in D>(
    private val fetchLimit: Long,
    private val isDynamicFetchEnabled: Boolean,
    dataSource: PagedDataSource<D>? = null
) : DataProvider<E> {

    private val fetchDistance = fetchLimit.toInt() / 2

    private lateinit var pagedDataSource: PagedDataSource<D>

    private val dataListeners = mutableListOf<DataProvider.DataListener>()
    private val errorListeners = mutableListOf<DataProvider.ErrorListener>()
    private val statusListeners = mutableListOf<DataProvider.StatusListener>()

    private var isFetching = false
    private var isFullResult = false
    private var isRefreshing = false
    private val isEmpty get() = data.isEmpty()
    private val isError get() = error != null

    private val data = mutableListOf<D>()
    private val onData = { newData: List<D> ->
        error = null

        addData(newData)

        isFullResult = newData.size < fetchLimit
        isFetching = false
        isRefreshing = false
        notifyStatusListeners()

        if (isFullResult && isDynamicFetchEnabled) {
            pagedDataSource.startDynamicFetch(fetchLimit, onDynamicFetchData, {})
        }
    }
    private val onDynamicFetchData = { dynamicData: List<D> ->
        addData(dynamicData)
        notifyStatusListeners()
    }

    private var error: DataException? = null
    private val onError = { newError: DataException ->
        error = newError
        notifyError(newError)

        if (isRefreshing && !isEmpty) {
            val itemCount = data.size
            clear()
            notifyDataRemoved(0, itemCount)
        }

        isFullResult = newError.type == DataException.Type.NOT_FOUND
        isFetching = false
        isRefreshing = false
        notifyStatusListeners()

        if (isFullResult && isDynamicFetchEnabled) {
            pagedDataSource.startDynamicFetch(fetchLimit, onDynamicFetchData, {})
        }
    }

    init {
        dataSource?.also {
            setDataSource(it)
        }
    }

    final override fun getItemCount() = data.size

    final override fun getItem(position: Int): E = data[position].let {
        fetchNextIfNeeded(position)
        transformItem(it, position)
    }

    protected abstract fun transformItem(item: D, position: Int): E

    protected fun setDataSource(dataSource: PagedDataSource<D>) {
        if (::pagedDataSource.isInitialized) {
            pagedDataSource.reset()
        }
        pagedDataSource = dataSource

        if (data.isNotEmpty()) {
            val itemCount = data.size
            clear()
            notifyDataRemoved(0, itemCount)
        }

        isFullResult = false
        isFetching = fetchNextData()
        notifyStatusListeners()
    }

    final override fun retryFetching() {
        isFetching = fetchNextData()
        notifyStatusListeners()
    }

    final override fun refreshData() {
        pagedDataSource.reset()
        isRefreshing = true
        isFullResult = false
        isFetching = fetchNextData()
        notifyStatusListeners()
    }

    private fun addData(newData: List<D>) {
        val currentSize = data.size
        val newDataSize = newData.size
        if (isRefreshing && currentSize > 0) {
            clear()
            data.addAll(newData)

            val diff = currentSize - newDataSize
            when {
                diff > 0 -> {
                    notifyDataChanged(0, newDataSize)
                    notifyDataRemoved(newDataSize, diff)
                }

                diff < 0 -> {
                    notifyDataChanged(0, currentSize)
                    notifyDataInserted(currentSize, -diff)
                }

                else -> {
                    notifyDataChanged(0, newDataSize)
                }
            }
        } else {
            data.addAll(newData)
            notifyDataInserted(currentSize, newDataSize)
        }
    }

    private fun fetchNextData(): Boolean {
        error = null
        return pagedDataSource.fetchNext(fetchLimit, onData, onError)
    }

    private fun fetchNextIfNeeded(position: Int) {
        if (!isFetching && !isFullResult && !isError && position >= data.lastIndex - fetchDistance) {
            isFetching = fetchNextData()
            notifyStatusListeners()
        }
    }

    @CallSuper
    protected open fun clear() {
        pagedDataSource.stopDynamicFetch()
        data.clear()
    }

    final override fun registerDataListener(dataListener: DataProvider.DataListener) {
        dataListeners.add(dataListener)
        if (dataListeners.size == 1) {
            onDataActive()
        }
    }

    final override fun registerErrorListener(errorListener: DataProvider.ErrorListener) {
        error?.also { errorListener.onFetchError(it) }
        errorListeners.add(errorListener)
    }

    final override fun registerStatusListener(statusListener: DataProvider.StatusListener) {
        statusListener.onStatusChange(
            isFetching,
            isRefreshing,
            isFullResult,
            data.isEmpty(),
            error != null
        )
        statusListeners.add(statusListener)
    }

    final override fun unregisterDataListener(dataListener: DataProvider.DataListener) {
        dataListeners.remove(dataListener)
        if (dataListeners.isEmpty()) {
            onDataInactive()
        }
    }

    final override fun unregisterErrorListener(errorListener: DataProvider.ErrorListener) {
        errorListeners.remove(errorListener)
    }

    final override fun unregisterStatusListener(statusListener: DataProvider.StatusListener) {
        statusListeners.remove(statusListener)
    }

    protected fun notifyDataChanged(positionStart: Int, itemCount: Int) {
        dataListeners.forEach {
            it.onDataChanged(positionStart, itemCount)
        }
    }

    protected fun notifyDataInserted(positionStart: Int, itemCount: Int) {
        dataListeners.forEach {
            it.onDataInserted(positionStart, itemCount)
        }
    }

    protected fun notifyDataRemoved(positionStart: Int, itemCount: Int) {
        dataListeners.forEach {
            it.onDataRemoved(positionStart, itemCount)
        }
    }

    protected fun notifyError(error: DataException) {
        errorListeners.forEach {
            it.onFetchError(error)
        }
    }

    protected fun notifyStatusListeners() {
        statusListeners.forEach {
            it.onStatusChange(isFetching, isRefreshing, isFullResult, isEmpty, isError)
        }
    }

    protected open fun onDataActive() {
        if (isFullResult && isDynamicFetchEnabled) {
            pagedDataSource.startDynamicFetch(fetchLimit, onDynamicFetchData, {})
        }
    }

    protected open fun onDataInactive() {
        pagedDataSource.stopDynamicFetch()
    }

}