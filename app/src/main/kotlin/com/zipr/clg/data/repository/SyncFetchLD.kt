package com.zipr.clg.data.repository

import android.arch.lifecycle.LiveData
import com.zipr.clg.data.entity.DataMap
import com.zipr.clg.ui.common.SyncMediatorLiveData

data class FetchingData<T>(
    val sourceKey: String,
    val targetKey: String,
    val data: (Any?) -> LiveData<T>?
)

infix fun <T> String.by(data: (Any?) -> LiveData<T>?) =
    FetchingData(this, this, data)

infix fun <T> Pair<String, String>.by(data: (Any?) -> LiveData<T>?) =
    FetchingData(first, second, data)

class SyncFetchLD(
    fetchingDataList: List<FetchingData<*>>,
    dataMap: DataMap?
) : SyncMediatorLiveData<DataMap>() {

    override val syncValue = dataMap

    init {
        if (dataMap != null) {
            fetchingDataList.forEach { fetchingData ->
                fetchingData.data(dataMap[fetchingData.sourceKey])?.apply {
                    addSource(this, {
                        dataMap[fetchingData.targetKey] = it
                    })
                }
            }
        }
        sync()
    }

}

class SyncFetchListLD(
    fetchingDataList: List<FetchingData<*>>,
    dataMapList: List<DataMap>?
) : SyncMediatorLiveData<List<DataMap>>() {

    override val syncValue = dataMapList

    init {
        if (dataMapList?.isNotEmpty() == true) {
            dataMapList.forEach {
                addSource(SyncFetchLD(fetchingDataList, it))
            }
        }
        sync()
    }

}