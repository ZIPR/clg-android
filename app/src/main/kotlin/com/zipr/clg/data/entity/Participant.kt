package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.*

@IgnoreExtraProperties
data class Participant(
    val user: User?,
    val isComplete: Boolean?,
    @ServerTimestamp val completionDate: Date?,
    @ServerTimestamp val joinDate: Date?,
    @get:Exclude val id: String?
) {
    
    constructor(map: DataMap): this(
        map(Participant::user.name, ::User),
        map(Participant::isComplete.name),
        map(Participant::completionDate.name),
        map(Participant::joinDate.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        user?.also { dataMap[Participant::user.name] = it.toDataMap() }
        isComplete?.also { dataMap[Participant::isComplete.name] = it }
        completionDate?.also { dataMap[Participant::completionDate.name] = it }
        joinDate?.also { dataMap[Participant::joinDate.name] = it }
        return dataMap
    }

}