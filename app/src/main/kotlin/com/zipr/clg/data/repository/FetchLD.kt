package com.zipr.clg.data.repository

import android.arch.lifecycle.LiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestoreException
import com.zipr.clg.data.entity.Document

abstract class FetchLD<R, T> : LiveData<Document<R>>() {

    protected abstract val snapshotListener: EventListener<T>

    private val fetchTaskListener = OnCompleteListener<T> {
        snapshotListener.onEvent(
            if (it.isSuccessful) it.result else null,
            it.exception as? FirebaseFirestoreException?
        )
    }
    
    protected fun observeFetchTask(task: Task<T>) {
        task.addOnCompleteListener(fetchTaskListener)
    }

}