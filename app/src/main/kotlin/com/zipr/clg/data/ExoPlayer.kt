package com.zipr.clg.data

import android.content.Context
import com.google.android.exoplayer2.ExoPlaybackException
import com.orhanobut.logger.Logger
import com.zipr.clg.util.showToast

fun onExoPlayerError(context: Context?, e: ExoPlaybackException, message: String) {
    when (e.type) {
        ExoPlaybackException.TYPE_SOURCE ->
            showToast(context, "$message: ${e.sourceException.message}")

        ExoPlaybackException.TYPE_RENDERER ->
            showToast(context, "$message: ${e.rendererException.message}")

        ExoPlaybackException.TYPE_UNEXPECTED ->
            showToast(context, "$message: ${e.unexpectedException.message}")

        else -> showToast(context, "$message: ${e.message}")
    }

    Logger.e(e, message)
}