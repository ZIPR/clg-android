package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.*

@IgnoreExtraProperties
data class Participating(
    val challenge: Challenge?,
    val isComplete: Boolean?,
    @ServerTimestamp val completionDate: Date?,
    @ServerTimestamp val joinDate: Date?,
    @get:Exclude val id: String? = null
) {
    
    constructor(map: DataMap): this(
        map(Participating::challenge.name, ::Challenge),
        map(Participating::isComplete.name),
        map(Participating::completionDate.name),
        map(Participating::joinDate.name),
        map.id
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        challenge?.also { dataMap[Participating::challenge.name] = it.toDataMap() }
        isComplete?.also { dataMap[Participating::isComplete.name] = it }
        completionDate?.also { dataMap[Participating::completionDate.name] = it }
        joinDate?.also { dataMap[Participating::joinDate.name] = it }
        return dataMap
    }
    
}
