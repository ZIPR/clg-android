package com.zipr.clg.data.entity

import com.zipr.clg.util.invoke

data class Video(
    val url: String?,
    val thumbnail: Image?
) {

    constructor(map: DataMap): this(
        map(Video::url.name),
        map(Video::thumbnail.name, ::Image)
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        url?.also { dataMap[Video::url.name] = it }
        thumbnail?.also { dataMap[Video::thumbnail.name] = it.toDataMap() }
        return dataMap
    }

}