package com.zipr.clg.data.paging.provider

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import com.zipr.clg.data.paging.source.PagedDataSource

open class PagedLiveDataProvider<E>(
    fetchLimit: Long,
    isDynamicFetchEnabled: Boolean,
    dataSource: PagedDataSource<LiveData<E>>?
) : PagedDataProvider<E, LiveData<E>>(fetchLimit, isDynamicFetchEnabled, dataSource) {

    private val liveItemsSize = fetchLimit.toInt()
    private val liveItems = Array<LiveItem<E>>(liveItemsSize, { _ ->
        LiveItem(::onLiveItemChanged)
    })

    private fun onLiveItemChanged(position: Int) {
        notifyDataChanged(position, 1)
    }

    final override fun transformItem(item: LiveData<E>, position: Int) =
        liveItems[position % liveItemsSize].let {
            it.setItem(position, item)
            item.value!!
        }

    final override fun clear() {
        clearLiveItems()
        super.clear()
    }

    final override fun onDataInactive() {
        super.onDataInactive()
        clearLiveItems()
    }

    private fun clearLiveItems() {
        liveItems.forEach { it.clear() }
    }

    private class LiveItem<E>(
        private val onChanged: (position: Int) -> Unit
    ) {

        private val observer = Observer<E> {
            if (value != it) {
                value = it
                onChanged(position)
            }
        }

        private var isEnabled = false
            set (value) {
                if (field != value) {
                    itemLD?.apply {
                        if (value) observeForever(observer)
                        else removeObserver(observer)
                    }
                    field = value
                }
            }

        private var itemLD: LiveData<E>? = null
            set(value) {
                field = value
                this.value = value?.value
            }

        private var value: E? = null
        private var position = -1

        fun setItem(position: Int, itemLD: LiveData<E>) {
            if (this.itemLD != itemLD) {
                this.isEnabled = false
                this.position = position
                this.itemLD = itemLD
            }
            this.isEnabled = true
        }

        fun clear() {
            isEnabled = false
            position = -1
            itemLD = null
        }

    }

}