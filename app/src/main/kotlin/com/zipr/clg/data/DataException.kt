package com.zipr.clg.data

import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.storage.StorageException

class DataException(val type: Type, message: String? = null) : Exception(message) {

    companion object {

        fun fromFirebaseFirestoreException(
            exception: FirebaseFirestoreException?,
            message: String? = null
        ) = DataException(
            when (exception?.code) {
                FirebaseFirestoreException.Code.NOT_FOUND -> Type.NOT_FOUND
                FirebaseFirestoreException.Code.ALREADY_EXISTS -> Type.ALREADY_EXISTS
                FirebaseFirestoreException.Code.PERMISSION_DENIED -> Type.PERMISSION_DENIED
                FirebaseFirestoreException.Code.UNAVAILABLE -> Type.UNAVAILABLE
                FirebaseFirestoreException.Code.UNAUTHENTICATED -> Type.UNAUTHENTICATED
                FirebaseFirestoreException.Code.CANCELLED -> Type.CANCELLED

                else -> Type.UNEXPECTED
            },
            message ?: exception?.message
        )

        fun fromStorageException(
            exception: StorageException?,
            message: String? = null
        ) = DataException(
            when (exception?.errorCode) {
                StorageException.ERROR_OBJECT_NOT_FOUND -> Type.NOT_FOUND
                StorageException.ERROR_BUCKET_NOT_FOUND -> Type.NOT_FOUND
                StorageException.ERROR_PROJECT_NOT_FOUND -> Type.NOT_FOUND
                StorageException.ERROR_RETRY_LIMIT_EXCEEDED -> Type.UNAVAILABLE
                StorageException.ERROR_NOT_AUTHENTICATED -> Type.UNAUTHENTICATED
                StorageException.ERROR_CANCELED -> Type.CANCELLED

                else -> Type.UNEXPECTED
            },
            message ?: exception?.message
        )

    }

    enum class Type {
        NOT_FOUND,
        ALREADY_EXISTS,
        PERMISSION_DENIED,
        UNAVAILABLE,
        UNAUTHENTICATED,
        CANCELLED,
        UNEXPECTED
    }

    override fun toString() = "DataException(type=${type.name},message=$message)"

}