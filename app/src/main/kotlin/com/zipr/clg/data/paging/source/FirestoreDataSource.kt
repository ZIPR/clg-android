package com.zipr.clg.data.paging.source

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.Query
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.DataMap
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.id
import com.zipr.clg.data.repository.QueryFetchLD
import com.zipr.clg.ui.common.SyncMediatorLiveData
import com.zipr.clg.util.SingleEventObserver
import com.zipr.clg.util.map
import com.zipr.clg.util.observeSingleEvent
import com.zipr.clg.util.switchMap

class Order(val field: String, val direction: Query.Direction)

abstract class FirestoreDataSource<out T>(
    query: Query,
    order: List<Order>?
) : PagedDataSource<T> {

    private var pageToken: Array<out Any?>? = null
    private val order = if (order?.isNotEmpty() == true) order else null
    private val query = query.run {
        order?.fold(this) { query: Query, order: Order ->
            query.orderBy(order.field, order.direction)
        }?.orderBy(FieldPath.documentId()) ?: orderBy(FieldPath.documentId())
    }

    protected var isFetching = false
    protected var isDynamicFetchEnabled = false

    protected var currentEventObserver: SingleEventObserver<*, *>? = null

    protected fun getQuery(limit: Long) = query.limit(limit).run {
        pageToken?.let { startAfter(*it) } ?: this
    }

    protected fun setPage(page: List<DataMap>) {
        pageToken = page.lastOrNull()?.let { lastItem ->
            order?.mapTo(mutableListOf()) {
                lastItem[it.field]
            }?.also {
                it.add(lastItem.id)
            }?.toTypedArray() ?: arrayOf(lastItem.id)
        }
    }

    override fun reset() {
        stopDynamicFetch()
        currentEventObserver?.cancel()
        pageToken = null
        isFetching = false
        isDynamicFetchEnabled = false
    }

}

class FirestoreRawDataSource<out T>(
    query: Query,
    order: List<Order>?,
    private val map: (DataMap) -> T
) : FirestoreDataSource<T>(query, order) {

    private var dynamicDataListener: DynamicDataListener? = null

    override fun fetchNext(limit: Long, onData: (List<T>) -> Unit, onError: (DataException) -> Unit): Boolean {
        if (isFetching || isDynamicFetchEnabled) {
            return false
        }
        isFetching = true

        currentEventObserver = getQueryFetchLD(
            limit,
            false,
            false
        ).observeSingleEvent {
            isFetching = false
            it?.data?.map {
                map(it)
            }?.also {
                onData(it)
            } ?: onError(
                it?.exception ?: DataException(DataException.Type.UNEXPECTED, "Unexpected error")
            )
        }

        return true
    }

    override fun startDynamicFetch(
        limit: Long,
        onData: (List<T>) -> Unit,
        onError: (DataException) -> Unit
    ): Boolean {
        if (isFetching || isDynamicFetchEnabled) {
            return false
        }

        dynamicDataListener = DynamicDataListener(
            limit,
            onData,
            onError
        ).also {
            it.start()
        }

        isDynamicFetchEnabled = true
        return true
    }

    override fun stopDynamicFetch() {
        dynamicDataListener?.also { it.stop() }
        isDynamicFetchEnabled = false
    }

    private fun getQueryFetchLD(limit: Long, isRealtime: Boolean, includeMetadataChanges: Boolean) = QueryFetchLD(
        getQuery(limit),
        isRealtime,
        includeMetadataChanges
    ).map {
        it?.data?.also { setPage(it) }
        it
    }

    inner class DynamicDataListener(
        private val limit: Long,
        onData: (List<T>) -> Unit,
        onError: (DataException) -> Unit
    ) {

        private val dataObserver = Observer<Document<List<DataMap>>> {
            if (it?.metadata?.hasPendingWrites == true) {
                return@Observer
            }

            it?.data?.map{
                map(it)
            }?.also {
                onData(it)
                updateDataLD()
            } ?: onError(
                it?.exception ?: DataException(DataException.Type.UNEXPECTED, "Unexpected error")
            )
        }
        private var currentDataLD: LiveData<Document<List<DataMap>>>? = null

        fun start() {
            updateDataLD()
        }

        fun stop() {
            currentDataLD?.also {
                it.removeObserver(dataObserver)
            }
        }

        private fun updateDataLD() {
            currentDataLD?.also {
                it.removeObserver(dataObserver)
            }
            currentDataLD = getQueryFetchLD(limit, true, true).also {
                it.observeForever(dataObserver)
            }
        }

    }

}

open class FirestoreLiveDataSource<T>(
    query: Query,
    order: List<Order>?,
    private val map: (DataMap) -> LiveData<T>
) : FirestoreDataSource<LiveData<T>>(query, order) {

    private var dynamicDataListener: DynamicDataListener? = null

    override fun fetchNext(limit: Long, onData: (List<LiveData<T>>) -> Unit, onError: (DataException) -> Unit): Boolean {
        if (isFetching || isDynamicFetchEnabled) {
            return false
        }
        isFetching = true

        currentEventObserver = getQueryFetchLD(
            limit,
            false,
            false
        ).observeSingleEvent {
            isFetching = false
            it?.data?.also {
                onData(it)
            } ?: onError(
                it?.exception ?: DataException(DataException.Type.UNEXPECTED, "Unexpected error")
            )
        }

        return true
    }

    override fun startDynamicFetch(
        limit: Long,
        onData: (List<LiveData<T>>) -> Unit,
        onError: (DataException) -> Unit
    ): Boolean {
        if (isFetching || isDynamicFetchEnabled) {
            return false
        }

        dynamicDataListener = DynamicDataListener(
            limit,
            onData,
            onError
        ).also {
            it.start()
        }

        isDynamicFetchEnabled = true
        return true
    }

    override fun stopDynamicFetch() {
        dynamicDataListener?.also { it.stop() }
        isDynamicFetchEnabled = false
    }

    private fun getQueryFetchLD(limit: Long, isRealtime: Boolean, includeMetadataChanges: Boolean) = QueryFetchLD(
        getQuery(limit),
        isRealtime,
        includeMetadataChanges
    ).switchMap {
        it?.data?.apply { setPage(this) }
        object : SyncMediatorLiveData<Document<List<LiveData<T>>>>() {

            override val syncValue = it?.map { it.map(map) }

            init {
                syncValue?.data?.forEach { addSource(it) }
                sync()
            }

        }
    }

    inner class DynamicDataListener(
        private val limit: Long,
        onData: (List<LiveData<T>>) -> Unit,
        onError: (DataException) -> Unit
    ) {

        private val dataObserver = Observer<Document<List<LiveData<T>>>> {
            if (it?.metadata?.hasPendingWrites == true) {
                return@Observer
            }

            it?.data?.also {
                onData(it)
                updateDataLD()
            } ?: onError(
                it?.exception ?: DataException(DataException.Type.UNEXPECTED, "Unexpected error")
            )
        }
        private var currentDataLD: LiveData<Document<List<LiveData<T>>>>? = null

        fun start() {
            updateDataLD()
        }

        fun stop() {
            currentDataLD?.also {
                it.removeObserver(dataObserver)
            }
        }

        private fun updateDataLD() {
            currentDataLD?.also {
                it.removeObserver(dataObserver)
            }
            currentDataLD = getQueryFetchLD(limit, true, true).also {
                it.observeForever(dataObserver)
            }
        }

    }

}
