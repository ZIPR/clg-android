package com.zipr.clg.data

import android.graphics.Bitmap
import android.net.Uri
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageReference
import java.io.ByteArrayOutputStream
import java.util.UUID

const val MAX_UPLOAD_RETRY_TIME = 10000L /* 10 sec */
private const val BITMAP_QUALITY = 100

val firebaseStorage get() = FirebaseStorage.getInstance()

fun FirebaseStorage.challenge(challengeId: String) = StorageChallengeRef(
    reference.child("challenges").child(challengeId)
)

fun getUserAvatarPath(id: String) = "users/$id/avatar/${UUID.randomUUID()}.jpg"

fun uploadBitmap(storageReference: StorageReference, bitmap: Bitmap) = AsyncResult.fromUploadTaskWithUrl(
    storageReference.putBytes(
        ByteArrayOutputStream().use {
            bitmap.compress(Bitmap.CompressFormat.JPEG, BITMAP_QUALITY, it)
            it.toByteArray()
        }
    ),
    { snapshot, exception ->
        AsyncResult(
            snapshot, exception?.let { DataException.fromStorageException(it as? StorageException) }
        )
    }
)

fun uploadFile(storageReference: StorageReference, uri: Uri) = AsyncResult.fromUploadTaskWithUrl(
    storageReference.putFile(uri),
    { snapshot, exception ->
        AsyncResult(
            snapshot, exception?.let { DataException.fromStorageException(it as? StorageException) }
        )
    }
)

class StorageChallengeRef(private val challengeReference: StorageReference) {

    val image get() = challengeReference.child("image")
    
    fun descriptionImage(index: Int) = challengeReference.child("desc_images").child("$index-image")

    fun descriptionVideo(index: Int) = challengeReference.child("desc_videos").child("$index-video")

    fun descriptionVideoThumbnail(index: Int) = challengeReference.child("desc_videos").child("$index-thumbnail")
    
    fun post(postId: String) = StoragePostRef(challengeReference, postId)
    
    class StoragePostRef(challengeReference: StorageReference, id: String) {
        
        private val postReference = challengeReference.child("posts").child(id)
    
        fun image(index: Int) = postReference.child("images").child("$index-image")
    
        fun video(index: Int) = postReference.child("videos").child("$index-video")
    
        fun videoThumbnail(index: Int) = postReference.child("videos").child("$index-thumbnail")
        
    }

}
