package com.zipr.clg.data.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.firestore.GeoPoint
import com.zipr.clg.util.invoke

data class Location(
    val geoPoint: GeoPoint?,
    val name: String?,
    val address: String?
) : Parcelable {

    constructor(map: DataMap): this(
        map(Location::geoPoint.name),
        map(Location::name.name),
        map(Location::address.name)
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        geoPoint?.also { dataMap[Location::geoPoint.name] = it }
        name?.also { dataMap[Location::name.name] = it }
        address?.also { dataMap[Location::address.name] = it }
        return dataMap
    }

    constructor(parcel: Parcel) : this(
        GeoPoint(parcel.readDouble(), parcel.readDouble()),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        geoPoint!!.also {
            parcel.writeDouble(it.latitude)
            parcel.writeDouble(it.longitude)
        }
        name.orEmpty().also { parcel.writeString(it) }
        address.orEmpty().also { parcel.writeString(it) }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {

        override fun createFromParcel(parcel: Parcel) = Location(parcel)

        override fun newArray(size: Int) = arrayOfNulls<Location>(size)

    }

}
