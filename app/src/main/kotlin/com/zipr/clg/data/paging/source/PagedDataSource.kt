package com.zipr.clg.data.paging.source

import com.zipr.clg.data.DataException

interface PagedDataSource<out T> {

    fun fetchNext(limit: Long, onData: (List<T>) -> Unit, onError: (DataException) -> Unit): Boolean

    fun reset()

    fun startDynamicFetch(limit: Long, onData: (List<T>) -> Unit, onError: (DataException) -> Unit): Boolean

    fun stopDynamicFetch()

}