package com.zipr.clg.data

import android.arch.lifecycle.LiveData
import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.experimental.Deferred

data class AsyncResult<out R, E : Throwable>(val result: R? = null, val error: E? = null) {

    companion object {

        fun <R, E : Throwable> fromTask(
            task: Task<R>,
            factory: (R?, Exception?) -> AsyncResult<R, E>
        ) = object : LiveData<AsyncResult<R, E>>() {

            init {
                task.addOnCompleteListener {
                    postValue(
                        if (it.isSuccessful) {
                            factory(it.result, null)
                        } else {
                            factory(null, it.exception)
                        }
                    )
                }
            }

        }

        fun <E : Throwable> fromUploadTask(
            task: UploadTask,
            factory: (UploadTask.TaskSnapshot?, Exception?) -> AsyncResult<UploadTask.TaskSnapshot, E>
        ) = object : LiveData<AsyncResult<UploadTask.TaskSnapshot, E>>() {

            override fun onActive() {
                task.addOnCompleteListener {
                    postValue(
                        if (it.isSuccessful) {
                            factory(it.result, null)
                        } else {
                            factory(null, it.exception)
                        }
                    )
                }
            }

            override fun onInactive() {
                if (!task.isComplete) {
                    task.cancel()
                }
            }

        }

        fun <E : Throwable> fromUploadTaskWithUrl(
            task: UploadTask,
            factory: (Uri?, Exception?) -> AsyncResult<Uri, E>
        ) = object : LiveData<AsyncResult<Uri, E>>() {

            override fun onActive() {
                task.continueWithTask {
                    if (it.isSuccessful)  task.snapshot.storage.downloadUrl
                    else throw it.exception!!
                }.addOnCompleteListener {
                    postValue(
                        if (it.isSuccessful) {
                            factory(it.result, null)
                        } else {
                            factory(null, it.exception)
                        }
                    )
                }
            }

            override fun onInactive() {
                if (!task.isComplete) {
                    task.cancel()
                }
            }

        }

        fun <R, E : Throwable> fromDeferred(
            deferred: Deferred<R>,
            factory: (R?, Throwable?) -> AsyncResult<R, E>
        ) = object : LiveData<AsyncResult<R, E>>() {

            override fun onActive() {
                deferred.invokeOnCompletion(onCancelling = false) {
                    postValue(
                        if (it == null) {
                            factory(deferred.getCompleted(), null)
                        } else {
                            factory(null, it)
                        }
                    )
                }
            }

            override fun onInactive() {
                if (!deferred.isCompleted) {
                    deferred.cancel()
                }
            }

        }

    }

}

