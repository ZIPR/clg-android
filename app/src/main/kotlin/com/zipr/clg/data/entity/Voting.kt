package com.zipr.clg.data.entity

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.zipr.clg.util.invoke
import java.util.Date

@IgnoreExtraProperties
data class Voting(
    val post: Post?,
    @ServerTimestamp val voteDate: Date?,
    @get:Exclude val id: String?
) {

    constructor(map: DataMap): this(
        map(Voting::post.name, ::Post),
        map(Voting::voteDate.name),
        map.id
    )

}