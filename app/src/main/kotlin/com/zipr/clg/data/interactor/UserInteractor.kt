package com.zipr.clg.data.interactor

import android.arch.lifecycle.LiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query
import com.zipr.clg.data.entity.*
import com.zipr.clg.data.firestore
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.data.paging.provider.PagedLiveDataProvider
import com.zipr.clg.data.paging.provider.QueryDataProvider
import com.zipr.clg.data.paging.source.FirestoreLiveDataSource
import com.zipr.clg.data.paging.source.Order
import com.zipr.clg.data.repository.DocumentFetchLD
import com.zipr.clg.data.repository.QueryFetchLD
import com.zipr.clg.data.repository.SyncFetchLD
import com.zipr.clg.data.repository.by
import com.zipr.clg.data.users
import com.zipr.clg.util.letTyped
import com.zipr.clg.util.map
import javax.inject.Singleton

@Singleton
class UserInteractor {

    fun getUser(userId: String, isRealtime: Boolean): LiveData<Document<User?>> =
        DocumentFetchLD(firestore.users.user(userId), isRealtime).map {
            it?.map(::User)
        }

    fun getUserByUsername(username: String, isRealtime: Boolean): LiveData<Document<User?>> =
        QueryFetchLD(
            firestore.users.query().whereEqualTo(User::username.name, username).limit(1),
            isRealtime
        ).map {
            it?.map { it.firstOrNull()?.let(::User) }
        }

    fun getUserFollower(userId: String, followerId: String, isUserInfoRealtime: Boolean, isFollowerRealtime: Boolean) =
        SyncFetchLD(
            listOf(
                UserFollower::user.name by {
                    DocumentFetchLD(firestore.users.user(userId), isUserInfoRealtime).map {
                        it?.data
                    }
                },
                UserFollower::follower.name by {
                    DocumentFetchLD(firestore.users.following(followerId).user(userId), isFollowerRealtime).map {
                        it?.data
                    }
                }
            ),
            mutableMapOf()
        ).map {
            it?.let(::UserFollower)
        }

    fun getUsers(
        fetchLimit: Long,
        followStatusUserId: String,
        isFollowStatusRealtime: Boolean
    ): QueryDataProvider<UserFollower> = getUsersQueryDataProvider(
        firestore.users.query(),
        listOf(
            Order(User::followersCount.name, Query.Direction.DESCENDING)
        ),
        fetchLimit,
        followStatusUserId,
        isFollowStatusRealtime
    )

    fun getFollowers(
        userId: String,
        followStatusUserId: String,
        fetchLimit: Long,
        isUserInfoRealtime: Boolean,
        isFollowStatusRealtime: Boolean
    ): DataProvider<UserFollower> = getUserFollowers(
        firestore.users.followers(userId).query(),
        listOf(
            Order(Follower::date.name, Query.Direction.DESCENDING)
        ),
        fetchLimit,
        followStatusUserId,
        isUserInfoRealtime,
        isFollowStatusRealtime
    )

    fun getFollowing(
        userId: String,
        followStatusUserId: String,
        fetchLimit: Long,
        isUserInfoRealtime: Boolean,
        isFollowStatusRealtime: Boolean
    ): DataProvider<UserFollower> = getUserFollowers(
        firestore.users.following(userId).query(),
        listOf(
            Order(Follower::date.name, Query.Direction.DESCENDING)
        ),
        fetchLimit,
        followStatusUserId,
        isUserInfoRealtime,
        isFollowStatusRealtime
    )

    private fun getUsersQueryDataProvider(
        query: Query,
        order: List<Order>,
        fetchLimit: Long,
        followStatusUserId: String,
        isFollowStatusRealtime: Boolean
    ): QueryDataProvider<UserFollower> =
        object : PagedLiveDataProvider<UserFollower>(fetchLimit, false, null), QueryDataProvider<UserFollower> {

            override var query: String? = null
                set(value) {
                    field = value
                    setDataSource(
                        FirestoreLiveDataSource(
                            query.run {
                                if (value?.isNotBlank() == true) whereEqualTo(User::username.name, value)
                                else this
                            },
                            order,
                            { userDataMap ->
                                val user = User(userDataMap)
                                DocumentFetchLD(firestore.users.following(followStatusUserId).user(userDataMap.id), isFollowStatusRealtime).map {
                                    UserFollower(user, it?.data?.let(::Follower))
                                }
                            }
                        )
                    )
                }

        }

    private fun getUserFollowers(
        query: Query,
        order: List<Order>,
        fetchLimit: Long,
        followStatusUserId: String,
        isUserInfoRealtime: Boolean,
        isFollowStatusRealtime: Boolean
    ): DataProvider<UserFollower> =
        PagedLiveDataProvider(
            fetchLimit,
            false,
            FirestoreLiveDataSource(
                query,
                order,
                {
                    SyncFetchLD(
                        listOf(
                            (Follower::user.name to UserFollower::user.name).by(letTyped { documentReference: DocumentReference ->
                                DocumentFetchLD(documentReference, isUserInfoRealtime).map {
                                    it?.data
                                }
                            }),
                            (it.FIELD_ID to UserFollower::follower.name).by(letTyped { followerId: String ->
                                DocumentFetchLD(
                                    firestore.users.following(followStatusUserId).user(followerId),
                                    isFollowStatusRealtime,
                                    listOf(Follower::date.name)
                                ).map {
                                    it?.data
                                }
                            })
                        ),
                        it
                    ).map {
                        it?.let(::UserFollower)
                    }
                }
            )
        )

    fun setUser(user: User): Task<Void> {
        if (user.id == null) throw IllegalArgumentException("User id must not be null")

        return firestore.users.user(user.id).set(
            userToUpdateMap(user)
        )
    }

    fun updateUser(user: User): Task<Void> {
        if (user.id == null) throw IllegalArgumentException("User id must not be null")

        return firestore.users.user(user.id).update(
            userToUpdateMap(user)
        )
    }

    fun follow(userId: String, followerId: String) {
        firestore.batch()
            .set(
                firestore.users.followers(userId).user(followerId),
                mapOf(
                    Follower::user.name to firestore.users.user(followerId),
                    Follower::date.name to FieldValue.serverTimestamp()
                )
            ).set(
                firestore.users.following(followerId).user(userId),
                mapOf(
                    Follower::user.name to firestore.users.user(userId),
                    Follower::date.name to FieldValue.serverTimestamp()
                )
            ).commit()
    }

    fun unfollow(userId: String, followerId: String) {
        firestore.batch()
            .delete(firestore.users.followers(userId).user(followerId))
            .delete(firestore.users.following(followerId).user(userId))
            .commit()
    }

    private fun userToUpdateMap(user: User) = user.toDataMap().also {
        it.remove(user::followingCount.name)
        it.remove(user::followersCount.name)
    }

}