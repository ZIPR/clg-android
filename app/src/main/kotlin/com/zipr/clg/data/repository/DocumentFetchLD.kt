package com.zipr.clg.data.repository

import com.google.firebase.firestore.*
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.DataMap
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.id
import com.zipr.clg.data.getFirestoreNotFoundMessage
import com.zipr.clg.data.getFirestoreUnavailableMessage
import com.zipr.clg.util.mapEquals

class DocumentFetchLD(
    private val documentReference: DocumentReference,
    private val isRealtime: Boolean,
    private val ignoredFields: List<String>? = null,
    private val includeMetadataChanges: Boolean = false
) : FetchLD<DataMap, DocumentSnapshot>() {

    private lateinit var listenerRegistration: ListenerRegistration
    private var currentSnapshot: DocumentSnapshot? = null

    override val snapshotListener =
        EventListener<DocumentSnapshot> { snapshot: DocumentSnapshot?, exception: FirebaseFirestoreException? ->
            if (currentSnapshot == null || currentSnapshot != snapshot) {
                currentSnapshot = snapshot
            } else {
                return@EventListener
            }

            val data = snapshot?.data?.apply {
                id = snapshot.id
                ignoredFields?.forEach { remove(it) }
            }
            val metadata = snapshot?.metadata?.let { Document.Metadata(it.isFromCache, it.hasPendingWrites()) }
            if (exception == null && data != null) {
                if (ignoredFields?.isNotEmpty() == true && value?.data?.mapEquals(data) == true) {
                    return@EventListener
                }
                value = Document(data, metadata, null)
            } else {
                value = Document(
                    null,
                    null,
                    when {
                        exception != null -> DataException.fromFirebaseFirestoreException(exception)

                        metadata?.isFromCache == true -> DataException(
                            DataException.Type.UNAVAILABLE,
                            getFirestoreUnavailableMessage()
                        )

                        else -> DataException(
                            DataException.Type.NOT_FOUND,
                            getFirestoreNotFoundMessage(snapshot?.reference.toString())
                        )
                    }
                )
            }
        }

    init {
        if (!isRealtime) {
            observeFetchTask(documentReference.get())
        }
    }

    override fun onActive() {
        if (isRealtime) {
            listenerRegistration = documentReference.addSnapshotListener(
                if (includeMetadataChanges) MetadataChanges.INCLUDE else MetadataChanges.EXCLUDE,
                snapshotListener
            )
        }
    }

    override fun onInactive() {
        if (isRealtime) {
            listenerRegistration.remove()
        }
    }

}