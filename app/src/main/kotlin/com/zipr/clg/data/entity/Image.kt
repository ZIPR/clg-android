package com.zipr.clg.data.entity

import com.zipr.clg.util.invoke

data class Image(
    val url: String?,
    val aspectRatio: Float?
) {

    constructor(map: DataMap): this(
        map(Image::url.name),
        map(Image::aspectRatio.name) { it: Double -> it.toFloat() }
    )

    fun toDataMap(): DataMap {
        val dataMap: DataMap = mutableMapOf()
        url?.also { dataMap[Image::url.name] = it }
        aspectRatio?.also { dataMap[Image::aspectRatio.name] = it }
        return dataMap
    }
    
}