package com.zipr.clg.ui.controller

import android.os.Bundle

private const val KEY_STATE = "KEY_STATE_CONTROLLER"

interface StateAct<in T> {
    val state: T.() -> Unit
}

open class StateController<in T, S : StateAct<T>>(
    private val defaultState: S,
    private val stateOf: (String) -> S
) {
    private val stateKey by lazy { "$KEY_STATE(${this::class.qualifiedName})" }

    lateinit var currentState: S
        private set
    
    fun T.restoreState(savedInstanceState: Bundle?) {
        setState(
            if (savedInstanceState?.containsKey(stateKey) == true) {
                stateOf(savedInstanceState.getString(stateKey))
            } else {
                defaultState
            }
        )
    }
    
    fun saveState(savedInstanceState: Bundle) {
        savedInstanceState.putString(stateKey, currentState.toString())
    }
    
    fun T.setState(newState: S) {
        currentState = newState
        currentState.state(this)
    }
    
    fun T.resetState() {
        setState(defaultState)
    }
    
}