package com.zipr.clg.ui.adapter.post

import android.net.Uri
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.entity.Post
import com.zipr.clg.ui.adapter.*
import com.zipr.clg.ui.widget.ThumbnailPlayerView

class PostContentAdapter private constructor(
    private val descTxtAdapter: TextAdapter,
    private val descPicAdapter: ImageAdapter,
    private val descVidAdapter: VideoAdapter,
    private val locationAdapter: LocationAdapter
) : SegmentAdapter(
    descTxtAdapter,
    descPicAdapter,
    descVidAdapter,
    locationAdapter
) {
    
    fun setPost(post: Post) {
        descTxtAdapter.set(post.contentText)
        descPicAdapter.set(post.contentImages)
        descVidAdapter.set(post.contentVideos)
        locationAdapter.set(post.location)
        descTxtAdapter.isVisible = true
    }
    
    companion object {
        
        fun create(
            onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
            onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
            onClickLocation: (location: Location) -> Unit
        ) = PostContentAdapter(
            TextAdapter(),
            ImageAdapter(),
            VideoAdapter(onBindPlayerViewTag, onClickPlayerView),
            LocationAdapter(onClickLocation)
        )
        
    }
    
}