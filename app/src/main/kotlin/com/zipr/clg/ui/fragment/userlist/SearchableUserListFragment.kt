package com.zipr.clg.ui.fragment.userlist

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.paging.provider.QueryDataProvider
import com.zipr.clg.ui.controller.BackPressHandler
import com.zipr.clg.ui.controller.RefreshStateHandler
import com.zipr.clg.ui.controller.SearchQueryHandler
import com.zipr.clg.util.BackpressureData
import kotlinx.android.synthetic.main.abc_search_view.view.*
import kotlinx.android.synthetic.main.fragment_searchable_userlist.*
import kotlinx.android.synthetic.main.fragment_userlist.*
import kotlin.properties.Delegates

private const val SEARCH_INSTANT_TIME_PERIOD_MILLIS = 500L

abstract class SearchableUserListFragment<VM : UserListViewModel<QueryDataProvider<UserFollower>>> :
    UserListFragment<VM>(),
    SearchQueryHandler,
    RefreshStateHandler,
    BackPressHandler {
    
    private val queryLD = BackpressureData<String>(SEARCH_INSTANT_TIME_PERIOD_MILLIS)
    
    private lateinit var searchIconIv: ImageView
    private var isSearchDefaultState: Boolean by Delegates.observable(true) { _, _, new ->
        searchIconIv.apply {
            isEnabled = !new
            setImageResource(
                if (new) R.drawable.ic_search
                else R.drawable.ic_arrow_back
            )
        }
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_searchable_userlist, container, false)
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        
        searchIconIv = searchView.search_mag_icon
        searchIconIv.setOnClickListener {
            onRefreshState()
        }
        
        searchView.apply {
            activity?.also { setSearchableInfo(ClgApplication.getSearchableInfo(it)) }
            isIconified = false
            clearFocus()
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }
            
                override fun onQueryTextChange(newText: String?): Boolean {
                    queryLD.postValue(newText)
                    return false
                }
            
            })
        }
        
        usersRv.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
                    searchView.clearFocus()
                }
            }
            
        })
        
        queryLD.observe(viewLifecycleOwner, Observer {
            it?.also { onSearchQuery(it) }
        })
        isSearchDefaultState = userListViewModel.dataProvider.query.isNullOrBlank()
    }
    
    override fun onSearchQuery(query: String) {
        val trimmedQuery = query.trim()
        isSearchDefaultState = trimmedQuery.isEmpty()
        if (userListViewModel.dataProvider.query == trimmedQuery) {
            return
        }
        if (!query.contentEquals(searchView.query)) {
            searchView.setQuery(query, false)
        }
        usersRv.scrollToPosition(0)
        userListViewModel.dataProvider.query = trimmedQuery
    }
    
    override fun onRefreshState(): Boolean =
        if (!isSearchDefaultState) {
            isSearchDefaultState = true
            searchView.setQuery(null, false)
            searchView.clearFocus()
            userListViewModel.dataProvider.query = null
            true
        } else {
            false
        }
    
    override fun handleBackPress(): Boolean = onRefreshState()
    
    override fun onRetry(view: View) {
        super.onRetry(view)
        searchView.clearFocus()
    }
    
}