package com.zipr.clg.ui.adapter

import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.zipr.clg.R
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.util.cancelAndPost
import com.zipr.clg.util.runOrPostIfComputingLayout
import com.zipr.clg.util.toPx
import kotlinx.android.synthetic.main.item_pagination_status.view.*
import kotlin.properties.Delegates

abstract class PagedAdapter<out E, T : RecyclerView.ViewHolder>(
    private val pagedDataProvider: DataProvider<E>,
    private val lifecycleRegistrar: LifecycleRegistrar
) : RecyclerViewAdapter<T>() {
    
    init {
        lifecycleRegistrar.registerImmediately(
            object : DataProvider.DataListener {

                override fun onDataInserted(positionStart: Int, itemCount: Int) {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeInserted(positionStart, itemCount)
                    }
                }

                override fun onDataRemoved(positionStart: Int, itemCount: Int) {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeRemoved(positionStart, itemCount)
                    }
                }

                override fun onDataChanged(positionStart: Int, itemCount: Int) {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeChanged(positionStart, itemCount)
                    }
                }

            },
            pagedDataProvider::registerDataListener,
            pagedDataProvider::unregisterDataListener
        )
    }
    
    final override fun getItemCount() = pagedDataProvider.getItemCount()

    protected fun getItem(position: Int) = pagedDataProvider.getItem(position)

    fun withPaginationStatus(statusTopMarginDp: Int = 0, statusBottomMarginDp: Int = 0): RecyclerViewAdapter<*> =
        SegmentAdapter(
            this,
            PaginationController(
                statusTopMarginDp.toPx(),
                statusBottomMarginDp.toPx(),
                { pagedDataProvider.retryFetching() }
            ).also {
                lifecycleRegistrar.registerImmediately(
                    it,
                    pagedDataProvider::registerStatusListener,
                    pagedDataProvider::unregisterStatusListener
                )
            }
        )

    private class PaginationController(
        private val statusTopMarginPx: Int,
        private val statusBottomMarginPx: Int,
        private var retryAction: () -> Unit
    ) : RecyclerViewAdapter<PaginationController.ViewHolderPaginationStatus>(),
        DataProvider.StatusListener {
        
        private val handler = Handler(Looper.getMainLooper())
        private val notifyItemInsertedAction = Runnable { notifyItemInserted(0) }
        private val notifyItemRemovedAction = Runnable { notifyItemRemoved(0) }
        private val notifyItemChangedAction = Runnable { notifyItemChanged(0) }
    
        private var isFetching = false
        private var isError = false
        private var isPaginationStatusVisible by Delegates.observable<Boolean?>(null) { _, old, new ->
            if (old == null || new == null) return@observable
            if (new != old) {
                if (new) handler.cancelAndPost(notifyItemInsertedAction)
                else handler.cancelAndPost(notifyItemRemovedAction)
            } else if (old && !handler.hasMessages(0)) {
                handler.post(notifyItemChangedAction)
            }
        }

        override fun onStatusChange(
            isFetching: Boolean,
            isRefreshing: Boolean,
            isFullResult: Boolean,
            isEmpty: Boolean,
            isError: Boolean
        ) {
            this@PaginationController.isFetching = isFetching
            this@PaginationController.isError = isError
            this@PaginationController.isPaginationStatusVisible = !isFullResult && !isEmpty && !isRefreshing
        }

        override fun getItemCount() = if (isPaginationStatusVisible!!) 1 else 0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolderPaginationStatus(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pagination_status, parent, false).also {
                (it.layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
                    topMargin = statusTopMarginPx
                    bottomMargin = statusBottomMarginPx
                }
            },
            retryAction
        )
    
        override fun onBindViewHolder(holder: ViewHolderPaginationStatus, position: Int) {
            holder.bind(isFetching, isError)
        }
    
        class ViewHolderPaginationStatus(viewGroup: View, retryAction: () -> Unit) : RecyclerView.ViewHolder(viewGroup) {
        
            private val progressBar = viewGroup.progressBar as ProgressBar
            private val errorFetchLayout = viewGroup.errorFetchLayout as ViewGroup
            
            fun bind(isFetching: Boolean, isError: Boolean) {
                progressBar.visibility = if (isFetching) View.VISIBLE else View.INVISIBLE
                errorFetchLayout.visibility = if (isError) View.VISIBLE else View.INVISIBLE
            }

            init {
                errorFetchLayout.retryTv.setOnClickListener{ retryAction() }
            }
            
        }

    }
    
}
