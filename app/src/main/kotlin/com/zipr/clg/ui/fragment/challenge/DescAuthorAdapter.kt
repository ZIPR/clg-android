package com.zipr.clg.ui.fragment.challenge

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.TextView.BufferType
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.entity.User
import com.zipr.clg.ui.adapter.RecyclerViewAdapter
import com.zipr.clg.util.runOrPostIfComputingLayout
import kotlinx.android.synthetic.main.item_challenge_desc_author.view.*
import java.util.Date

class DescAuthorAdapter(
    private val onAuthorClick: (username: String) -> Unit
) : RecyclerViewAdapter<DescAuthorAdapter.AuthorViewHolder>() {

    private var author: User? = null
    private var date: Date? = null

    fun set(author: User?, date: Date?) {
        this.author = author
        this.date = date
        recyclerView.runOrPostIfComputingLayout {
            notifyItemChanged(0)
        }
    }

    override fun getItemCount() = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AuthorViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_desc_author, parent, false)
    )
    
    override fun onBindViewHolder(holder: AuthorViewHolder, position: Int) =
        holder.bind(author, date, onAuthorClick)

    class AuthorViewHolder(viewGroup: View) : RecyclerView.ViewHolder(viewGroup) {

        val dateTv = itemView.dateTv as TextView
        val authorTv = itemView.authorTv as TextView

        fun bind(author: User?, date: Date?, onAuthorClick: (String) -> Unit) {
            dateTv.text = if (date != null) ClgApplication.dateFormat.format(date) else ""

            if (author?.username == null) {
                return
            }

            val authorSpn = SpannableString(ClgApplication.getString(
                R.string.challenge_author,
                author.username
            ))
            val authorClickSpn = object : ClickableSpan() {

                override fun onClick(textView: View) {
                    author.id?.also(onAuthorClick)
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.isUnderlineText = false
                }

            }
            authorSpn.setSpan(
                authorClickSpn,
                authorSpn.length - author.username.length - 1,
                authorSpn.length,
                0
            )
            authorSpn.setSpan(
                ForegroundColorSpan(Color.BLUE),
                authorSpn.length - author.username.length - 1,
                authorSpn.length,
                0
            )

            authorTv.apply {
                movementMethod = LinkMovementMethod.getInstance()
                setText(authorSpn, BufferType.SPANNABLE)
            }
        }

    }

}