package com.zipr.clg.ui.fragment.challenge

import android.arch.lifecycle.MutableLiveData
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.ui.adapter.RecyclerViewAdapter
import com.zipr.clg.util.overrideColor

class TagAdapter : RecyclerViewAdapter<TagAdapter.TagViewHolder>() {

    private var tags: List<String>? = null
    private val tagsColor = MutableLiveData<Int>()

    fun setTags(tags: List<String>?) {
        this.tags = tags
        dataItemCount = tags?.size ?: 0
    }

    fun setTagsColor(color: Int) {
        tagsColor.postValue(color)
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TagViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_tag, parent, false)
    ).apply {
        tagsColor.observeForever({
            it?.also { tagTV.setTextColor(it) }
        })
        tagTV.background.overrideColor(
            ClgApplication.getColor(R.color.grey_200)
        )
    }

    override fun onBindViewHolder(holder: TagViewHolder, position: Int) =
        holder.bind(tags!![position])

    class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tagTV = itemView as TextView

        fun bind(tag: String) {
            tagTV.text = tag
        }

    }

}