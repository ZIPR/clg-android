package com.zipr.clg.ui.fragment.challenge

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.zipr.clg.R
import com.zipr.clg.data.entity.Participant
import com.zipr.clg.data.entity.Participating
import com.zipr.clg.ui.adapter.RecyclerViewAdapter
import com.zipr.clg.ui.widget.ActiveButton
import com.zipr.clg.util.orZero
import com.zipr.clg.util.runOrPostIfComputingLayout
import kotlinx.android.synthetic.main.item_challenge_desc_join_participants.view.*

class DescJoinParticipantsAdapter(
    private val onJoinClick: () -> Unit,
    private val onLeaveClick: () -> Unit,
    private val onParticipantsPreviewClick: () -> Unit
) : RecyclerViewAdapter<DescJoinParticipantsAdapter.JoinParticipantsViewHolder>() {

    private var participating: Participating? = null
    private var participantsCount: Long? = null
    private var participantsPreview: List<Participant>? = null

    fun set(participating: Participating?, participantsCount: Long?, participantsPreview: List<Participant>?) {
        this.participating = participating
        this.participantsCount = participantsCount
        this.participantsPreview = participantsPreview
        recyclerView.runOrPostIfComputingLayout {
            notifyItemChanged(0)
        }
    }

    override fun getItemCount() = 1
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = JoinParticipantsViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_desc_join_participants, parent, false)
    ).apply {
        joinBtn.setOnClickListener {
            if (joinBtn.isActive) onJoinClick()
            else onLeaveClick()
        }
        itemView.participantsPreviewFl.setOnClickListener { onParticipantsPreviewClick() }
    }

    override fun onBindViewHolder(holder: JoinParticipantsViewHolder, position: Int) =
        holder.bind(participating, participantsCount, participantsPreview)

    class JoinParticipantsViewHolder(viewGroup: View) : RecyclerView.ViewHolder(viewGroup) {

        val joinBtn = itemView.joinBtn as ActiveButton
        val participantCountTv = itemView.participantsCountTv as TextView
        val user1Dv = itemView.user1Dv as SimpleDraweeView
        val user2Dv = itemView.user2Dv as SimpleDraweeView
        val user3Dv = itemView.user3Dv as SimpleDraweeView

        fun bind(participating: Participating?, participantsCount: Long?, participantsPreview: List<Participant>?) {
            if (participating == null && participantsCount == null && participantsPreview == null) {
                itemView.visibility = View.INVISIBLE
                return
            } else {
                itemView.visibility = View.VISIBLE
            }

            joinBtn.apply {
                if (participating == null) {
                    text = resources.getString(R.string.join)
                    isActive = true
                } else {
                    text = resources.getString(R.string.joined)
                    isActive = false
                }
            }
            
            participantCountTv.text = participantsCount?.toString()
    
            participantsPreview?.size.orZero().also {
                user1Dv.visibility = if (it > 0) View.VISIBLE else View.GONE
                user2Dv.visibility = if (it > 1) View.VISIBLE else View.GONE
                user3Dv.visibility = if (it > 2) View.VISIBLE else View.GONE
            }

            participantsPreview?.apply {
                getOrNull(0)?.user?.photoUrl?.also {
                    itemView.user1Dv.setImageURI(it)
                }
                getOrNull(1)?.user?.photoUrl?.also {
                    itemView.user2Dv.setImageURI(it)
                }
                getOrNull(2)?.user?.photoUrl?.also {
                    itemView.user3Dv.setImageURI(it)
                }
            }
        }

    }

}