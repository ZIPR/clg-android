package com.zipr.clg.ui.adapter

import android.graphics.drawable.Animatable
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.common.RotationOptions
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.zipr.clg.R
import com.zipr.clg.ui.common.SourceImageLD
import com.zipr.clg.ui.common.SourceVideo
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.*
import kotlinx.android.synthetic.main.item_video_editable.view.*
import kotlin.properties.Delegates

class EditableVideoAdapter(
    videoUriList: List<String>,
    private val onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
    private val onClickPlayerView: (ThumbnailPlayerView?, Uri?) -> Unit
) : RecyclerViewAdapter<EditableVideoAdapter.VidViewHolder>() {
    
    private val _sourceVideoList = mutableListOf<SourceVideo>()
    
    val sourceVideoList get() = _sourceVideoList as List<SourceVideo>
    val videoUriList get() = _sourceVideoList.map { it.uri.toString() }

    var isRemoveEnabled by Delegates.observable(true) { _, _, _ ->
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRangeChanged(0, _sourceVideoList.size)
        }
    }
    
    init {
        videoUriList.forEach {
            Uri.parse(it).also {
                _sourceVideoList.add(
                    SourceVideo(it, SourceImageLD(makeImageRequest(it)))
                )
            }
        }
    }
    
    fun addVideo(uri: Uri) {
        if (_sourceVideoList.any { it.uri == uri }) {
            return
        }
        SourceVideo(uri, SourceImageLD(makeImageRequest(uri))).also {
            if (_sourceVideoList.add(it)) {
                val index = _sourceVideoList.lastIndex
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemInserted(index)
                }
            }
        }
    }
    
    private fun removeVideo(index: Int) {
        _sourceVideoList.removeAt(index)
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRemoved(index)
        }
    }

    override fun getItemCount() = _sourceVideoList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VidViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_video_editable, parent, false)
    ).apply {
        playerView.scrimView.setOnClickListener {
            onClickPlayerView(playerView, sourceVideo.uri)
        }
        removeIv.setOnClickListener {
            _sourceVideoList.indexOf(sourceVideo).also {
                if (it != -1) {
                    if (playerView.player != null) {
                        onClickPlayerView(null, null)
                    }
                    removeVideo(it)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: VidViewHolder, position: Int) {
        holder.apply {
            bind(_sourceVideoList[position])
        }.apply {
            playerView.tag = "${this::class.simpleName}($position)-${sourceVideo.uri}"
            onBindPlayerViewTag(playerView)
            removeIv.visibility = if (isRemoveEnabled) View.VISIBLE else View.GONE
        }
    }

    private fun makeImageRequest(uri: Uri) = ImageRequestBuilder.newBuilderWithSource(uri)
        .setRotationOptions(RotationOptions.autoRotate())
        .setLocalThumbnailPreviewsEnabled(true)
        .build()

    class VidViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val playerView = itemView.descriptionVidPv as ThumbnailPlayerView
        val removeIv = itemView.removeIv as ImageView

        lateinit var sourceVideo: SourceVideo
            private set

        private val controllerListener = object: BaseControllerListener<ImageInfo>() {
    
            override fun onSubmit(id: String?, callerContext: Any?) {}
    
            override fun onFailure(id: String?, throwable: Throwable?) {
                debugLog(throwable)
                itemView.context.apply {
                    showToast(this, getString(R.string.err_open_video))
                }
                removeIv.callOnClick()
            }
    
            override fun onFinalImageSet(
                id: String?,
                imageInfo: ImageInfo?,
                animatable: Animatable?
            ) {
                playerView.thumbnailView.aspectRatio = imageInfo?.aspectRatio ?: DEFAULT_ASPECT_RATIO
            }
        
        }

        fun bind(sourceVideo: SourceVideo) {
            this.sourceVideo = sourceVideo
            playerView.thumbnailView.controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(sourceVideo.thumbnailLD.imageRequest)
                .setControllerListener(controllerListener)
                .build()
        }

    }

}