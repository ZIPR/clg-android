package com.zipr.clg.ui.controller

import android.content.Intent
import android.support.v4.app.Fragment

interface NavigationController {

    fun navigateToFragment(fragment: Fragment, name: String? = null)

    fun replaceWithFragment(fragment: Fragment, name: String? = null)

    fun navigateBack()

    fun navigateToMain()

    fun navigateToAuth()

    fun navigateToUserProfile(userId: String)

    fun startActivity(intent: Intent)

}