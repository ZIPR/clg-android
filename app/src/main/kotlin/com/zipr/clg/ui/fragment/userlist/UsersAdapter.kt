package com.zipr.clg.ui.fragment.userlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zipr.clg.R
import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.PagedAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.util.firebaseUser
import kotlinx.android.synthetic.main.item_userinfo.view.*

class UsersAdapter(
    usersProvider: DataProvider<UserFollower>,
    lifecycleRegistrar: LifecycleRegistrar,
    private val onUserClick: (userFollower: UserFollower) -> Unit,
    private val onFollow: (userFollower: UserFollower) -> Unit,
    private val onUnfollow: (userFollower: UserFollower) -> Unit
) : PagedAdapter<UserFollower, UsersAdapter.UserViewHolder>(usersProvider, lifecycleRegistrar) {
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = UserViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_userinfo, parent, false),
        onFollow,
        onUnfollow
    ).apply {
        itemView.setOnClickListener { userFollower.also(onUserClick) }
    }
    
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) =
        holder.bindUser(getItem(position))
    
    class UserViewHolder(
        view: View,
        private val onFollow: (userFollower: UserFollower) -> Unit,
        private val onUnfollow: (userFollower: UserFollower) -> Unit
    ) : RecyclerView.ViewHolder(view) {
    
        lateinit var userFollower: UserFollower
            private set
        
        private val fullNameTv = itemView.fullNameTv
        private val usernameTv = itemView.usernameTv
        private val photoDv = itemView.photoDv
        private val isCurrentUserTv = itemView.isCurrentUserTv
        private val followBtn = itemView.followBtn
        
        fun bindUser(userFollower: UserFollower) {
            this.userFollower = userFollower
            
            fullNameTv.text = userFollower.user?.fullName.orEmpty()
            usernameTv.text = "@${userFollower.user?.username.orEmpty()}"
            photoDv.setImageURI(userFollower.user?.photoUrl)
    
            if(userFollower.user?.id == firebaseUser?.uid ) {
                followBtn.visibility = View.GONE
                isCurrentUserTv.visibility = View.VISIBLE
            } else {
                isCurrentUserTv.visibility = View.GONE
                followBtn.apply {
                    visibility = View.VISIBLE
                    if (userFollower.follower != null) {
                        setText(R.string.following)
                        isActive = false
                        setOnClickListener { onUnfollow(userFollower) }
                    } else {
                        setText(R.string.follow)
                        isActive = true
                        setOnClickListener { onFollow(userFollower) }
                    }
                }
            }
        }
        
    }
    
}
