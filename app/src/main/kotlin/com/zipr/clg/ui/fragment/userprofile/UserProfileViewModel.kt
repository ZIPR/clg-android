package com.zipr.clg.ui.fragment.userprofile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.interactor.UserInteractor
import javax.inject.Inject

class UserProfileViewModel @Inject constructor(private val userInteractor: UserInteractor) : ViewModel() {
    
    val userFollowerLD: LiveData<UserFollower> by lazy {
        userInteractor.getUserFollower(userId, followerId, true, true)
    }
    
    private lateinit var userId: String
    private lateinit var followerId: String
    
    fun setUser(userId: String, followerId: String) {
        this.userId = userId
        this.followerId = followerId
    }
    
    fun follow() {
        userInteractor.follow(userId, followerId)
    }
    
    fun unfollow() {
        userInteractor.unfollow(userId, followerId)
    }
    
}
