package com.zipr.clg.ui.fragment.challengecreation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.AsyncResult
import com.zipr.clg.data.interactor.ChallengeInteractor
import com.zipr.clg.ui.common.SourceImage
import com.zipr.clg.ui.common.SourceVideo
import com.zipr.clg.util.SingleEventObserver
import com.zipr.clg.util.observeSingleEvent
import javax.inject.Inject

class ChallengeCreationViewModel @Inject constructor(
    private val challengeInteractor: ChallengeInteractor
) : ViewModel() {

    private var creatingChallengeObserver: SingleEventObserver<*, *>? = null

    private val _creatingChallengeLD = MutableLiveData<AsyncResult<String, Throwable>>()
    val creatingChallengeLD = _creatingChallengeLD as LiveData<AsyncResult<String, Throwable>>

    fun createChallenge(
        title: String,
        descText: String,
        tags: List<String>,
        imageColor: Int,
        imageLD: LiveData<AsyncResult<SourceImage, Throwable>>,
        descSourceImageLDList: List<LiveData<AsyncResult<SourceImage, Throwable>>>,
        descSourceVideoList: List<SourceVideo>
    ) {
        cancel()

        creatingChallengeObserver = challengeInteractor.create(
            title,
            descText,
            tags,
            imageColor,
            imageLD,
            descSourceImageLDList,
            descSourceVideoList
        ).observeSingleEvent {
            _creatingChallengeLD.value = it
            creatingChallengeObserver = null
        }
    }

    fun cancel() {
        creatingChallengeObserver?.cancel()
        creatingChallengeObserver = null
        _creatingChallengeLD.value = null
    }

}