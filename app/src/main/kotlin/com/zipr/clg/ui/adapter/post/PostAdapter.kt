package com.zipr.clg.ui.adapter.post

import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.entity.Post
import com.zipr.clg.data.entity.VotedPost
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.PagedAdapter
import com.zipr.clg.ui.adapter.SingleItemAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.orZero
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter(
    private val onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
    private val onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
    private val onAuthorClick: (post: Post) -> Unit,
    private val onLocationClick: (location: Location) -> Unit,
    private val onRatingUp: (post: Post) -> Unit,
    private val onRatingDown: (post: Post) -> Unit,
    private val onCommentsClick: (post: Post) -> Unit
) : SingleItemAdapter<PostViewHolder>(R.layout.item_post) {

    var votedPost: VotedPost? = null
        set(value) {
            field = value
            dataItemCount = if (value != null) 1 else 0
        }

    override fun onCreateViewHolder(view: View): PostViewHolder =
        PostViewHolder(
            view,
            onBindPlayerViewTag,
            onClickPlayerView,
            onLocationClick,
            onRatingUp,
            onRatingDown
        ).apply {
            authorAreaView.setOnClickListener { votedPost.post?.also(onAuthorClick) }
            commentsBtn.setOnClickListener { votedPost.post?.also(onCommentsClick) }
        }

    override fun onBindViewHolder(holder: PostViewHolder) =
            holder.bindPost(votedPost!!)

}

class PostCardAdapter(
    pagedDataProvider: DataProvider<VotedPost>,
    lifecycleRegistrar: LifecycleRegistrar,
    private val onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
    private val onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
    private val onPostClick: (post: Post) -> Unit,
    private val onAuthorClicked: (post: Post) -> Unit,
    private val onLocationClicked: (location: Location) -> Unit,
    private val onRatingUp: (post: Post) -> Unit,
    private val onRatingDown: (post: Post) -> Unit,
    private val onCommentsClick: (post: Post) -> Unit
) : PagedAdapter<VotedPost, PostViewHolder>(pagedDataProvider, lifecycleRegistrar) {

    private val contentViewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_post_card, parent, false),
            onBindPlayerViewTag,
            onClickPlayerView,
            onLocationClicked,
            onRatingUp,
            onRatingDown
        ).apply {
            itemView.setOnClickListener { votedPost.post?.also(onPostClick) }
            authorAreaView.setOnClickListener { votedPost.post?.also(onAuthorClicked) }
            commentsBtn.setOnClickListener { votedPost.post?.also(onCommentsClick) }
            contentRv.recycledViewPool = contentViewPool
        }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) =
            holder.bindPost(getItem(position))
    }
    class PostViewHolder(
        view: View,
        onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
        onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
        onLocationClicked: (location: Location) -> Unit,onRatingUp: (post: Post) -> Unit,
        onRatingDown: (post: Post) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        val authorAreaView = itemView.authorAreaView as View
        val authorPhotoDv = itemView.authorPhotoDv as SimpleDraweeView
        val authorUsernameTv = itemView.authorUsernameTv as TextView
        val creationDateTv = itemView.creationDateTv as TextView
        val contentRv = itemView.contentRv as RecyclerView
        val ratingBtn = itemView.ratingBtn as Button
        val commentsBtn = itemView.commentsBtn as Button

        lateinit var votedPost: VotedPost

        private val ratingNormalToUpIc = ClgApplication.getDrawable(R.drawable.ic_rating_normal) as AnimatedVectorDrawable
        private val ratingUpToNormalIc = ClgApplication.getDrawable(R.drawable.ic_rating_up) as AnimatedVectorDrawable
        private val contentAdapter: PostContentAdapter = PostContentAdapter.create(
            onBindPlayerViewTag,
            onClickPlayerView,
            onLocationClicked
        )

        private var isRatingBtnStateUp: Boolean

        init {
            contentRv.apply {
                adapter = contentAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                itemAnimator = null
            }
            ratingBtn.setOnClickListener {
                votedPost.post?.also {
                    if (votedPost.voting == null) onRatingUp(it) else onRatingDown(it)
                }
            }
            isRatingBtnStateUp = false
            setRatingBtnDrawable(ratingNormalToUpIc)
        }

        fun bindPost(votedPost: VotedPost) {
            val animateRatingStateChange = this::votedPost.isInitialized && this.votedPost == votedPost
            this.votedPost = votedPost

            authorPhotoDv.setImageURI(votedPost.post?.author?.photoUrl)
            authorUsernameTv.text = votedPost.post?.author?.username.orEmpty()
            creationDateTv.text = votedPost.post?.creationDate?.let ( ClgApplication.dateFormat::format) .orEmpty()
            commentsBtn.text = votedPost.post?.commentsCount.orZero().toString()
            ratingBtn.text = votedPost.post?.rating.orZero().toString()
            votedPost.post?.also(
                contentAdapter::setPost)

            setRatingBtnStatus(votedPost.voting != null, animateRatingStateChange)
        }

        private fun setRatingBtnStatus(toUpState: Boolean, animate: Boolean) {
            if (toUpState != isRatingBtnStateUp) {
                isRatingBtnStateUp = toUpState
                if (toUpState) {
                    if (animate) {
                        setRatingBtnDrawable(ratingNormalToUpIc)
                        ratingNormalToUpIc.start()
                    } else {
                        setRatingBtnDrawable(ratingUpToNormalIc)
                    }
                } else {
                    if (animate) {
                        setRatingBtnDrawable(ratingUpToNormalIc)
                        ratingUpToNormalIc.start()
                    } else {
                        setRatingBtnDrawable(ratingNormalToUpIc)
                    }
                }
            }
        }

        private fun setRatingBtnDrawable(drawable: Drawable) {
            ratingBtn.setCompoundDrawablesWithIntrinsicBounds(
                drawable,
                null,
                null,
                null
            )
    }

}