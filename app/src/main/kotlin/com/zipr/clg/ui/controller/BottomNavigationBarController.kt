package com.zipr.clg.ui.controller

interface BottomNavigationBarController {

    var isNavigationBarVisible: Boolean

}