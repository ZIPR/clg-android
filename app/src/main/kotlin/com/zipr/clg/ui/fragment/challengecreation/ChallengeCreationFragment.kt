package com.zipr.clg.ui.fragment.challengecreation

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.net.Uri
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.v7.app.AlertDialog
import android.support.v7.graphics.Palette
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.common.RotationOptions
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.onExoPlayerError
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.adapter.EditableImageAdapter
import com.zipr.clg.ui.adapter.EditableVideoAdapter
import com.zipr.clg.ui.adapter.SegmentAdapter
import com.zipr.clg.ui.common.*
import com.zipr.clg.ui.controller.*
import com.zipr.clg.ui.fragment.challenge.ChallengeFragment
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.*
import kotlinx.android.synthetic.main.fragment_challenge_creation.*
import kotlinx.android.synthetic.main.state_editable_image.view.*
import javax.inject.Inject

private const val BUNDLE_TAGS = "BUNDLE_TAGS"
private const val BUNDLE_BUILDING_TAG = "BUNDLE_BUILDING_TAG"
private const val BUNDLE_IMAGE_URI = "BUNDLE_IMAGE_URI"
private const val BUNDLE_DESC_IMAGES = "BUNDLE_DESC_IMAGES"
private const val BUNDLE_DESC_VIDEOS = "BUNDLE_DESC_VIDEOS"
private const val BUNDLE_NO_TAGS_VISIBILITY = "BUNDLE_NO_TAGS_VISIBILITY"
private const val BUNDLE_NO_IMAGE_VISIBILITY = "BUNDLE_NO_IMAGE_VISIBILITY"
private const val BUNDLE_PLAYER_VIEW_TAG = "BUNDLE_PLAYER_VIEW_TAG"
private const val BUNDLE_BUILDING_TAG_SELECTION_INFO = "BUNDLE_BUILDING_TAG_SELECTION_INFO"

private const val RC_CHALLENGE_IMAGE_PICK = 100
private const val RC_DESC_IMAGE_PICK = 101
private const val RC_DESC_VIDEO_PICK = 102

private const val NO_TAGS_ANIM_DURATION = 120L

@Injectable
class ChallengeCreationFragment : ViewLifecycleFragment(), FinishHandler {

    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var statusBarController: StatusBarController
    @Inject lateinit var navigationBarController: BottomNavigationBarController

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val challengeCreationViewModel by lazy { viewModelProvider.get(ChallengeCreationViewModel::class.java) }
    private val playerViewModel by lazy { viewModelProvider.get(PlayerViewModel::class.java) }

    private val tagAdapter by lazy { AutoClearedValue<TagAdapter>(viewLifecycleOwner) }
    private val descImgAdapter by lazy { AutoClearedValue<EditableImageAdapter>(viewLifecycleOwner) }
    private val descVidAdapter by lazy { AutoClearedValue<EditableVideoAdapter>(viewLifecycleOwner) }

    private var challengeSourceImageLD: SourceImageLD? = null
    private var challengeImageColor: Int? = null

    private val currentPlayerView by lazy { AutoClearedValue<ThumbnailPlayerView>(viewLifecycleOwner) }

    private var alertDialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_challenge_creation, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val playerViewTag = savedInstanceState?.getString(BUNDLE_PLAYER_VIEW_TAG)
        val tags = savedInstanceState?.getStringArrayList(BUNDLE_TAGS)
        val buildingTag = savedInstanceState?.getString(BUNDLE_BUILDING_TAG)
        val buildingTagSelectionInfo = savedInstanceState?.getParcelable<SelectionInfo>(BUNDLE_BUILDING_TAG_SELECTION_INFO)
        val imageUriList = savedInstanceState?.getStringArrayList(BUNDLE_DESC_IMAGES)
        val videoUriList = savedInstanceState?.getStringArrayList(BUNDLE_DESC_VIDEOS)
        val imageUri = savedInstanceState?.getString(BUNDLE_IMAGE_URI)
        val noTagsVisibility = savedInstanceState?.getInt(BUNDLE_NO_TAGS_VISIBILITY, View.VISIBLE)
        val noImageVisibility = savedInstanceState?.getInt(BUNDLE_NO_IMAGE_VISIBILITY, View.VISIBLE)

        navigationBarController.isNavigationBarVisible = false

        playerViewModel.addPlayerEventListener(
            viewLifecycleOwner,
            object : Player.DefaultEventListener() {

                override fun onPlayerError(error: ExoPlaybackException) {
                    setPlayerView(null, null)
                    this@ChallengeCreationFragment.onPlayerError(error)
                }
            }
        )

        tagAdapter.value = TagAdapter(
            tags = tags.orEmpty(),
            buildingTag = buildingTag.orEmpty(),
            buildingTagSelectionInfo = buildingTagSelectionInfo ?: SelectionInfo(),
            onTagsChanged = {
                if (it.isEmpty() == true) {
                    noTagsTv.animate().cancel()
                    noTagsTv.animate()
                        .alpha(1f)
                        .setDuration(NO_TAGS_ANIM_DURATION)
                        .start()
                } else {
                    noTagsTv.animate().cancel()
                    noTagsTv.animate()
                        .alpha(0f)
                        .setDuration(NO_TAGS_ANIM_DURATION)
                        .start()
                }
            }
        )
        descImgAdapter.value = EditableImageAdapter(
            imageUriList = imageUriList ?: emptyList()
        )
        descVidAdapter.value = EditableVideoAdapter(
            videoUriList = videoUriList ?: emptyList(),
            onBindPlayerViewTag = { playerView: ThumbnailPlayerView ->
                playerView.setLifecycleOwner(this)
                if (playerViewTag != null && playerView.tag == playerViewTag) {
                    setPlayerView(playerView, null)
                }
            },
            onClickPlayerView = { player: ThumbnailPlayerView?, uri: Uri? ->
                setPlayerView(player, uri)
            }
        )

        State.apply { restoreState(savedInstanceState) }
        ImageState.apply { restoreState(savedInstanceState) }

        imageUri?.also {
            setChallengeImage(Uri.parse(it))
        }

        noTagsTv.visibility = noTagsVisibility ?: View.VISIBLE
        noImageTv.visibility = noImageVisibility ?: View.VISIBLE

        createChallengeBtn.setOnClickListener { createChallenge() }
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        cancelIv.setOnClickListener {
            challengeSourceImageLD = null
            imageDv.controller = null
            setChallengeColor(ClgApplication.getColor(R.color.colorPrimary))
            noImageTv.visibility = View.VISIBLE
            ImageState.apply { setState(ImageState.IDLE) }
        }
        stateEditableImage.setImageIv.setOnClickListener {
            startImageChooser(RC_CHALLENGE_IMAGE_PICK)
        }
        addDescImageIv.setOnClickListener {
            startImageChooser(RC_DESC_IMAGE_PICK)
        }
        addDescVideoIv.setOnClickListener {
            startVideoChooser(RC_DESC_VIDEO_PICK)
        }

        titleTxInEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) validateTitle() == null
            else false
        }
        descriptionTxInEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) validateDescription() == null
            else false
        }
        titleTxInEt.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (!titleTxInL.error.isNullOrEmpty()) {
                    validateTitle()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        })
        descriptionTxInEt.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (!descriptionTxInL.error.isNullOrEmpty()) {
                    validateDescription()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        })

        tagsRv.apply {
            adapter = tagAdapter.value!!
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
        descriptionMediaRv.apply {
            adapter = SegmentAdapter(descImgAdapter.value!!, descVidAdapter.value!!)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            isNestedScrollingEnabled = false
        }

        challengeCreationViewModel.creatingChallengeLD.observeNonNull(viewLifecycleOwner) {
            if (it.result != null) {
                navigationController.replaceWithFragment(
                    ChallengeFragment.newInstance(it.result)
                )
            } else if (it.error != null) {
                State.apply { setState(State.IDLE) }
                ImageState.apply { setState(ImageState.IDLE) }
                onCreationError(it.error)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val uri = data?.data?.getContentPath(context!!)
            when (requestCode) {
                RC_CHALLENGE_IMAGE_PICK -> uri?.also {
                    setChallengeImage(it)
                }

                RC_DESC_IMAGE_PICK -> uri?.also {
                    descImgAdapter.value?.addImage(it)
                }

                RC_DESC_VIDEO_PICK -> uri?.also {
                    descVidAdapter.value?.addVideo(it)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }
        ImageState.apply { saveState(outState) }

        tagAdapter.value?.also {
            outState.putStringArrayList(BUNDLE_TAGS, it.tags as ArrayList<String>)
            outState.putString(BUNDLE_BUILDING_TAG, it.buildingTag)
            outState.putParcelable(BUNDLE_BUILDING_TAG_SELECTION_INFO, it.buildingTagSelectionInfo)
        }
        challengeSourceImageLD?.imageRequest?.sourceUri?.toString()?.also {
            outState.putString(BUNDLE_IMAGE_URI, it)
        }
        (descImgAdapter.value?.imageUriList as? ArrayList<String>)?.also {
            outState.putStringArrayList(BUNDLE_DESC_IMAGES, it)
        }
        (descVidAdapter.value?.videoUriList as? ArrayList<String>)?.also {
            outState.putStringArrayList(BUNDLE_DESC_VIDEOS, it)
        }
        currentPlayerView.value?.tag?.toString()?.also {
            outState.putString(BUNDLE_PLAYER_VIEW_TAG, it)
        }
        noTagsTv?.visibility?.also {
            outState.putInt(BUNDLE_NO_TAGS_VISIBILITY, it)
        }
        noImageTv?.visibility?.also {
            outState.putInt(BUNDLE_NO_IMAGE_VISIBILITY, it)
        }
    }

    override fun onStop() {
        super.onStop()

        alertDialog?.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        statusBarController.setStatusBarColor(null)
        navigationBarController.isNavigationBarVisible = true

        if (isRemoving) {
            currentPlayerView.value?.player?.playWhenReady = false
        }
    }

    override fun onFinish(continueFinish: () -> Unit) {
        context?.also {
            alertDialog = AlertDialog.Builder(it)
                .setTitle(getString(R.string.cancel_challenge))
                .setPositiveButton(R.string.yes) { dialog, _ ->
                    dialog.cancel()
                    challengeCreationViewModel.cancel()
                    continueFinish()
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    dialog.cancel()
                }
                .show()
        } ?: continueFinish()
    }

    private fun setChallengeImage(uri: Uri) {
        ClgApplication.getColor(R.color.colorPrimary).also {
            setChallengeColor(it)
            imageDv.setBackgroundColor(it)
        }

        val imageRequest = ImageRequestBuilder
            .newBuilderWithSource(uri)
            .setRotationOptions(RotationOptions.autoRotate())
            .setLocalThumbnailPreviewsEnabled(true)
            .build()

        challengeSourceImageLD = SourceImageLD(imageRequest).also {
            it.observeSingleEventNonNull(viewLifecycleOwner) {
                it.result?.bitmap?.also {
                    Palette.from(it).generate {
                        setChallengeColor(
                            it.vibrantOrMutedSwatch?.rgb ?: ClgApplication.getColor(R.color.colorPrimary)
                        )
                        imageDv.setBackgroundColor(Color.BLACK)
                    }
                }
            }
        }

        imageDv.controller = Fresco.newDraweeControllerBuilder()
            .setOldController(imageDv.controller)
            .setControllerListener(object : BaseControllerListener<ImageInfo>() {

                override fun onSubmit(id: String?, callerContext: Any?) {
                    noImageTv.visibility = View.GONE
                    if (ImageState.currentState != ImageState.UPLOADING) {
                        ImageState.apply { setState(ImageState.OPENING) }
                    }
                }

                override fun onFailure(id: String?, throwable: Throwable?) {
                    showValidationToast(context, getString(R.string.err_open_image))
                    challengeSourceImageLD = null
                    noImageTv.visibility = View.VISIBLE
                    if (ImageState.currentState != ImageState.UPLOADING) {
                        ImageState.apply { setState(ImageState.IDLE) }
                    }
                }

                override fun onFinalImageSet(
                    id: String?,
                    imageInfo: ImageInfo?,
                    animatable: Animatable?
                ) {
                    noImageTv.visibility = View.GONE
                    if (ImageState.currentState != ImageState.UPLOADING) {
                        ImageState.apply { setState(ImageState.IDLE) }
                    }
                }

            })
            .setImageRequest(imageRequest)
            .build()
    }

    private fun setChallengeColor(@ColorInt color: Int) {
        challengeImageColor = color
        background.setBackgroundColor(color)
        statusBarController.setStatusBarColor(color)
        tagAdapter.value?.tagsColor = color
    }

    private fun validateTitle() = titleTxInEt.let {
        it.text.toString().run {
            if (length >= resources.getInteger(R.integer.challenge_title_length_min) &&
                    length <= resources.getInteger(R.integer.challenge_title_length_max)) this
            else null
        }.also {
            titleTxInL.error = if (it == null) getString(R.string.err_title_valid) else null
        }
    }

    private fun validateDescription() = descriptionTxInEt.let {
        it.text.toString().run {
            if (length >= resources.getInteger(R.integer.challenge_description_length_min) &&
                length <= resources.getInteger(R.integer.challenge_description_length_max)) this
            else null
        }.also {
            descriptionTxInL.error = if (it == null) getString(R.string.err_description_valid) else null
        }
    }

    private fun validateTags() = tagAdapter.value?.tags?.run {
        if (isNotEmpty()) this else null
    }

    private fun createChallenge() {
        titleTxInEt.trimTextAndSelection()
        descriptionTxInEt.trimTextAndSelection()

        val title = validateTitle()
        val descText = validateDescription()
        val tags = validateTags()
        val imageColor = challengeImageColor
        val sourceImageLD = challengeSourceImageLD
        val descSourceImageLDList = descImgAdapter.value!!.sourceImageLDList
        val descSourceVideoList = descVidAdapter.value!!.sourceVideoList

        if (title == null) {
            showValidationToast(context, getString(R.string.err_title_valid))
            return
        }
        if (descText == null) {
            showValidationToast(context, getString(R.string.err_description_valid))
            return
        }
        if (tags == null) {
            showValidationToast(context, getString(R.string.err_specify_tags))
            return
        }
        if (sourceImageLD == null || imageColor == null) {
            showValidationToast(context, getString(R.string.err_specify_image))
            return
        }

        State.apply { setState(State.CREATING) }
        ImageState.apply { setState(ImageState.UPLOADING) }
        challengeCreationViewModel.createChallenge(
            title,
            descText,
            tags,
            imageColor,
            sourceImageLD,
            descSourceImageLDList,
            descSourceVideoList
        )
    }

    private fun setPlayerView(playerView: ThumbnailPlayerView?, uri: Uri?) {
        currentPlayerView.value?.apply {
            if (this != playerView) {
                player?.stop(true)
                player = null
            }
        }
        currentPlayerView.value = playerView

        playerView?.also {
            playerViewModel.startVideo(it, uri)
        }
    }

    private fun onCreationError(e: Throwable) {
        showValidationToast(context, getString(R.string.err_creating_challenge, e.message))
    }

    private fun onPlayerError(e: ExoPlaybackException) {
        onExoPlayerError(context, e, getString(R.string.err_video))
    }

    companion object {

        fun newInstance() = ChallengeCreationFragment()

    }

    private enum class State(override val state: ChallengeCreationFragment.() -> Unit) :
        StateAct<ChallengeCreationFragment> {
        IDLE({
            titleTxInEt.isEnabled = true
            descriptionTxInEt.isEnabled = true
            stateEditableImage.setImageIv.isEnabled = true
            cancelIv.isEnabled = true
            addDescImageIv.apply {
                isEnabled = true
                visibility = View.VISIBLE
            }
            addDescVideoIv.apply {
                isEnabled = true
                visibility = View.VISIBLE
            }
            tagAdapter.value?.isEditable = true
            descImgAdapter.value?.isRemoveEnabled = true
            descVidAdapter.value?.isRemoveEnabled = true
            creatingPb.visibility = View.GONE
            createChallengeBtn.textScaleX = 1.0f
            createChallengeBtn.isEnabled = true
        }),
        CREATING({
            titleTxInEt.isEnabled = false
            descriptionTxInEt.isEnabled = false
            stateEditableImage.setImageIv.isEnabled = false
            cancelIv.isEnabled = false
            addDescImageIv.apply {
                isEnabled = false
                visibility = View.GONE
            }
            addDescVideoIv.apply {
                isEnabled = false
                visibility = View.GONE
            }
            tagAdapter.value?.isEditable = false
            descImgAdapter.value?.isRemoveEnabled = false
            descVidAdapter.value?.isRemoveEnabled = false
            creatingPb.visibility = View.VISIBLE
            createChallengeBtn.textScaleX = 0.0f
            createChallengeBtn.isEnabled = false
        });

        companion object : StateController<ChallengeCreationFragment, State>(
            State.IDLE,
            { state: String -> State.valueOf(state) }
        )

    }

    private enum class ImageState(override val state: ChallengeCreationFragment.() -> Unit) :
        StateAct<ChallengeCreationFragment> {
        IDLE({
            stateEditableImage.visibility = View.VISIBLE
            stateEditableImage.setImageIv.visibility = View.VISIBLE
            stateEditableImage.progressBar.visibility = View.GONE
            cancelIv.visibility = View.GONE
        }),
        OPENING({
            stateEditableImage.visibility = View.GONE
            stateEditableImage.setImageIv.visibility = View.GONE
            stateEditableImage.progressBar.visibility = View.GONE
            cancelIv.visibility = View.VISIBLE
        }),
        UPLOADING({
            stateEditableImage.visibility = View.GONE
            stateEditableImage.setImageIv.visibility = View.GONE
            stateEditableImage.progressBar.visibility = View.GONE
            cancelIv.visibility = View.GONE
        });

        companion object : StateController<ChallengeCreationFragment, ImageState>(
            ImageState.IDLE,
            { state: String -> ImageState.valueOf(state) }
        )

    }

}