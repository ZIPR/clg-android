package com.zipr.clg.ui.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.View

class LayoutAdapter(
    @param:LayoutRes private val layout: Int
) : SingleItemAdapter<LayoutAdapter.LayoutViewHolder>(layout) {
    
    override fun onCreateViewHolder(view: View) = LayoutViewHolder(view)
    
    override fun onBindViewHolder(holder: LayoutViewHolder) {
        
    }
    
    class LayoutViewHolder(view: View) : RecyclerView.ViewHolder(view)
    
}