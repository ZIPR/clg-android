package com.zipr.clg.ui.fragment.challenges

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.entity.Challenge
import com.zipr.clg.data.entity.ChallengeParticipating
import com.zipr.clg.data.entity.Participating
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.PagedAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.util.overrideColor
import kotlinx.android.synthetic.main.item_challenges_challenge.view.*

class ChallengeAdapter(
    challengeProvider: DataProvider<ChallengeParticipating>,
    lifecycleRegistrar: LifecycleRegistrar,
    private val onChallengeClick: (Challenge) -> Unit
) : PagedAdapter<ChallengeParticipating, ChallengeAdapter.ChallengeViewHolder>(challengeProvider, lifecycleRegistrar) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ChallengeViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_challenges_challenge, parent, false)
    ).apply {
        itemView.apply {
            tagsRv.apply {
                adapter = TagAdapter()
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            }
            authorTv.background.overrideColor(
                ClgApplication.getColor(R.color.colorPrimary_60tp)
            )
            participantsCountTv.background.overrideColor(
                ClgApplication.getColor(R.color.colorPrimary_60tp)
            )
            setOnClickListener {
                onChallengeClick(challenge)
            }
        }
    }
    
    override fun onBindViewHolder(holder: ChallengeViewHolder, position: Int) {
        getItem(position).also {
            holder.bindChallenge(it.challenge, it.participating)
        }
    }

    class ChallengeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    
        lateinit var challenge: Challenge
            private set
        
        val titleTv = itemView.titleTv as TextView
        val pictureDv = itemView.imageDv as SimpleDraweeView
        val participantsCountTv = itemView.participantsCountTv as TextView
        val tagsRv = itemView.tagsRv as RecyclerView
        val authorTv = itemView.authorTv as TextView
        val authorDv = itemView.authorDv as SimpleDraweeView
        val isInvolvedIv = itemView.isInvolvedIv as ImageView
        
        fun bindChallenge(challenge: Challenge?, participating: Participating?) {
            if (challenge == null) {
                return
            }

            this.challenge = challenge
            
            titleTv.text = challenge.title
            pictureDv.setImageURI(challenge.image?.url)
            participantsCountTv.text = challenge.participantsCount?.toString()
            (tagsRv.adapter as TagAdapter).setTags(challenge.tags)
            authorTv.text = challenge.author?.username
            authorDv.setImageURI(challenge.author?.photoUrl)
            isInvolvedIv.visibility = if (participating != null) View.VISIBLE else View.GONE
        }

    }

}