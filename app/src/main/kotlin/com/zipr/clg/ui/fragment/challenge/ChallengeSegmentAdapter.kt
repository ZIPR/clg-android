package com.zipr.clg.ui.fragment.challenge

import android.net.Uri
import com.zipr.clg.R
import com.zipr.clg.data.entity.ChallengeParticipating
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.entity.Post
import com.zipr.clg.data.entity.VotedPost
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.*
import com.zipr.clg.ui.adapter.post.PostCardAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.widget.ThumbnailPlayerView

class ChallengeSegmentAdapter private constructor(
    private val descJoinParticipantsAdapter: DescJoinParticipantsAdapter,
    private val descTxtAdapter: TextAdapter,
    private val descImgAdapter: ImageAdapter,
    private val descVidAdapter: VideoAdapter,
    private val descAuthorAdapter: DescAuthorAdapter,
    postsDividerAdapter: LayoutAdapter,
    postAdapter: PostCardAdapter,
    private val postsNoContentLayoutAdapter: LayoutAdapter
) : SegmentAdapter(
    descJoinParticipantsAdapter,
    descTxtAdapter,
    descImgAdapter,
    descVidAdapter,
    descAuthorAdapter,
    postsDividerAdapter,
    postAdapter.withPaginationStatus(16),
    postsNoContentLayoutAdapter
) {

    init {
        postsDividerAdapter.isVisible = true
        postsNoContentLayoutAdapter.isVisible = false
    }

    fun setPostsStatus(isPostsListEmpty: Boolean) {
        postsNoContentLayoutAdapter.isVisible = isPostsListEmpty
    }

    fun setChallengeParticipating(challengeParticipating: ChallengeParticipating) {
        challengeParticipating.apply {
            descJoinParticipantsAdapter.set(
                participating,
                challenge?.participantsCount,
                challenge?.participantsPreview
            )
            descTxtAdapter.set(challenge?.descriptionText)
            descImgAdapter.set(challenge?.descriptionImages)
            descVidAdapter.set(challenge?.descriptionVideos)
            descAuthorAdapter.set(
                challenge?.author,
                challenge?.creationDate
            )
        }
        descTxtAdapter.isVisible = true
    }

    companion object {

        fun create(
            lifecycleRegistrar: LifecycleRegistrar,
            postDataProvider: DataProvider<VotedPost>,
            onJoinClick: () -> Unit,
            onLeaveClick: () -> Unit,
            onParticipantsPreviewClick: () -> Unit,
            onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
            onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
            onAuthorClick: (username: String) -> Unit,
            onPostClick: (post: Post) -> Unit,
            onPostAuthorClick: (post: Post) -> Unit,
            onPostLocationClick: (location: Location) -> Unit,
            onPostRatingUp: (post: Post) -> Unit,
            onPostRatingDown: (post: Post) -> Unit,
            onPostCommentsClick: (post: Post) -> Unit
        ) = ChallengeSegmentAdapter(
            DescJoinParticipantsAdapter(onJoinClick, onLeaveClick, onParticipantsPreviewClick),
            TextAdapter(),
            ImageAdapter(),
            VideoAdapter(onBindPlayerViewTag, onClickPlayerView),
            DescAuthorAdapter(onAuthorClick),
            LayoutAdapter(R.layout.item_challenge_posts_divider),
            PostCardAdapter(
                postDataProvider,
                lifecycleRegistrar,
                onBindPlayerViewTag,
                onClickPlayerView,
                onPostClick,
                onPostAuthorClick,
                onPostLocationClick,
                onPostRatingUp,
                onPostRatingDown,
                onPostCommentsClick
            ),
            LayoutAdapter(R.layout.item_challenge_posts_no_content)
        )

    }

}
