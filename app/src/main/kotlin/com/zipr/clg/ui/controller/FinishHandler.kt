package com.zipr.clg.ui.controller

interface FinishHandler {

    fun onFinish(continueFinish: () -> Unit)

}