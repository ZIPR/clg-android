package com.zipr.clg.ui.widget

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView
import com.zipr.clg.R

private const val DEFAULT_ASPECT_RATIO = 1.77777778f

class ThumbnailPlayerView : PlayerView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val thumbnailView = SimpleDraweeView(context)
    val scrimView = ImageView(context)
    private val progressBarView = ProgressBar(context)

    private var lifecycleOwnerState: Lifecycle.State? = null

    private val eventListener = object : Player.DefaultEventListener() {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            val isIdle = playbackState == Player.STATE_IDLE || player?.currentTimeline?.isEmpty == true
            val isBuffering = playbackState == Player.STATE_BUFFERING

            thumbnailView.visibility = if (isIdle) View.VISIBLE else View.INVISIBLE
            progressBarView.visibility = if (isBuffering) View.VISIBLE else View.INVISIBLE
            scrimView.visibility = if (isIdle && !isBuffering) View.VISIBLE else View.INVISIBLE
        }

    }

    init {
        thumbnailView.aspectRatio = DEFAULT_ASPECT_RATIO
        addView(
            thumbnailView,
            LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )
        )

        scrimView.setImageResource(R.drawable.exo_controls_play)
        scrimView.setBackgroundResource(R.color.colorPrimary_30tp)
        scrimView.scaleType = ImageView.ScaleType.CENTER
        scrimView.isClickable = true
        addView(
            scrimView,
            LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
            )
        )

        progressBarView.indeterminateTintList = ColorStateList.valueOf(
            Color.parseColor("#ffffff")
        )
        progressBarView.visibility = View.INVISIBLE
        addView(
            progressBarView,
            LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER
            )
        )
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        if (lifecycleOwnerState?.isAtLeast(Lifecycle.State.RESUMED) == true) {
            player?.playWhenReady = false
        }
    }

    override fun setPlayer(player: Player?) {
        this.player?.removeListener(eventListener)

        super.setPlayer(player)

        this.player?.also {
            eventListener.onPlayerStateChanged(it.playWhenReady, it.playbackState)
            it.addListener(eventListener)
        }
    }

    fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        if (lifecycleOwnerState == null) {
            lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
        
                @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
                fun onChangeState() {
                    lifecycleOwnerState = lifecycleOwner.lifecycle.currentState
                }
        
            })
        }
    }
    
}