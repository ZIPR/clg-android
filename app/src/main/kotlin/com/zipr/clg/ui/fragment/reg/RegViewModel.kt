package com.zipr.clg.ui.fragment.reg

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.zipr.clg.data.entity.User
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.util.defaultPhotoUrl
import com.zipr.clg.util.firebaseAuth
import javax.inject.Inject

class RegViewModel @Inject constructor(
    private val userInteractor: UserInteractor
) : ViewModel() {

    private val _regLiveData = MutableLiveData<Task<*>>()
    val regLiveData get() = _regLiveData as LiveData<Task<*>>

    fun resetReg() {
        _regLiveData.value = null
    }

    fun createUser(fullName: String, username: String, email: String, password: String) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener({
                if(it.isSuccessful) {
                    it.result.user.apply {
                        val user = User(
                            username,
                            fullName,
                            "",
                            defaultPhotoUrl,
                            0,
                            0,
                            ""
                        )
                        userInteractor.setUser(user)
                            .addOnCompleteListener({ _regLiveData.postValue(it) })
                            .addOnFailureListener({ delete() })
                    }
                } else {
                    _regLiveData.postValue(it)
                }
            })
    }

}