package com.zipr.clg.ui.fragment.post

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.entity.Comment
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.VotedPost
import com.zipr.clg.data.interactor.PostInteractor
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.util.calculateFetchLimit
import javax.inject.Inject

private const val COMMENT_ITEM_VIEW_MINIMUM_HEIGHT_DP = 80

class PostViewModel @Inject constructor(
    private val postInteractor: PostInteractor
) : ViewModel() {

    lateinit var commentsDataProvider: DataProvider<Comment>
        private set

    private val _postLD = MediatorLiveData<Document<VotedPost>>()
    val postLD: LiveData<Document<VotedPost>> get() = _postLD

    private var currentPostSource: LiveData<Document<VotedPost>>? = null
    private val sourceObserver: Observer<Document<VotedPost>> = Observer {
        _postLD.value = it
    }

    private lateinit var challengeId: String
    private lateinit var postId: String
    private lateinit var userId: String

    fun setPost(challengeId: String, postId: String, userId: String) {
        if (this::challengeId.isInitialized && this::postId.isInitialized && this::userId.isInitialized
            && this.challengeId == challengeId && this.postId == postId && this.userId == userId) return

        this.challengeId = challengeId
        this.postId = postId
        this.userId = userId

        commentsDataProvider = postInteractor.getComments(
            challengeId, postId, calculateFetchLimit(COMMENT_ITEM_VIEW_MINIMUM_HEIGHT_DP)
        )
        refresh()
    }

    fun refresh() {
        currentPostSource?.also { _postLD.removeSource(it) }
        currentPostSource = postInteractor.getPost(challengeId, postId, userId).also {
            _postLD.addSource(it, sourceObserver)
        }
    }

    fun addComment(comment: Comment) {
        postInteractor.addComment(challengeId, postId, comment)
    }

    fun upPostRating(postId: String) {
        postInteractor.upRating(challengeId, postId, userId)
    }

    fun downPostRating(postId: String) {
        postInteractor.downRating(challengeId, postId, userId)
    }

}