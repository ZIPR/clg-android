package com.zipr.clg.ui.fragment.userlist.followers

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.fragment.userlist.UserListFragment
import com.zipr.clg.util.firebaseUser
import kotlinx.android.synthetic.main.fragment_followers.*
import javax.inject.Inject

private const val ARG_USER_ID = "ARG_USER_ID"

@Injectable
class FollowersFragment : UserListFragment<FollowersViewModel>() {
    
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    
    override val userListViewModel: FollowersViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(FollowersViewModel::class.java)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_followers, container,false)
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        userListViewModel.setUser(arguments!!.getString(ARG_USER_ID), firebaseUser!!.uid)
        super.onActivityCreated(savedInstanceState)
        
        toolbar.title = getString(R.string.followers)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
    }
    
    override fun getErrorMessage(error: DataException): String =
        when(error.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_no_followers)
    
            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)
    
            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)
    
            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)
    
            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)
            
            else -> error.message ?: getString(R.string.err_unexpected)
        }
    
    companion object {
        
        fun newInstance(userId: String) = FollowersFragment().apply {
            arguments = Bundle().apply { putString(ARG_USER_ID, userId) }
        }
        
    }
    
}