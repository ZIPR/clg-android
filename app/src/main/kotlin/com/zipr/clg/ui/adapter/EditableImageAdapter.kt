package com.zipr.clg.ui.adapter

import android.graphics.drawable.Animatable
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.common.RotationOptions
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.zipr.clg.R
import com.zipr.clg.ui.common.SourceImageLD
import com.zipr.clg.util.*
import kotlinx.android.synthetic.main.item_image_editable.view.*
import kotlin.properties.Delegates

class EditableImageAdapter(
    imageUriList: List<String>
) : RecyclerViewAdapter<EditableImageAdapter.ImgViewHolder>() {
    
    private val _sourceImageLDList = mutableListOf<SourceImageLD>()
    
    val sourceImageLDList get() = _sourceImageLDList as List<SourceImageLD>
    val imageUriList get() = _sourceImageLDList.map { it.imageRequest.sourceUri.toString() }

    var isRemoveEnabled by Delegates.observable(true) { _, _, _ ->
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRangeChanged(0, _sourceImageLDList.size)
        }
    }
    
    init {
        imageUriList.forEach {
            _sourceImageLDList.add(
                SourceImageLD(makeImageRequest(Uri.parse(it)))
            )
        }
    }
    
    fun addImage(uri: Uri) {
        if (_sourceImageLDList.any { it.imageRequest.sourceUri == uri }) {
            return
        }
        SourceImageLD(makeImageRequest(uri)).also {
            if (_sourceImageLDList.add(it)) {
                val index = _sourceImageLDList.lastIndex
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemInserted(index)
                }
            }
        }
    }
    
    private fun removeImage(index: Int) {
        _sourceImageLDList.removeAt(index)
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRemoved(index)
        }
    }

    override fun getItemCount() = _sourceImageLDList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ImgViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_image_editable, parent, false)
    ).apply {
        removeIv.setOnClickListener {
            _sourceImageLDList.indexOf(sourceImageLD).also {
                if (it != -1) {
                    removeImage(it)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ImgViewHolder, position: Int) {
        holder.apply {
            bind(_sourceImageLDList[position])
        }.apply {
            removeIv.visibility = if (isRemoveEnabled) View.VISIBLE else View.GONE
        }
    }
    
    private fun makeImageRequest(uri: Uri) = ImageRequestBuilder.newBuilderWithSource(uri)
        .setRotationOptions(RotationOptions.autoRotate())
        .setLocalThumbnailPreviewsEnabled(true)
        .build()

    class ImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val descriptionPicDv = itemView.descImageDv as SimpleDraweeView
        val removeIv = itemView.removeIv as ImageView

        lateinit var sourceImageLD: SourceImageLD
            private set
    
        private val controllerListener = object: BaseControllerListener<ImageInfo>() {
        
            override fun onSubmit(id: String?, callerContext: Any?) {}
        
            override fun onFailure(id: String?, throwable: Throwable?) {
                debugLog(throwable)
                itemView.context.apply {
                    showToast(this, getString(R.string.err_open_image))
                }
                removeIv.callOnClick()
            }
        
            override fun onFinalImageSet(
                id: String?,
                imageInfo: ImageInfo?,
                animatable: Animatable?
            ) {
                descriptionPicDv.aspectRatio = imageInfo?.aspectRatio ?: DEFAULT_ASPECT_RATIO
            }
        
        }

        fun bind(sourceImageLD: SourceImageLD) {
            this.sourceImageLD = sourceImageLD
            descriptionPicDv.controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(sourceImageLD.imageRequest)
                .setControllerListener(controllerListener)
                .build()
        }

    }

}