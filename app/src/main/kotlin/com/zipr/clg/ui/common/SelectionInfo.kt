package com.zipr.clg.ui.common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectionInfo(
    var hasFocus: Boolean = false,
    var selectionStart: Int = 0,
    var selectionEnd: Int = 0
) : Parcelable