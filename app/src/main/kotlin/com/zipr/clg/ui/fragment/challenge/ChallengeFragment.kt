package com.zipr.clg.ui.fragment.challenge

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.*
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.RotationOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.Image
import com.zipr.clg.data.entity.Post
import com.zipr.clg.data.onExoPlayerError
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.AutoClearedValue
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.common.PlayerViewModel
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.controller.StateAct
import com.zipr.clg.ui.controller.StateController
import com.zipr.clg.ui.controller.StatusBarController
import com.zipr.clg.ui.fragment.post.PostFragment
import com.zipr.clg.ui.fragment.postcreation.PostCreationFragment
import com.zipr.clg.ui.fragment.userlist.participants.ParticipantsFragment
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.darkenColor
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.openLocation
import kotlinx.android.synthetic.main.fragment_challenge.*
import kotlinx.android.synthetic.main.state_layout.view.*
import javax.inject.Inject

private const val BUNDLE_PLAYER_VIEW_TAG = "BUNDLE_PLAYER_VIEW_TAG"
private const val BUNDLE_IS_APP_BAR_COLLAPSED = "BUNDLE_IS_APP_BAR_COLLAPSED"
private const val ARG_CHALLENGE_ID = "ARG_CHALLENGE_ID"

@Injectable
class ChallengeFragment : ViewLifecycleFragment(), Toolbar.OnMenuItemClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var statusBarController: StatusBarController
    @Inject lateinit var navigationController: NavigationController

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val challengeViewModel by lazy { viewModelProvider.get(ChallengeViewModel::class.java) }
    private val playerViewModel by lazy { viewModelProvider.get(PlayerViewModel::class.java) }

    private val lifecycleRegistrar by lazy { LifecycleRegistrar(viewLifecycleOwner) }

    private val appBarController by lazy { AutoClearedValue<AppBarController>(viewLifecycleOwner) }
    private var shareIntent: Intent? = null

    private val currentPlayerView by lazy { AutoClearedValue<ThumbnailPlayerView>(viewLifecycleOwner) }

    private val challengeRvItemAnimator = DefaultItemAnimator().apply {
        supportsChangeAnimations = false
    }

    private var isFabHideByScrollEnabled = false
    private var isFabHiddenByScroll = false

    private var isAppBarCollapsed = false

    private var isChallengeRvAnimationsEnabled = true
        set(enable) {
            if (enable != field) {
                challengeRv.post {
                    challengeRv?.apply { itemAnimator = if (enable) challengeRvItemAnimator else null }
                }
                field = enable
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_challenge, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        State.apply { restoreState(savedInstanceState) }

        val argChallengeId = arguments!!.getString(ARG_CHALLENGE_ID)
        val playerViewTag = savedInstanceState?.getString(BUNDLE_PLAYER_VIEW_TAG)

        savedInstanceState?.getBoolean(BUNDLE_IS_APP_BAR_COLLAPSED)?.also {
            isAppBarCollapsed = it
        }

        challengeViewModel.setChallenge(argChallengeId)
        challengeViewModel.setUser(firebaseUser!!.uid)

        stateLayout.button.apply {
            text = getString(R.string.retry)
            setOnClickListener { onRetryClick() }
        }
        newPostFab.setOnClickListener {
            challengeViewModel.challengeLD.value?.data?.challenge?.also {
                navigationController.navigateToFragment(
                    PostCreationFragment.newInstance(it.id!!, it.title!!, it.imageColor!!)
                )
            }
        }
        statusBarController.setStatusBarColor(Color.TRANSPARENT)
        statusBarScrim.layoutParams.height = statusBarController.statusBarHeight
        statusBarScrim.requestLayout()

        toolbar.also {
            it.setNavigationOnClickListener { activity?.onBackPressed() }
            it.inflateMenu(R.menu.share_menu)
            it.setOnMenuItemClickListener(this)
            it.title = null
            it.setOnHierarchyChangeListener(object : ViewGroup.OnHierarchyChangeListener {

                override fun onChildViewAdded(parent: View?, child: View?) {
                    if (child is TextView) {
                        appBarController.value?.also {
                            appBarLayout.removeOnOffsetChangedListener(it)
                        }
                        appBarController.value = AppBarController(child, isAppBarCollapsed).also {
                            appBarLayout.addOnOffsetChangedListener(it)
                        }
                    }
                }

                override fun onChildViewRemoved(parent: View?, child: View?) {
                    if (child is TextView) {
                        appBarController.value?.also {
                            appBarLayout.removeOnOffsetChangedListener(it)
                        }
                    }
                }

            })
        }
        (appBarLayout.layoutParams as CoordinatorLayout.LayoutParams).apply {
            behavior = (behavior as? AppBarLayout.Behavior
                ?: AppBarLayout.Behavior()).apply {
                setDragCallback(
                    object : AppBarLayout.Behavior.DragCallback() {

                        override fun canDrag(appBarLayout: AppBarLayout) = false
                    }
                )
            }
        }
        appBarLayout.addOnLayoutChangeListener { v, _, _, _, _, _, _, _, _ ->
            collapsingToolbarLayout?.scrimVisibleHeightTrigger = v.height -
                    imageDv.height + toolbar.height + statusBarController.statusBarHeight
        }
        playerViewModel.addPlayerEventListener(
            viewLifecycleOwner,
            object : Player.DefaultEventListener() {

                override fun onPlayerError(error: ExoPlaybackException) {
                    setPlayerView(null, null)
                    this@ChallengeFragment.onPlayerError(error)
                }

            }
        )

        val tagAdapter = TagAdapter()
        val challengeAdapter= ChallengeSegmentAdapter.create(
            lifecycleRegistrar =  lifecycleRegistrar,
            postDataProvider = challengeViewModel.postsDataProvider,
            onJoinClick = {
                challengeViewModel.join()
            },
            onLeaveClick = {
                challengeViewModel.leave()
            },
            onParticipantsPreviewClick = {
                navigationController.navigateToFragment(
                    ParticipantsFragment.newInstance(argChallengeId)
                )
            },
            onBindPlayerViewTag = { playerView: ThumbnailPlayerView ->
                playerView.setLifecycleOwner(this)
                if (playerViewTag != null && playerView.tag == playerViewTag) {
                    setPlayerView(playerView, null)
                }
            },
            onClickPlayerView = { player: ThumbnailPlayerView, uri: Uri ->
                setPlayerView(player, uri)
            },
            onAuthorClick = navigationController::navigateToUserProfile,
            onPostClick = {
                navigateToPost(it)
            },
            onPostAuthorClick = {
                it.author?.id?.also(navigationController::navigateToUserProfile)
            },
            onPostLocationClick = {
                openLocation(it)
            },
            onPostRatingUp = {
                it.id?.also(challengeViewModel::upPostRating)
            },
            onPostRatingDown = {
                it.id?.also(challengeViewModel::downPostRating)
            },
            onPostCommentsClick = {
                navigateToPost(it)
            }
        )

        challengeRv.apply {
            adapter = challengeAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            itemAnimator = null
            addOnScrollListener(
                object : RecyclerView.OnScrollListener() {

                    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        if (recyclerView != null) {
                            newPostFab?.also {
                                if (challengeViewModel.challengeLD.value?.data?.participating != null) {
                                    val canScrollUp = recyclerView.canScrollVertically(Int.MIN_VALUE)
                                    val canScrollDown = recyclerView.canScrollVertically(Int.MAX_VALUE)

                                    if (canScrollDown) {
                                        if (!it.isShown) {
                                            it.show()
                                            isFabHiddenByScroll = false
                                        }
                                    } else if (
                                        it.isShown &&
                                        isFabHideByScrollEnabled &&
                                        canScrollUp
                                    ) {
                                        it.hide()
                                        isFabHiddenByScroll = true
                                    }
                                }
                            }
                        }
                    }

                }
            )
        }
        tagsRv.apply {
            adapter = tagAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }

        challengeViewModel.postsDataProvider.apply {
            lifecycleRegistrar.registerImmediately(
                object : DataProvider.StatusListener {

                    override fun onStatusChange(
                        isFetching: Boolean,
                        isRefreshing: Boolean,
                        isFullResult: Boolean,
                        isEmpty: Boolean,
                        isError: Boolean
                    ) {
                        challengeAdapter.setPostsStatus(isFullResult && isEmpty)
                        isFabHideByScrollEnabled = isFullResult && !isEmpty
                    }

                },
                ::registerStatusListener,
                ::unregisterStatusListener
            )
        }

        challengeViewModel.challengeLD.observe(viewLifecycleOwner, Observer {
            it?.data?.also {
                challengeAdapter.setChallengeParticipating(it)
                it.challenge?.also {
                    it.title?.also { setTitle(it) }
                    it.image?.also { setChallengeImage(it) }
                    it.tags?.also { tagAdapter.setTags(it) }
                    it.imageColor?.also {
                        collapsingToolbarLayout.setBackgroundColor(it)
                        collapsingToolbarLayout.setContentScrimColor(it)
                        toolbarContentScrim.setBackgroundColor(it)
                        tagAdapter.setTagsColor(it)
                        newPostFab.backgroundTintList = ColorStateList.valueOf(it)
                        darkenColor(it).also {
                            collapsingToolbarLayout.setStatusBarScrimColor(it)
                            statusBarScrim.setBackgroundColor(it)
                        }
                    }
                    shareIntent = Intent.createChooser(Intent()
                        .setAction(Intent.ACTION_SEND)
                        .putExtra(Intent.EXTRA_TEXT, it.toShareString())
                        .setType("text/plain"),
                        resources.getText(R.string.share_via)
                    )
                }
                newPostFab.apply {
                    if (it.participating != null)  {
                        if (!isFabHiddenByScroll) show()
                    } else {
                        hide()
                    }
                }
                State.apply { setState(State.RESULT) }
            } ?: it?.exception?.also {
                onDataError(it)
                State.apply { setState(State.ERROR) }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }

        appBarController.value?.isCollapsed?.also {
            outState.putBoolean(BUNDLE_IS_APP_BAR_COLLAPSED, it)
        }
        currentPlayerView.value?.tag?.toString()?.also {
            outState.putString(BUNDLE_PLAYER_VIEW_TAG, it)
        }
    }

    override fun onStop() {
        super.onStop()

        if (isRemoving) {
            appBarController.value?.isCollapsed?.also {
                isAppBarCollapsed = it
            }
            currentPlayerView.value?.player?.playWhenReady = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        statusBarController.setStatusBarColor(null)
    }

    override fun onMenuItemClick(item: MenuItem) = when (item.itemId) {
        R.id.action_share -> {
            shareIntent?.also { navigationController.startActivity(it) }
            true
        }

        else -> false
    }

    private fun setTitle(title: String) {
        toolbar.title = title
        expandedTitleTv.text = title
    }

    private fun setChallengeImage(image: Image) {
        val imageRequest = ImageRequestBuilder
            .newBuilderWithSource(Uri.parse(image.url))
            .setRotationOptions(RotationOptions.autoRotate())
            .build()

        val controller = Fresco.newDraweeControllerBuilder()
            .setOldController(imageDv.controller)
            .setImageRequest(imageRequest)
            .build()

        imageDv.controller = controller
    }

    private fun setPlayerView(playerView: ThumbnailPlayerView?, uri: Uri?) {
        currentPlayerView.value?.apply {
            if (this != playerView) {
                player?.stop(true)
                player = null
            }
        }
        currentPlayerView.value = playerView

        playerView?.also {
            playerViewModel.startVideo(it, uri)
        }
    }

    private fun onRetryClick() {
        State.apply { setState(State.LOADING) }
        challengeViewModel.refresh()
    }

    private fun onDataError(e: DataException) {
        stateLayout.textView.text = when (e.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_search_no_challenge)

            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)

            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)

            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)

            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)

            else -> e.message
        }
    }

    private fun onPlayerError(e: ExoPlaybackException) {
        onExoPlayerError(context, e, getString(R.string.err_video))
    }

    private fun navigateToPost(post: Post) {
        if (post.id != null) {
            challengeViewModel.challengeLD.value?.data?.challenge?.also { challenge ->
                navigationController.navigateToFragment(
                    PostFragment.newInstance(
                        challenge.id!!,
                        challenge.title!!,
                        challenge.imageColor!!,
                        post.id
                    )
                )
            }
        }
    }

    companion object {

        fun newInstance(challengeId: String) = ChallengeFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_CHALLENGE_ID, challengeId)
            }
        }

    }

    private inner class AppBarController(
        val titleTv: TextView,
        isCollapsed: Boolean
    ) : AppBarLayout.OnOffsetChangedListener {

        var isCollapsed: Boolean = isCollapsed
            private set

        private var isScrimShown: Boolean
        private var isShowTitle: Boolean

        init {
            isShowTitle = isCollapsed
            isScrimShown = isCollapsed
            if (isCollapsed) {
                toolbarContentScrim.alpha = 1f
                statusBarScrim.alpha = 1f
                titleTv.alpha = 1f
            } else {
                toolbarContentScrim.alpha = 0f
                statusBarScrim.alpha = 0f
                titleTv.alpha = 0f
            }
        }

        override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
            val toolbar = collapsingToolbarLayout ?: return
            val height = toolbar.scrimVisibleHeightTrigger - appBarLayout.height
            isCollapsed = verticalOffset < height
            if (isCollapsed) {
                if (!isScrimShown) {
                    val scrimAlpha = (height - verticalOffset).toFloat() /
                            (appBarLayout.totalScrollRange + height).toFloat()
                    isScrimShown = scrimAlpha == 1f
                    toolbarContentScrim.alpha = scrimAlpha
                    statusBarScrim.alpha = scrimAlpha
                    if (!isShowTitle) {
                        titleTv.animate()
                            .alpha(1f)
                            .setStartDelay(toolbar.scrimAnimationDuration)
                            .setDuration(
                                if (toolbarContentScrim.alpha != 1f) toolbar.scrimAnimationDuration
                                else 0
                            )
                            .start()
                        isShowTitle = true
                    }
                }
            } else {
                toolbarContentScrim.alpha = 0f
                statusBarScrim.alpha = 0f
                isScrimShown = false
                if (isShowTitle) {
                    titleTv.animate().cancel()
                    titleTv.alpha = 0f
                    isShowTitle = false
                }
            }
        }

    }

    private enum class State(override val state: ChallengeFragment.() -> Unit) : StateAct<ChallengeFragment> {
        RESULT({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.GONE
            challengeRv.visibility = View.VISIBLE
            appBarLayout.visibility = View.VISIBLE
            isChallengeRvAnimationsEnabled = true
        }),
        LOADING({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.VISIBLE
            challengeRv.visibility = View.INVISIBLE
            appBarLayout.visibility = View.INVISIBLE
            isChallengeRvAnimationsEnabled = false
        }),
        ERROR({
            stateLayout.textView.visibility = View.VISIBLE
            stateLayout.button.visibility = View.VISIBLE
            stateLayout.progressBar.visibility = View.GONE
            challengeRv.visibility = View.INVISIBLE
            appBarLayout.visibility = View.INVISIBLE
            isChallengeRvAnimationsEnabled = true
        });

        companion object : StateController<ChallengeFragment, State>(
            State.LOADING,
            { state: String -> State.valueOf(state) }
        )

    }

}