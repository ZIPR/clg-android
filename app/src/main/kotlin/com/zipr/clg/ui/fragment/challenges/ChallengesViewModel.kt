package com.zipr.clg.ui.fragment.challenges

import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.entity.ChallengeParticipating
import com.zipr.clg.data.interactor.ChallengeInteractor
import com.zipr.clg.data.paging.provider.QueryDataProvider
import com.zipr.clg.util.calculateFetchLimit
import javax.inject.Inject

private const val ITEM_VIEW_HEIGHT_DP = 160

class ChallengesViewModel @Inject constructor(
    private val challengeInteractor: ChallengeInteractor
) : ViewModel() {
    
    private var _challengesProvider: QueryDataProvider<ChallengeParticipating>? = null
    val challengesProvider get() = _challengesProvider
            ?: challengeInteractor.getChallenges(
                calculateFetchLimit(ITEM_VIEW_HEIGHT_DP)
            ).also {
                _challengesProvider = it
            }
    
    init {
        challengesProvider.query = ""
    }
    
    fun clear() {
        _challengesProvider = null
    }

}