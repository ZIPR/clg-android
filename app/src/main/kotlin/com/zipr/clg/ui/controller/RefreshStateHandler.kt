package com.zipr.clg.ui.controller

interface RefreshStateHandler {
    
    fun onRefreshState() : Boolean
    
}