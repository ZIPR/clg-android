package com.zipr.clg.ui.fragment.profile

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import com.orhanobut.logger.Logger
import com.zipr.clg.data.*
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.User
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.util.center
import com.zipr.clg.util.observeSingleEvent
import com.zipr.clg.util.toOriginalOrientation
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val userInteractor: UserInteractor) : ViewModel() {
    
    lateinit var userLD: LiveData<Document<User?>>
    
    var processedPhoto: Bitmap? = null
    var profilePhoto: Bitmap? = null
    
    var uploadResultLD = MediatorLiveData<AsyncResult<Uri, DataException>>()
        private set
    private var currentUploadLD: LiveData<AsyncResult<Uri, DataException>>? = null

    var processResultLD = MediatorLiveData<AsyncResult<Bitmap, Throwable>>()
        private set
    private var currentProcessLD: LiveData<AsyncResult<Bitmap, Throwable>>? = null

    private val _usernameValidationResultLD = MutableLiveData<Document<User?>>()
    val usernameValidationResultLD get() = _usernameValidationResultLD as LiveData<Document<User?>>
    
    private var userId: String? = null
    
    fun setUser(userId: String) {
        if (userId == this.userId) {
            return
        }
        
        this.userId = userId
    
        userLD = userInteractor.getUser(userId, true)
    }
    
    fun getCurrentUser(): User? = if (this::userLD.isInitialized) userLD.value?.data else null
    
    fun checkUsernameAvailability(username: String) {
        _usernameValidationResultLD.value = null
        userInteractor.getUserByUsername(username, false).observeSingleEvent {
            _usernameValidationResultLD.postValue(it)
        }
    }
    
    fun updateUserInfo(username: String?, fullName: String?, bio: String?, photoUrl: String?) {
        userInteractor.updateUser(
            User(
                username,
                fullName,
                bio,
                photoUrl,
                null,
                null,
                userId
            )
        )
    }
    
    fun processImage(context: Context, uri: Uri, resultHeight: Int, resultWidth: Int) {
        cancelImageProcessing()
        currentProcessLD = AsyncResult.fromDeferred(
            async(CommonPool) {
                MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                    .center(resultHeight, resultWidth)
                    .toOriginalOrientation(context, uri)
            },
            ::AsyncResult
        ).also {
            processResultLD.addSource(it) {
                processResultLD.value = it
            }
        }
    }
    
    fun uploadImage(image: Bitmap) {
        cancelImageUploading()
        userId?.also {
            val storageReference = firebaseStorage.reference.child(getUserAvatarPath(it))
            currentUploadLD = uploadBitmap(storageReference, image).also {
                uploadResultLD.addSource(it) {
                    uploadResultLD.value = it
                }
            }
        } ?: Logger.e("Unable to upload image: userId is null")
    }
    
    fun cancelImageProcessing() {
        currentProcessLD?.also {
            processResultLD.removeSource(it)
        }
    }

    fun cancelImageUploading() {
        currentUploadLD?.also {
            uploadResultLD.removeSource(it)
        }
    }
    
    fun clearEditData() {
        processedPhoto = null
    }
    
    public override fun onCleared() {
        clearEditData()
        profilePhoto = null
    }
    
}