package com.zipr.clg.ui.fragment.userprofile

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.zipr.clg.R
import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.fragment.userlist.followers.FollowersFragment
import com.zipr.clg.ui.fragment.userlist.following.FollowingFragment
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.orZero
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.fragment_user_profile.*
import kotlinx.android.synthetic.main.item_profileinfo_followersbar.*
import kotlinx.android.synthetic.main.item_profileinfo_idle.*
import javax.inject.Inject

private const val ARG_USER_ID = "ARG_USER_ID"

@Injectable
class UserProfileFragment : ViewLifecycleFragment() {
    
    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    
    private val userProfileViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(UserProfileViewModel::class.java)
    }
    
    private val userObserver = Observer<UserFollower> {
        it?.also {
            fullNameTv.text = it.user?.fullName.orEmpty()
            usernameTv.text = "@${it.user?.username.orEmpty()}"
            bioTv.text = it.user?.bio.orEmpty()
            followersCountTv.text = it.user?.followersCount.orZero().toString()
            followingCountTv.text = it.user?.followingCount.orZero().toString()
            
            setProfilePhoto(it.user?.photoUrl.orEmpty())
            setFollowingStatus(it.follower != null)
        }
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_user_profile, container, false)
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    
        val userId = arguments!!.getString(ARG_USER_ID)
        
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
        
        firebaseUser?.uid?.also {
            userProfileViewModel.setUser(userId, it)
        } ?: showToast(context, getString(R.string.err_unauthorized))
        
        followersCl.setOnClickListener {
            navigationController.navigateToFragment(FollowersFragment.newInstance(userId))
        }
        followingCl.setOnClickListener {
            navigationController.navigateToFragment(FollowingFragment.newInstance(userId))
        }
    
        userProfileViewModel.userFollowerLD.observe(viewLifecycleOwner, userObserver)
    }
    
    private fun setProfilePhoto(uri: String) {
        val imageRequest = ImageRequestBuilder
            .newBuilderWithSource(Uri.parse(uri))
            .build()
    
        photoDv.controller = Fresco.newDraweeControllerBuilder()
            .setOldController(photoDv.controller)
            .setImageRequest(imageRequest)
            .setTapToRetryEnabled(true)
            .build()
    }
    
    private fun setFollowingStatus(isFollowing: Boolean) {
        profileBtn.apply {
            if (isFollowing) {
                setText(R.string.following)
                isActive = false
                setOnClickListener { userProfileViewModel.unfollow() }
            } else {
                setText(R.string.follow)
                isActive = true
                setOnClickListener { userProfileViewModel.follow() }
            }
        }
    }
    
    companion object {
        
        fun newInstance(userId: String) = UserProfileFragment().apply {
            arguments = Bundle().apply { putString(ARG_USER_ID, userId) }
        }
        
    }
    
}
