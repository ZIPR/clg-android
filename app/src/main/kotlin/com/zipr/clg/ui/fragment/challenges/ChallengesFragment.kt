package com.zipr.clg.ui.fragment.challenges

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.*
import com.zipr.clg.ui.fragment.challenge.ChallengeFragment
import com.zipr.clg.ui.fragment.challengecreation.ChallengeCreationFragment
import com.zipr.clg.util.BackpressureData
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.abc_search_view.view.*
import kotlinx.android.synthetic.main.fragment_challenges.*
import kotlinx.android.synthetic.main.state_layout.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

private const val NEW_CHALLENGE_BUTTON_ANIM_DURATION = 200L
private const val SEARCH_INSTANT_TIME_PERIOD_MILLIS = 500L

@Injectable
class ChallengesFragment : ViewLifecycleFragment(),
    SearchQueryHandler,
    RefreshStateHandler,
    BackPressHandler {

    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val challengesViewModel by lazy { viewModelProvider.get(ChallengesViewModel::class.java) }

    private val lifecycleRegistrar by lazy { LifecycleRegistrar(viewLifecycleOwner) }
    private val queryLD = BackpressureData<String>(SEARCH_INSTANT_TIME_PERIOD_MILLIS)

    private lateinit var searchIconIv: ImageView
    private var isSearchDefaultState by Delegates.observable(true) { _, _, new ->
        searchIconIv.apply {
            isEnabled = !new
            setImageResource(
                if (new) R.drawable.ic_search
                else R.drawable.ic_arrow_back
            )
        }
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_challenges, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        searchIconIv = searchView.search_mag_icon
        searchIconIv.setOnClickListener {
            onRefreshState()
        }
        stateLayout.button.apply {
            text = getString(R.string.retry)
            setOnClickListener { onRetryClick() }
        }
        newChallengeFab.setOnClickListener { onNewChallengeClick() }
        newChallengeBtn.setOnClickListener { onNewChallengeClick() }

        searchView.apply {
            activity?.also { setSearchableInfo(ClgApplication.getSearchableInfo(it)) }
            isIconified = false
            clearFocus()
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }
        
                override fun onQueryTextChange(newText: String?): Boolean {
                    queryLD.postValue(newText)
                    return false
                }
        
            })
        }
        queryLD.observe(viewLifecycleOwner, Observer {
            it?.also { onSearchQuery(it) }
        })
        isSearchDefaultState = challengesViewModel.challengesProvider.query.isNullOrBlank()

        val challengeAdapter = ChallengeAdapter(
            challengesViewModel.challengesProvider,
            lifecycleRegistrar,
            {
                it.id?.also {
                    navigationController.navigateToFragment(
                        ChallengeFragment.newInstance(it)
                    )
                }
            }
        )
        val challengesLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val searchViewFocusController = SearchViewFocusController()
        val newChallengeButtonController = NewChallengeButtonController()
        challengesRv.apply {
            adapter = challengeAdapter.withPaginationStatus(16)
            layoutManager = challengesLayoutManager
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            addOnScrollListener(searchViewFocusController)
            addOnScrollListener(newChallengeButtonController)
        }
        newChallengeBtn.addOnLayoutChangeListener { _, _, top: Int, _, bottom: Int, _, _, _, _ ->
            newChallengeButtonController.setNewChallengeButtonParams(top.toFloat(), bottom.toFloat())
        }

        challengesSrl.apply {
            setColorSchemeColors(
                ClgApplication.getColor(R.color.colorAccent)
            )
            setOnRefreshListener {
                challengesViewModel.challengesProvider.refreshData()
            }
        }

        challengesViewModel.challengesProvider.apply {
            lifecycleRegistrar.registerImmediately(
                object : DataProvider.StatusListener {

                    override fun onStatusChange(
                        isFetching: Boolean,
                        isRefreshing: Boolean,
                        isFullResult: Boolean,
                        isEmpty: Boolean,
                        isError: Boolean
                    ) {
                        State.apply {
                            setState(
                                when {
                                    !isError -> if (isEmpty && isFetching) State.SEARCHING else State.RESULT
                                    
                                    isEmpty -> State.ERROR
                                    
                                    else -> {
                                        if (challengesSrl.isRefreshing) {
                                            showToast(context, getString(R.string.err_refresh))
                                        }
                                        State.RESULT
                                    }
                                }
                            )
                        }
                        challengesSrl.isRefreshing = isRefreshing
                    }

                },
                ::registerStatusListener,
                ::unregisterStatusListener
            ).registerImmediately(
                object : DataProvider.ErrorListener {

                    override fun onFetchError(exception: DataException) {
                        onDataError(exception)
                    }

                },
                ::registerErrorListener,
                ::unregisterErrorListener
            )
        }
    }
    
    override fun onDestroyView() {
        super.onDestroyView()
        
        if (isRemoving && challengesViewModel.challengesProvider.getItemCount() == 0) {
            challengesViewModel.clear()
        }
    }

    override fun onSearchQuery(query: String) {
        val trimmedQuery = query.trim()
        isSearchDefaultState = trimmedQuery.isEmpty()
        if (challengesViewModel.challengesProvider.query == trimmedQuery) {
            return
        }
        State.apply { setState(State.SEARCHING) }
        if (!query.contentEquals(searchView.query)) {
            searchView.setQuery(query, false)
        }
        challengesRv.scrollToPosition(0)
        challengesViewModel.challengesProvider.query = trimmedQuery
    }
    
    private fun onNewChallengeClick() {
        navigationController.navigateToFragment(
            ChallengeCreationFragment.newInstance()
        )
    }

    private fun onRetryClick() {
        searchView.clearFocus()
        challengesViewModel.challengesProvider.retryFetching()
    }
    
    override fun onRefreshState() =
        if (!isSearchDefaultState) {
            isSearchDefaultState = true
            State.apply { resetState() }
            searchView.setQuery(null, false)
            searchView.clearFocus()
            challengesViewModel.challengesProvider.query = null
            true
        } else {
            false
        }

    override fun handleBackPress() = onRefreshState()
    
    private fun onDataError(e: DataException) {
        stateLayout.textView.text = when (e.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_search_no_challenge)
    
            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)
    
            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)
    
            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)
    
            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)
    
            else -> e.message
        }
    }

    companion object {

        fun newInstance() = ChallengesFragment()

    }

    private inner class SearchViewFocusController : RecyclerView.OnScrollListener() {

        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            if (newState != RecyclerView.SCROLL_STATE_IDLE) {
                searchView.clearFocus()
            }
        }

    }

    private inner class NewChallengeButtonController : RecyclerView.OnScrollListener() {

        private var newChallengeYShow = 0f
        private var newChallengeYHide = 0f
        private var isNewChallengeVisible = true
            set(value) {
                if (field != value) {
                    field = value
                    if (value) {
                        newChallengeFab.hide()
                        newChallengeBtn.animate()
                            .y(newChallengeYShow)
                            .setInterpolator(LinearOutSlowInInterpolator())
                            .setDuration(NEW_CHALLENGE_BUTTON_ANIM_DURATION)
                            .start()
                    } else {
                        newChallengeFab.show()
                        newChallengeBtn.animate()
                            .y(newChallengeYHide)
                            .setInterpolator(FastOutLinearInInterpolator())
                            .setDuration(NEW_CHALLENGE_BUTTON_ANIM_DURATION)
                            .start()
                    }
                }
            }

        fun setNewChallengeButtonParams(newChallengeYShow: Float, newChallengeYHide: Float) {
            if (this.newChallengeYShow != newChallengeYShow || this.newChallengeYHide != newChallengeYHide) {
                newChallengeBtn.animate().cancel()
                newChallengeFab.animate().cancel()

                this.newChallengeYShow = newChallengeYShow
                this.newChallengeYHide = newChallengeYHide
                if (isNewChallengeVisible) {
                    newChallengeBtn.y = newChallengeYShow
                    newChallengeFab.alpha = 0f
                    newChallengeFab.scaleX = 0f
                    newChallengeFab.scaleY = 0f
                    newChallengeFab.visibility = View.GONE
                } else {
                    newChallengeBtn.y = newChallengeYHide
                    newChallengeFab.alpha = 1f
                    newChallengeFab.scaleX = 1f
                    newChallengeFab.scaleY = 1f
                    newChallengeFab.visibility = View.VISIBLE
                }
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            isNewChallengeVisible = !recyclerView.canScrollVertically(Int.MIN_VALUE)
        }

    }

    private enum class State(override val state: ChallengesFragment.() -> Unit) : StateAct<ChallengesFragment> {
        RESULT({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.GONE
            challengesRv.visibility = View.VISIBLE
            challengesSrl.isEnabled = true
        }),
        SEARCHING({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.VISIBLE
            challengesRv.visibility = View.INVISIBLE
            challengesSrl.isEnabled = false
        }),
        ERROR({
            stateLayout.textView.visibility = View.VISIBLE
            stateLayout.button.visibility = View.VISIBLE
            stateLayout.progressBar.visibility = View.GONE
            challengesRv.visibility = View.INVISIBLE
            challengesSrl.isEnabled = false
        });
    
        companion object : StateController<ChallengesFragment, State>(
            State.SEARCHING,
            { state: String -> State.valueOf(state) }
        )
        
    }

}