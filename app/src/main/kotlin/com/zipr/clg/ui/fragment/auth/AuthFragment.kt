package com.zipr.clg.ui.fragment.auth

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.orhanobut.logger.Logger
import com.zipr.clg.R
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.controller.StateAct
import com.zipr.clg.ui.controller.StateController
import com.zipr.clg.ui.fragment.reg.RegFragment
import com.zipr.clg.util.firebaseAuth
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.fragment_auth.*
import javax.inject.Inject

private const val RC_GOOGLE_SIGN_IN = 100
private val RC_FACEBOOK_SIGN_IN = CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()

private const val AUTH_GOOGLE_CANCELED_STATUS = 12501
private const val AUTH_FACEBOOK_NO_NETWORK_MESSAGE = "net::ERR_INTERNET_DISCONNECTED"

@Injectable
class AuthFragment : Fragment() {

    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val authViewModel by lazy { viewModelProvider.get(AuthViewModel::class.java) }

    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var facebookCallbackManager: CallbackManager

    private var emailError: String
        get() = emailErrorTV.text.toString()
        set(value) {
            emailErrorTV.text = value
            if (!value.isEmpty()) {
                emailET.background.setColorFilter(
                    ContextCompat.getColor(context!!, R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                emailET.background.clearColorFilter()
            }
        }

    private var passwordError: String
        get() = passwordErrorTV.text.toString()
        set(value) {
            passwordErrorTV.text = value
            if (!value.isEmpty()) {
                passwordET.background.setColorFilter(
                    ContextCompat.getColor(context!!, R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                passwordET.background.clearColorFilter()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_auth, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    
        State.apply { restoreState(savedInstanceState) }
    
        signInBtn.setOnClickListener {
            signIn()
        }
        googleBtn.setOnClickListener {
            State.apply { setState(State.SIGN_IN_GOOGLE) }
            startActivityForResult(googleSignInClient.signInIntent, RC_GOOGLE_SIGN_IN)
        }
        facebookBtn.setOnClickListener {
            State.apply { setState(State.SIGN_IN_FACEBOOK) }
            LoginManager.getInstance().logInWithReadPermissions(
                this,
                resources.getStringArray(R.array.facebook_login_permissions).asList()
            )
        }
        forgotPasswordTV.setOnClickListener {
            restorePassword()
        }
        emailET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                validateEmail(emailET.text.toString().trim())
            }
            false
        }
        passwordET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signIn()
            }
            false
        }
        newAccountTV.setOnClickListener {
            navigationController.navigateToFragment(RegFragment.newInstance())
        }

        val authObserver = Observer<Task<AuthResult>> {
            it?.also {
                if (it.isSuccessful) {
                    State.apply { setState(State.SIGNED_IN) }
                } else {
                    State.apply { setState(State.IDLE) }
                    onAuthError(it.exception)
                }
            }
        }
        authViewModel.authLiveData.observe(this, authObserver)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.google_client_id_token))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        val facebookCallback = object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                signInWithFacebook(loginResult)
            }

            override fun onCancel() {
                State.apply { setState(State.IDLE) }
            }

            override fun onError(e: FacebookException) {
                State.apply { setState(State.IDLE) }
                onAuthError(
                    if (e.message != AUTH_FACEBOOK_NO_NETWORK_MESSAGE) e
                    else FirebaseNetworkException(AUTH_FACEBOOK_NO_NETWORK_MESSAGE)
                )
            }
        }
        facebookCallbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(facebookCallbackManager, facebookCallback)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_GOOGLE_SIGN_IN -> signInWithGoogle(GoogleSignIn.getSignedInAccountFromIntent(data))
            RC_FACEBOOK_SIGN_IN -> facebookCallbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun signIn() {
        val email = emailET.text.toString().trim()
        val password = passwordET.text.toString().trim()
        if (validateEmail(email) and validatePassword(password)) {
            State.apply { setState(State.SIGN_IN_EMAIL) }
            authViewModel.signInWithEmailAndPassword(email, password)
        }
    }

    private fun signInWithGoogle(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            authViewModel.signInWithCredential(credential)
        } catch (e: ApiException) {
            State.apply { setState(State.IDLE) }
            if (e.statusCode != AUTH_GOOGLE_CANCELED_STATUS) {
                onAuthError(e)
            }
        }
    }

    private fun signInWithFacebook(loginResult: LoginResult) {
        val credential = FacebookAuthProvider.getCredential(loginResult.accessToken.token)
        authViewModel.signInWithCredential(credential)
    }

    private fun restorePassword() {
        val email = emailET.text.toString()
        if (validateEmail(email)) {
            State.apply { setState(State.RESTORE_PASSWORD) }
            firebaseAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(activity!!, {
                    State.apply { setState(State.IDLE) }
                    if (it.isSuccessful) {
                        showToast(context!!, getString(R.string.email_reset_pass_sent))
                    } else {
                        onAuthError(it.exception)
                    }
                })
        }
    }

    private fun onAuthError(e: Exception?) {
        Logger.e(e, "auth error")
        when(e) {
            is FirebaseAuthInvalidUserException -> {
                emailError = getString(R.string.err_email_not_reg)
            }
            is FirebaseAuthInvalidCredentialsException -> {
                passwordError = getString(R.string.err_password_incorrect)
            }
            is FirebaseNetworkException -> {
                showToast(context!!, getString(R.string.err_no_network))
            }
            is FirebaseTooManyRequestsException -> {
                showToast(context!!, getString(R.string.err_many_requests))
            }
            is ApiException -> {
                showToast(context!!, getString(R.string.err_auth_google_fail))
            }
            is FacebookException -> {
                showToast(context!!, getString(R.string.err_auth_facebook_fail))
            }
            else -> {
                showToast(
                    context!!,
                    getString(R.string.err_auth_failed, e?.message)
                )
            }
        }
    }
    
    private fun validateEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()
        .also { emailError = if (!it) getString(R.string.err_email_valid) else "" }
    
    private fun validatePassword(password: String) = password.matches(
        Regex(
            getString(
                R.string.pattern_password,
                resources.getInteger(R.integer.password_length_min),
                resources.getInteger(R.integer.password_length_max)
            )
        )
    ).also { passwordError = if (!it) getString(R.string.err_password_valid) else "" }
    
    private fun setSignInEnabled(isEnabled: Boolean) {
        signInBtn.isEnabled = isEnabled
        googleBtn.isEnabled = isEnabled
        facebookBtn.isEnabled = isEnabled
        forgotPasswordTV.isEnabled = isEnabled
        emailET.isEnabled = isEnabled
        passwordET.isEnabled = isEnabled
        emailET.clearFocus()
        passwordET.clearFocus()
    }

    companion object {
        fun newInstance() = AuthFragment()
    }

    private enum class State(override val state: AuthFragment.() -> Unit) : StateAct<AuthFragment> {
        START({
            setState(if (firebaseUser != null) State.SIGNED_IN else State.IDLE)
        }),
        IDLE({
            authViewModel.resetAuth()
            setSignInEnabled(true)
        }),
        SIGN_IN({
            setSignInEnabled(false)
        }),
        SIGN_IN_EMAIL({
            SIGN_IN.state(this)
            emailError = ""
            passwordError = ""
        }),
        SIGN_IN_GOOGLE({
            SIGN_IN.state(this)
        }),
        SIGN_IN_FACEBOOK({
            SIGN_IN.state(this)
        }),
        SIGNED_IN({
            setSignInEnabled(false)
            navigationController.navigateToMain()
        }),
        RESTORE_PASSWORD({
            setSignInEnabled(false)
        });

        companion object : StateController<AuthFragment, State>(
            START,
            { state: String -> State.valueOf(state) }
        )
        
    }

}