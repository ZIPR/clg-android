package android.support.design.widget

import android.content.Context
import android.util.AttributeSet

class FixedMeasureCollapsingToolbarLayout : CollapsingToolbarLayout {
    
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    
    // fix the bottom empty padding
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val mode = MeasureSpec.getMode(heightMeasureSpec)
        val topInset = if (mLastInsets != null) mLastInsets.systemWindowInsetTop else 0
        if (mode == MeasureSpec.UNSPECIFIED && topInset > 0) {
            super.onMeasure(
                widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(measuredHeight - topInset, MeasureSpec.EXACTLY)
            )
        }
    }

}