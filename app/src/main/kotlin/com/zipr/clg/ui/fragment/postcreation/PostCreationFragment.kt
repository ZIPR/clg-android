package com.zipr.clg.ui.fragment.postcreation

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.firebase.firestore.GeoPoint
import com.zipr.clg.R
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.onExoPlayerError
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.adapter.EditableImageAdapter
import com.zipr.clg.ui.adapter.EditableLocationAdapter
import com.zipr.clg.ui.adapter.EditableVideoAdapter
import com.zipr.clg.ui.adapter.SegmentAdapter
import com.zipr.clg.ui.common.AutoClearedValue
import com.zipr.clg.ui.common.PlayerViewModel
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.*
import com.zipr.clg.ui.fragment.challenge.ChallengeFragment
import com.zipr.clg.ui.fragment.post.PostFragment
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.*
import kotlinx.android.synthetic.main.fragment_post_creation.*
import javax.inject.Inject

private const val ARG_CHALLENGE_ID = "ARG_CHALLENGE_ID"
private const val ARG_CHALLENGE_TITLE = "ARG_CHALLENGE_TITLE"
private const val ARG_CHALLENGE_COLOR = "ARG_CHALLENGE_COLOR"
private const val ARG_CHALLENGE_DARK_COLOR = "ARG_CHALLENGE_DARK_COLOR"

private const val BUNDLE_IMAGES = "BUNDLE_IMAGES"
private const val BUNDLE_VIDEOS = "BUNDLE_VIDEOS"
private const val BUNDLE_LOCATION = "BUNDLE_LOCATION"
private const val BUNDLE_PLAYER_VIEW_TAG = "BUNDLE_PLAYER_VIEW_TAG"

private const val RC_IMAGE_PICK = 100
private const val RC_VIDEO_PICK = 101
private const val RC_LOCATION_PICK = 102

@Injectable
class PostCreationFragment : ViewLifecycleFragment(), FinishHandler {

    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var statusBarController: StatusBarController
    @Inject lateinit var bottomNavigationBarController: BottomNavigationBarController

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val postCreationViewModel by lazy { viewModelProvider.get(PostCreationViewModel::class.java) }
    private val playerViewModel by lazy { viewModelProvider.get(PlayerViewModel::class.java) }

    private val imageAdapter by lazy { AutoClearedValue<EditableImageAdapter>(viewLifecycleOwner) }
    private val videoAdapter by lazy { AutoClearedValue<EditableVideoAdapter>(viewLifecycleOwner) }
    private val locationAdapter by lazy { AutoClearedValue<EditableLocationAdapter>(viewLifecycleOwner) }

    private val currentPlayerView by lazy { AutoClearedValue<ThumbnailPlayerView>(viewLifecycleOwner) }

    private var alertDialog: AlertDialog? = null

    private lateinit var challengeId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_post_creation, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        challengeId = arguments!!.getString(ARG_CHALLENGE_ID)
        val challengeTitle = arguments!!.getString(ARG_CHALLENGE_TITLE)
        val challengeColor = arguments!!.getInt(ARG_CHALLENGE_COLOR)
        val challengeDarkColor = arguments!!.getInt(ARG_CHALLENGE_DARK_COLOR)

        val playerViewTag = savedInstanceState?.getString(BUNDLE_PLAYER_VIEW_TAG)
        val imageUriList = savedInstanceState?.getStringArrayList(BUNDLE_IMAGES)
        val videoUriList = savedInstanceState?.getStringArrayList(BUNDLE_VIDEOS)
        val location = savedInstanceState?.getParcelable<Location>(BUNDLE_LOCATION)
        State.apply { restoreState(savedInstanceState) }

        bottomNavigationBarController.isNavigationBarVisible = false

        playerViewModel.addPlayerEventListener(
            viewLifecycleOwner,
            object : Player.DefaultEventListener() {

                override fun onPlayerError(error: ExoPlaybackException) {
                    setPlayerView(null, null)
                    this@PostCreationFragment.onPlayerError(error)
                }
            }
        )

        imageAdapter.value = EditableImageAdapter(
            imageUriList = imageUriList ?: emptyList()
        )
        videoAdapter.value = EditableVideoAdapter(
            videoUriList = videoUriList ?: emptyList(),
            onBindPlayerViewTag = { playerView: ThumbnailPlayerView ->
                playerView.setLifecycleOwner(this)
                if (playerViewTag != null && playerView.tag == playerViewTag) {
                    setPlayerView(playerView, null)
                }
            },
            onClickPlayerView = { player: ThumbnailPlayerView?, uri: Uri? ->
                setPlayerView(player, uri)
            }
        )
        locationAdapter.value = EditableLocationAdapter(
            location = location,
            onClickLocation = {
                location?.also { openLocation(it) }
            }
        )

        toolbar.also {
            it.title = challengeTitle
            it.setBackgroundColor(challengeColor)
            it.setNavigationIcon(R.drawable.ic_arrow_back)
            it.setNavigationOnClickListener { activity!!.onBackPressed() }
        }
        statusBarController.setStatusBarColor(challengeDarkColor)

        postTextTxInEt.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (!postTextTxInL.error.isNullOrEmpty()) {
                    validateText()
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        })

        postContentRv.apply {
            adapter = SegmentAdapter(imageAdapter.value!!, videoAdapter.value!!, locationAdapter.value!!)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            isNestedScrollingEnabled = false
        }

        addPostIv.setOnClickListener {
            createPost()
        }
        addImageIv.setOnClickListener {
            startImageChooser(RC_IMAGE_PICK)
        }
        addVideoIv.setOnClickListener {
            startVideoChooser(RC_VIDEO_PICK)
        }
        addLocationIv.setOnClickListener {
            startLocationChooser(RC_LOCATION_PICK)
        }

        postCreationViewModel.creatingPostLD.observeNonNull(viewLifecycleOwner) {
            if (it.result != null) {
                navigationController.navigateBack()
                navigationController.replaceWithFragment(
                    ChallengeFragment.newInstance(challengeId)
                )
                navigationController.navigateToFragment(
                    PostFragment.newInstance(challengeId, challengeTitle, challengeColor, it.result)
                )
            } else if (it.error != null) {
                State.apply { setState(State.IDLE) }
                onCreationError(it.error)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                RC_IMAGE_PICK -> data.data?.getContentPath(context!!)?.also {
                    imageAdapter.value?.addImage(it)
                }

                RC_VIDEO_PICK -> data.data?.getContentPath(context!!)?.also {
                    videoAdapter.value?.addVideo(it)
                }

                RC_LOCATION_PICK -> PlacePicker.getPlace(context, data).also {
                    locationAdapter.value?.location =
                        Location(
                            GeoPoint(it.latLng.latitude, it.latLng.longitude),
                            it.name?.toString(),
                            it.address?.toString()
                        )
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }

        currentPlayerView.value?.tag?.toString()?.also {
            outState.putString(BUNDLE_PLAYER_VIEW_TAG, it)
        }
        (imageAdapter.value?.imageUriList as? ArrayList<String>)?.also {
            outState.putStringArrayList(BUNDLE_IMAGES, it)
        }
        (videoAdapter.value?.videoUriList as? ArrayList<String>)?.also {
            outState.putStringArrayList(BUNDLE_VIDEOS, it)
        }
        locationAdapter.value?.location?.also {
            outState.putParcelable(BUNDLE_LOCATION, it)
        }
    }

    override fun onStop() {
        super.onStop()

        alertDialog?.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        statusBarController.setStatusBarColor(null)
        bottomNavigationBarController.isNavigationBarVisible = true

        if (isRemoving) {
            currentPlayerView.value?.player?.playWhenReady = false
        }
    }

    override fun onFinish(continueFinish: () -> Unit) {
        context?.also {
            alertDialog = AlertDialog.Builder(it)
                .setTitle(getString(R.string.cancel_post))
                .setPositiveButton(R.string.yes) { dialog, _ ->
                    dialog.cancel()
                    postCreationViewModel.cancel()
                    continueFinish()
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    dialog.cancel()
                }
                .show()
        } ?: continueFinish()
    }

    private fun setPlayerView(playerView: ThumbnailPlayerView?, uri: Uri?) {
        currentPlayerView.value?.apply {
            if (this != playerView) {
                player?.stop(true)
                player = null
            }
        }
        currentPlayerView.value = playerView

        playerView?.also {
            playerViewModel.startVideo(it, uri)
        }
    }

    private fun validateText() = postTextTxInEt.let {
        it.text.toString().run {
            if (length >= resources.getInteger(R.integer.post_text_length_min) &&
                length <= resources.getInteger(R.integer.post_text_length_max)) this
            else null
        }.also {
            postTextTxInL.error = if (it == null) getString(R.string.err_post_valid) else null
        }
    }

    private fun createPost() {
        postTextTxInEt.trimTextAndSelection()

        val text = validateText()
        val sourceImageLDList = imageAdapter.value!!.sourceImageLDList
        val sourceVideoList = videoAdapter.value!!.sourceVideoList
        val location = locationAdapter.value!!.location

        if (text == null) {
            showValidationToast(context, getString(R.string.err_post_valid))
            return
        }

        State.apply { setState(State.CREATING) }
        postCreationViewModel.createPost(
            challengeId,
            text,
            sourceImageLDList,
            sourceVideoList,
            location
        )
    }

    private fun onCreationError(e: Throwable) {
        showValidationToast(context, getString(R.string.err_creating_post, e.message))
    }

    private fun onPlayerError(e: ExoPlaybackException) {
        onExoPlayerError(context, e, getString(R.string.err_video))
    }

    companion object {

        fun newInstance(challengeId: String, challengeTitle: String, challengeColor: Int) =
            PostCreationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CHALLENGE_ID, challengeId)
                    putString(ARG_CHALLENGE_TITLE, challengeTitle)
                    putInt(ARG_CHALLENGE_COLOR, challengeColor)
                    putInt(ARG_CHALLENGE_DARK_COLOR, darkenColor(challengeColor))
                }
            }

    }

    private enum class State(override val state: PostCreationFragment.() -> Unit) :
        StateAct<PostCreationFragment> {
        IDLE({
            postTextTxInEt.isEnabled = true
            addImageIv.isEnabled = true
            addVideoIv.isEnabled = true
            addLocationIv.isEnabled = true
            imageAdapter.value?.isRemoveEnabled = true
            videoAdapter.value?.isRemoveEnabled = true
            locationAdapter.value?.isRemoveEnabled = true
            addPostIv.visibility = View.VISIBLE
            addingPostPb.visibility = View.GONE
        }),
        CREATING({
            postTextTxInEt.isEnabled = false
            addImageIv.isEnabled = false
            addVideoIv.isEnabled = false
            addLocationIv.isEnabled = false
            imageAdapter.value?.isRemoveEnabled = false
            videoAdapter.value?.isRemoveEnabled = false
            locationAdapter.value?.isRemoveEnabled = false
            addPostIv.visibility = View.GONE
            addingPostPb.visibility = View.VISIBLE
        });

        companion object : StateController<PostCreationFragment, State>(
            State.IDLE,
            { state: String -> State.valueOf(state) }
        )

    }

}