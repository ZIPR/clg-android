package com.zipr.clg.ui.controller

interface BackPressHandler {
    
    fun handleBackPress(): Boolean
    
}
