package com.zipr.clg.ui.controller

interface SearchQueryHandler {
    
    fun onSearchQuery(query: String)
    
}