package com.zipr.clg.ui.fragment.profile

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.facebook.common.executors.UiThreadImmediateExecutorService
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.orhanobut.logger.Logger
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.User
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.BackPressHandler
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.controller.StateAct
import com.zipr.clg.ui.controller.StateController
import com.zipr.clg.ui.fragment.userlist.followers.FollowersFragment
import com.zipr.clg.ui.fragment.userlist.following.FollowingFragment
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.orZero
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.item_profileinfo_edit.*
import kotlinx.android.synthetic.main.item_profileinfo_edit.view.*
import kotlinx.android.synthetic.main.item_profileinfo_followersbar.*
import kotlinx.android.synthetic.main.item_profileinfo_idle.view.*
import javax.inject.Inject

private const val RC_IMAGE_PICK = 100

@Injectable
class ProfileFragment : ViewLifecycleFragment(), BackPressHandler {
    
    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    
    private lateinit var idleView: View
    private lateinit var editView: View
    
    private val profileViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ProfileViewModel::class.java)
    }
    
    private val editedFullName get() = editView.fullNameTxInET.text.toString()
    private val editedUsername get() = editView.usernameTxInET.text.toString()
    private val editedBio get() = editView.bioTxInET.text.toString()
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        idleView = inflater.inflate(R.layout.item_profileinfo_idle, root.infoContainerFl, false)
        editView = inflater.inflate(R.layout.item_profileinfo_edit, root.infoContainerFl, false)
        
        return root
    }
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    
        State.apply { restoreState(savedInstanceState) }
        
        /* This way edit text is still multi-line and able to trigger IME actions */
        editView.bioTxInET.setRawInputType(InputType.TYPE_CLASS_TEXT)
        editView.editPhotoRiv.colorFilter = PorterDuffColorFilter(
            ClgApplication.getColor(R.color.black_55tp), PorterDuff.Mode.DARKEN
        )
        
        firebaseUser?.uid?.also {
            profileViewModel.setUser(it)
        } ?: showToast(context, getString(R.string.err_unauthorized))
        
        editView.fullNameTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                editView.fullNameTxInET.setText(editedFullName.trim())
                !validateFullName(editedFullName)
            } else {
                false
            }
        }
        editView.usernameTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                editView.usernameTxInET.setText(editedUsername.trim())
                !validateUsername(editedUsername)
            } else {
                false
            }
        }
        editView.bioTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                profileBtn.callOnClick()
            }
            false
        }
        
        editView.cancelEditIB.setOnClickListener {
            profileViewModel.cancelImageUploading()
            profileViewModel.cancelImageProcessing()
            profileViewModel.clearEditData()
            State.apply { setState(State.IDLE) }
        }
        
        editView.uploadPhotoIB.setOnClickListener {
            val intent = Intent().setType("image/*").setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(Intent.createChooser(intent, "Select Image"), RC_IMAGE_PICK)
        }
        
        followersCl.setOnClickListener {
            firebaseUser?.uid?.also {
                navigationController.navigateToFragment(FollowersFragment.newInstance(it))
            } ?: showToast(context, getString(R.string.err_unauthorized))
        }
        followingCl.setOnClickListener {
            firebaseUser?.uid?.also {
                navigationController.navigateToFragment(FollowingFragment.newInstance(it))
            } ?: showToast(context, getString(R.string.err_unauthorized))
        }
        
        profileViewModel.userLD.observe(viewLifecycleOwner, Observer {
            if (State.currentState == State.IDLE) {
                setProfileBtnStatus(it?.data != null)
            }
            
            it?.apply {
                if (data != null) {
                    setProfileInfo(data)
                } else {
                    Logger.e(exception.toString())
                    showToast(context, getString(R.string.err_user_fetch))
                }
            }
        })
        profileViewModel.processResultLD.observe(viewLifecycleOwner, Observer {
            if (State.currentState != State.PHOTO_PROCESS) {
                return@Observer
            }
            
            it?.apply {
                if (error == null) {
                    profileViewModel.processedPhoto = result
                } else {
                    Logger.e(error.toString())
                    showToast(context, getString(R.string.err_photo_process_failed))
                }
                State.apply { setState(State.EDIT) }
            }
        })
        profileViewModel.uploadResultLD.observe(viewLifecycleOwner, Observer {
            if (State.currentState != State.PHOTO_UPLOAD) {
                return@Observer
            }

            it?.apply {
                if (error == null) {
                    disablePhotoUploadCancelling()
                    result?.toString()?.also {
                        setProfilePhoto(it)
                        changeUserInfo(it)
                    }
                } else {
                    if (error.type != DataException.Type.CANCELLED) {
                        Logger.e(error.toString())
                        showToast(context, getString(R.string.err_upload_failed))
                    }
                    State.apply { setState(State.EDIT) }
                }
            }
        })
        profileViewModel.usernameValidationResultLD.observe(viewLifecycleOwner, Observer {
            if (State.currentState != State.USERNAME_VALIDATION) {
                return@Observer
            }
            
            it?.apply {
                when {
                    metadata?.isFromCache == true -> {
                        showToast(context, getString(R.string.err_no_network))
                        State.apply { setState(State.EDIT) }
                    }
                    exception?.type == DataException.Type.NOT_FOUND -> {
                        onUsernameValidationSuccess()
                    }
                    data != null -> {
                        editView.usernameTxInL.error = getString(R.string.err_username_owned)
                        showSoftKeyboard(editView.usernameTxInET)
                        State.apply { setState(State.EDIT) }
                    }
                    else -> {
                        Logger.e(exception.toString())
                        showToast(context, getString(R.string.err_username_availability_check))
                        State.apply { setState(State.EDIT) }
                    }
                }
            }
        })
    }
    
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        
        State.apply { saveState(outState) }
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        
        if (requestCode == RC_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            data?.data?.also { uri ->
                context?.also {
                    profileViewModel.processImage(
                        it,
                        uri,
                        resources.getInteger(R.integer.photo_height_px),
                        resources.getInteger(R.integer.photo_width_px)
                    )
                    
                    State.apply { setState(State.PHOTO_PROCESS) }
                }
            }
        }
    }
    
    override fun handleBackPress() =
        if (State.currentState != State.IDLE) {
            cancelEditIB.callOnClick()
            true
        } else {
            false
        }
    
    override fun onDestroyView() {
        super.onDestroyView()
        
        if (isRemoving) {
            profileViewModel.onCleared()
        }
    }
    
    private fun setProfilePhoto(uri: String) {
        val imageRequest = ImageRequestBuilder
            .newBuilderWithSource(Uri.parse(uri))
            .build()
        
        val dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, null)
        dataSource.subscribe(object : BaseBitmapDataSubscriber() {
            
            override fun onNewResultImpl(bitmap: Bitmap?) {
                profileViewModel.profilePhoto = bitmap
                setEditPhoto()
            }
            
            override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>?) {
                profileViewModel.profilePhoto = null
                setEditPhoto()
            }
            
        }, UiThreadImmediateExecutorService.getInstance())
        
        idleView.photoDv.controller = Fresco.newDraweeControllerBuilder()
            .setOldController(idleView.photoDv.controller)
            .setImageRequest(imageRequest)
            .setTapToRetryEnabled(true)
            .build()
    }
    
    private fun setProfileInfo(user: User) {
        idleView.fullNameTv.text = user.fullName.orEmpty()
        idleView.usernameTv.text = "@${user.username.orEmpty()}"
        idleView.bioTv.text = user.bio.orEmpty()
        followersCountTv.text = user.followersCount.orZero().toString()
        followingCountTv.text = user.followingCount.orZero().toString()
        setProfilePhoto(user.photoUrl.orEmpty())
    }
    
    private fun setEditPhoto() {
        when {
            profileViewModel.processedPhoto != null -> {
                editView.editPhotoRiv.setImageBitmap(profileViewModel.processedPhoto)
            }
            profileViewModel.profilePhoto != null -> {
                editView.editPhotoRiv.setImageBitmap(profileViewModel.profilePhoto)
            }
            else -> editView.editPhotoRiv.setImageResource(R.drawable.com_facebook_profile_picture_blank_square)
        }
    }
    
    private fun setContainerView(view: View) {
        if (view.parent != infoContainerFl) {
            infoContainerFl?.apply {
                removeAllViews()
                addView(view)
            }
        }
    }
    
    private fun setIdleView() {
        setContainerView(idleView)
        profileBtn.apply {
            text = getString(R.string.edit)
            setOnClickListener {
                onEditStart()
                State.apply { setState(State.EDIT) }
            }
        }
    }
    
    private fun setEditView() {
        setContainerView(editView)
        setEditPhoto()
        profileViewModel.processedPhoto?.also {
            editView.editPhotoRiv.setImageBitmap(it)
        }
        profileBtn.apply {
            text = getString(R.string.done)
            setOnClickListener { onEditDone() }
        }
    }
    
    private fun setProfileBtnStatus(isEnabled: Boolean) {
        profileBtn.isEnabled = isEnabled
        profileBtn.alpha = if (isEnabled) 1f else 0.5f
    }
    
    private fun setEditStatus(isEnabled: Boolean) {
        editView.fullNameTxInET.isEnabled = isEnabled
        editView.usernameTxInET.isEnabled = isEnabled
        editView.bioTxInET.isEnabled = isEnabled
        if (!isEnabled) {
            editView.uploadPhotoIB.visibility = View.GONE
            editView.cancelEditIB.visibility = View.GONE
            editView.fullNameTxInL.clearFocus()
            editView.usernameTxInL.clearFocus()
            editView.bioTxInL.clearFocus()
        } else {
            editView.cancelEditIB.visibility = View.VISIBLE
        }
    }
    
    private fun setPhotoUploadingStatus(isUploading: Boolean) {
        if (isUploading) {
            editView.uploadPhotoIB.visibility = View.GONE
            editView.photoUploadPB.visibility = View.VISIBLE
            editView.cancelUploadIB.visibility = View.VISIBLE
        } else {
            editView.uploadPhotoIB.visibility = View.VISIBLE
            editView.photoUploadPB.visibility = View.INVISIBLE
            editView.cancelUploadIB.visibility = View.GONE
        }
    }

    private fun disablePhotoUploadCancelling() {
        editView.cancelUploadIB.visibility = View.GONE
    }

    private fun uploadPhotoIfNeeded(): Boolean =
        profileViewModel.processedPhoto?.let {
            profileViewModel.uploadImage(it)
            State.apply { setState(State.PHOTO_UPLOAD) }
            true
        } ?: false
    
    private fun changeUserInfo(newPhotoUrl: String? = null) {
        val fullName = editedFullName.trim()
        val username = editedUsername.trim()
        val bio = editedBio.trim()
        
        profileViewModel.updateUserInfo(username, fullName, bio, newPhotoUrl)
        idleView.fullNameTv.text = fullName
        idleView.usernameTv.text = "@$username"
        idleView.bioTv.text = bio
        State.apply { setState(State.IDLE) }
    }
    
    private fun validateUsername(username: String) = username.matches(
        Regex(
            getString(
                R.string.pattern_username,
                resources.getInteger(R.integer.username_length_min),
                resources.getInteger(R.integer.username_length_max)
            )
        )
    ).also { editView.usernameTxInL.error = if (!it) getString(R.string.err_username_valid) else "" }
    
    private fun validateFullName(fullName: String) = fullName.matches(
        Regex(
            getString(
                R.string.pattern_full_name,
                resources.getInteger(R.integer.full_name_length_min),
                resources.getInteger(R.integer.full_name_length_max)
            )
        )
    ).also { editView.fullNameTxInL.error = if (!it) getString(R.string.err_full_name_valid) else "" }
    
    private fun validateBio(bio: String) = bio.matches(
        Regex(
            getString(
                R.string.pattern_bio,
                resources.getInteger(R.integer.bio_length_max)
            )
        )
    ).also { editView.bioTxInL.error = if (!it) getString(R.string.err_bio_valid) else "" }
    
    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .apply {
                    toggleSoftInput(
                        InputMethodManager.SHOW_IMPLICIT,
                        InputMethodManager.HIDE_IMPLICIT_ONLY
                    )
                }
        }
    }
    
    private fun onEditStart() {
        profileViewModel.clearEditData()
        editView.usernameTxInET.setText(idleView.usernameTv.text.removePrefix("@"))
        editView.fullNameTxInET.setText(idleView.fullNameTv.text)
        editView.bioTxInET.setText(idleView.bioTv.text)
        editView.usernameTxInL.error = ""
        editView.fullNameTxInL.error = ""
        editView.bioTxInL.error = ""
    }
    
    private fun onEditDone() {
        val user = profileViewModel.getCurrentUser()
        if (user == null) {
            showToast(context, getString(R.string.err_edit_apply))
            State.apply { setState(State.IDLE) }
            return
        }
        
        val fullName = editedFullName.trim()
        val username = editedUsername.trim()
        val bio = editedBio.trim()
        
        editView.fullNameTxInET.setText(fullName)
        editView.usernameTxInET.setText(username)
        editView.bioTxInET.setText(bio)
        
        if (user.username == username &&
            user.fullName == fullName &&
            user.bio == bio &&
            profileViewModel.processedPhoto == null
        ) {
            State.apply { setState(State.IDLE) }
        } else if (validateUsername(username) and
            validateFullName(fullName) and
            validateBio(bio)
        ) {
            if (user.username != username) {
                profileViewModel.checkUsernameAvailability(username)
                State.apply { setState(State.USERNAME_VALIDATION) }
            } else if (!uploadPhotoIfNeeded()) {
                changeUserInfo()
            }
        }
    }
    
    private fun onUsernameValidationSuccess() {
        if (!uploadPhotoIfNeeded()) {
            changeUserInfo()
        }
    }
    
    companion object {
        
        fun newInstance() = ProfileFragment()
        
    }
    
    private enum class State(
        override val state: ProfileFragment.() -> Unit
    ) : StateAct<ProfileFragment> {
        IDLE({
            setIdleView()
            
            setProfileBtnStatus(profileViewModel.getCurrentUser() != null)
        }),
        EDIT({
            setEditView()
            
            setProfileBtnStatus(true)
            setPhotoUploadingStatus(false)
            setEditStatus(true)
        }),
        PHOTO_PROCESS({
            setEditView()
            
            setProfileBtnStatus(false)
            setPhotoUploadingStatus(true)
            setEditStatus(true)
            editView.cancelUploadIB.setOnClickListener {
                profileViewModel.cancelImageProcessing()
                setState(State.EDIT)
            }
        }),
        PHOTO_UPLOAD({
            setEditView()
            
            setProfileBtnStatus(false)
            setPhotoUploadingStatus(true)
            setEditStatus(false)
            editView.cancelUploadIB.setOnClickListener {
                profileViewModel.cancelImageUploading()
                setState(State.EDIT)
            }
        }),
        USERNAME_VALIDATION({
            setEditView()
            
            setProfileBtnStatus(false)
            setPhotoUploadingStatus(false)
            setEditStatus(false)
        });
        
        companion object : StateController<ProfileFragment, State>(
            State.IDLE,
            { state: String -> State.valueOf(state) }
        )
        
    }
    
}
