package com.zipr.clg.ui.fragment.userlist

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.fragment_userlist.*
import javax.inject.Inject

@Injectable
abstract class UserListFragment<VM : UserListViewModel<*>>: ViewLifecycleFragment() {

    @Inject
    lateinit var navigationController: NavigationController

    protected val lifecycleRegistrar by lazy { LifecycleRegistrar(viewLifecycleOwner) }

    abstract val userListViewModel: VM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_userlist, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        usersSrl.apply {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener(::onRefresh)
        }

        retryBtn.setOnClickListener(::onRetry)

        val usersAdapter = UsersAdapter(
            userListViewModel.dataProvider,
            lifecycleRegistrar,
            {
                it.user?.id?.also(navigationController::navigateToUserProfile)
            },
            {
                it.user?.id?.also {
                    val curUser = firebaseUser?.uid
                    if (curUser != null) {
                        userListViewModel.follow(it, curUser)
                    } else {
                        showToast(context, getString(R.string.err_unauthorized))
                    }
                }
            },
            {
                it.user?.id?.also {
                    val curUser = firebaseUser?.uid
                    if (curUser != null) {
                        userListViewModel.unfollow(it, curUser)
                    } else {
                        showToast(context, getString(R.string.err_unauthorized))
                    }
                }
            }
        )

        usersRv.apply {
            adapter = usersAdapter.withPaginationStatus(16, 16)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }

        userListViewModel.dataProvider.apply {
            lifecycleRegistrar.registerImmediately(
                object : DataProvider.StatusListener {

                    override fun onStatusChange(
                        isFetching: Boolean,
                        isRefreshing: Boolean,
                        isFullResult: Boolean,
                        isEmpty: Boolean,
                        isError: Boolean
                    ) {
                        when {
                            !isError -> if (isEmpty && isFetching) setLoadingState() else setResultState()

                            isEmpty -> setErrorState()

                            else -> {
                                if (usersSrl.isRefreshing) {
                                    showToast(context, getString(R.string.err_refresh))
                                }
                                setResultState()
                            }
                        }
                        usersSrl.isRefreshing = isRefreshing
                    }

                },
                ::registerStatusListener,
                ::unregisterStatusListener
            ).registerImmediately(
                object : DataProvider.ErrorListener {

                    override fun onFetchError(exception: DataException) {
                        errorTv.text = getErrorMessage(exception)
                    }

                },
                ::registerErrorListener,
                ::unregisterErrorListener
            )
        }
    }

    private fun setLoadingState() {
        usersRv.visibility = View.GONE
        errorTv.visibility = View.GONE
        retryBtn.visibility = View.GONE
        dataLoadPb.visibility = View.VISIBLE
        usersSrl.isEnabled = false
    }

    private fun setResultState() {
        usersRv.visibility = View.VISIBLE
        errorTv.visibility = View.GONE
        retryBtn.visibility = View.GONE
        dataLoadPb.visibility = View.GONE
        usersSrl.isEnabled = true
    }

    private fun setErrorState() {
        usersRv.visibility = View.GONE
        errorTv.visibility = View.VISIBLE
        retryBtn.visibility = View.VISIBLE
        dataLoadPb.visibility = View.GONE
        usersSrl.isEnabled = false
    }

    @CallSuper
    protected open fun onRetry(view: View) {
        userListViewModel.dataProvider.retryFetching()
    }

    @CallSuper
    protected open fun onRefresh() {
        userListViewModel.dataProvider.refreshData()
    }

    abstract fun getErrorMessage(error: DataException): String

}