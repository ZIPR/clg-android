package com.zipr.clg.ui.common

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent

class LifecycleRegistrar(lifecycleOwner: LifecycleOwner) {

    private val entries = mutableListOf<RegisterEntry<*>>()

    init {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun unregisterEntries() {
                entries.apply {
                    forEach { it.unregister() }
                    clear()
                }
            }

        })
    }

    fun <V> registerImmediately(value: V, onRegister: (V) -> Unit, onUnregister: (V) -> Unit) = apply {
        entries.add(
            RegisterEntry(value, onRegister, onUnregister).apply { register() }
        )
    }

    private class RegisterEntry<V>(value: V, onRegister: (V) -> Unit, onUnregister: (V) -> Unit) {

        val register = {
            onRegister(value)
        }

        val unregister = {
            onUnregister(value)
        }

    }

}