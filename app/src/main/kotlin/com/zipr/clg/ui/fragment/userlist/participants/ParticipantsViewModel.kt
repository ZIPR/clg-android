package com.zipr.clg.ui.fragment.userlist.participants

import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.interactor.ChallengeInteractor
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.fragment.userlist.UserListViewModel
import javax.inject.Inject

class ParticipantsViewModel @Inject constructor(
    private val challengeInteractor: ChallengeInteractor,
    userInteractor: UserInteractor
): UserListViewModel<DataProvider<UserFollower>>(userInteractor) {
    
    private lateinit var userId: String
    private lateinit var challengeId: String
    
    fun setUser(userId: String) {
        this.userId = userId
    }
    
    fun setChallenge(challengeId: String) {
        this.challengeId = challengeId
    }
    
    override fun getDataProvider(fetchLimit: Long): DataProvider<UserFollower> =
        challengeInteractor.getParticipants(fetchLimit, challengeId, userId, false, true)
}