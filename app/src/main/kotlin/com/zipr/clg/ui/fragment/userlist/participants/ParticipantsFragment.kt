package com.zipr.clg.ui.fragment.userlist.participants

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.fragment.userlist.UserListFragment
import com.zipr.clg.util.firebaseUser
import kotlinx.android.synthetic.main.fragment_participants.*
import javax.inject.Inject

private const val ARG_CHALLENGE_ID = "ARG_CHALLENGE_ID"

@Injectable
class ParticipantsFragment : UserListFragment<ParticipantsViewModel>() {
    
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    
    override val userListViewModel: ParticipantsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ParticipantsViewModel::class.java)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_participants, container,false)
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        userListViewModel.setChallenge(arguments!!.getString(ARG_CHALLENGE_ID))
        userListViewModel.setUser(firebaseUser!!.uid)
        super.onActivityCreated(savedInstanceState)
        
        toolbar.title = getString(R.string.participants)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
    }
    
    override fun getErrorMessage(error: DataException): String =
        when(error.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_no_participants)
            
            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)
            
            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)
            
            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)
            
            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)
            
            else -> error.message ?: getString(R.string.err_unexpected)
        }
    
    companion object {
    
        fun newInstance(challengeId: String) = ParticipantsFragment().apply {
            arguments = Bundle().apply { putString(ARG_CHALLENGE_ID, challengeId) }
        }
        
    }
    
}