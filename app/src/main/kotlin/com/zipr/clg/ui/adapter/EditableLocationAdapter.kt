package com.zipr.clg.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.zipr.clg.R
import com.zipr.clg.data.entity.Location
import com.zipr.clg.util.runOrPostIfComputingLayout
import kotlinx.android.synthetic.main.item_location_editable.view.*
import kotlin.properties.Delegates

class EditableLocationAdapter(
    location: Location?,
    private val onClickLocation: (location: Location) -> Unit
) : SingleItemAdapter<EditableLocationAdapter.LocationViewHolder>(R.layout.item_location_editable) {

    var location by Delegates.observable(location) { _, _, new ->
        isVisible = new != null
    }

    var isRemoveEnabled by Delegates.observable(true) { _, _, _ ->
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRangeChanged(0, dataItemCount)
        }
    }

    init {
        isVisible = location != null
    }

    override fun onCreateViewHolder(view: View) = LocationViewHolder(view).apply {
        locationTv.setOnClickListener { onClickLocation(location) }
        removeLocationIv.setOnClickListener {
            this@EditableLocationAdapter.location = null
        }
    }

    override fun onBindViewHolder(holder: LocationViewHolder) {
        holder.also {
            it.bind(location!!)
            it.locationTv.isEnabled = isRemoveEnabled
            it.removeLocationIv.isEnabled = isRemoveEnabled
            it.removeLocationIv.visibility = if (isRemoveEnabled) View.VISIBLE else View.INVISIBLE
        }
    }

    class LocationViewHolder(viewGroup: View) : RecyclerView.ViewHolder(viewGroup) {

        val locationTv = itemView.locationTv as TextView
        val removeLocationIv = itemView.removeLocationIv as ImageView

        lateinit var location: Location
            private set

        fun bind(location: Location) {
            this.location = location
            locationTv.text = "${location.name}\n${location.address}"
        }

    }

}