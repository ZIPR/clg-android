package com.zipr.clg.ui.common

import android.net.Uri

data class SourceVideo(val uri: Uri, val thumbnailLD: SourceImageLD)
