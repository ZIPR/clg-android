package com.zipr.clg.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText

typealias OnSelectionChangedListener = (selStart: Int, selEnd: Int) -> Unit

class SelectableEditText : EditText {
    
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    
    private val onSelectionChangedListeners = mutableListOf<OnSelectionChangedListener>()
    
    fun addOnSelectionChangedListener(listener: OnSelectionChangedListener) {
        onSelectionChangedListeners.add(listener)
    }
    
    fun removeOnSelectionChangedListener(listener: OnSelectionChangedListener) {
        onSelectionChangedListeners.remove(listener)
    }


    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        // method is called by super constructor before property initialization
        @Suppress("UNNECESSARY_SAFE_CALL")
        onSelectionChangedListeners?.forEach { it(selStart, selEnd) }
    }
    
}
