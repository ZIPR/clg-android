package com.zipr.clg.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import com.zipr.clg.R
import com.zipr.clg.data.entity.Image

class ImageAdapter : RecyclerViewAdapter<ImageAdapter.PictureViewHolder>() {

    private var images: List<Image>? = null

    fun set(images: List<Image>?) {
        this.images = images
        dataItemCount = images?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PictureViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
    )

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        holder.bind(images!![position])
    }

    class PictureViewHolder(simpleDraweeView: View) : RecyclerView.ViewHolder(simpleDraweeView) {

        val draweeView = simpleDraweeView as SimpleDraweeView

        fun bind(image: Image) {
            if (image.aspectRatio != null && image.aspectRatio > 0) {
                draweeView.aspectRatio = image.aspectRatio.toFloat()
            }
            draweeView.setImageURI(image.url)
        }

    }

}
