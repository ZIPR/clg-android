package com.zipr.clg.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.zipr.clg.util.runOrPostIfComputingLayout

data class Segment(
    val dataAdapter: RecyclerViewAdapter<*>,
    val typeOffset: Int,
    val typesCount: Int,
    var itemCount: Int,
    var positionOffset: Int
)

/**
 * @param dataAdapters Adapters that will represent segments. Note that adapter view types should be
 *                     sequential numbers that starts from 0.
 */
open class SegmentAdapter(
    vararg dataAdapters: RecyclerViewAdapter<*>
) : RecyclerViewAdapter<RecyclerView.ViewHolder>() {
    
    private val segments = mutableListOf<Segment>()
    private var totalItemCount = 0
    private var totalTypesCount = 0
    
    override val itemTypesCount: Int = dataAdapters.size
    
    init {
        dataAdapters.forEach { dataAdapter ->
            val segmentInfo = Segment(
                dataAdapter = dataAdapter,
                typeOffset = totalTypesCount,
                typesCount = dataAdapter.itemTypesCount,
                itemCount = dataAdapter.itemCount,
                positionOffset = totalItemCount
            )
            
            dataAdapter.registerAdapterDataObserver(
                getSegmentAdapterDataObserver(
                    segment = segmentInfo,
                    segmentIndex = segments.size
                )
            )
            
            segments.add(segmentInfo)
            totalTypesCount += segmentInfo.typesCount
            totalItemCount += segmentInfo.itemCount
        }
    }
    
    final override fun getItemCount(): Int = totalItemCount
    
    final override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder = getSegmentWithType(viewType).run {
        dataAdapter.onCreateViewHolder(parent, viewType - typeOffset) 
    }
    
    final override fun getItemViewType(position: Int): Int = getSegmentAt(position).run {
        dataAdapter.getItemViewType(position - positionOffset) + typeOffset
    }
    
    @Suppress("UNCHECKED_CAST")
    final override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getSegmentAt(position).run {
            (dataAdapter as RecyclerView.Adapter<RecyclerView.ViewHolder>)
                .onBindViewHolder(holder, position - positionOffset)
        }
    }
    
    private fun getSegmentWithType(viewType: Int): Segment {
        segments.forEach { segment ->  
            if (viewType < segment.typeOffset + segment.typesCount) {
                return segment
            }
        }
    
        throw IllegalArgumentException("No data adapter for type $viewType")
    }
    
    private fun getSegmentAt(position: Int): Segment {
        segments.forEach { segment ->
            if (position < segment.positionOffset + segment.itemCount) {
                return segment
            }
        }
    
        throw IllegalArgumentException("No data adapter for position $position")
    }
    
    private fun getSegmentAdapterDataObserver(segment: Segment, segmentIndex: Int) =
        object : RecyclerView.AdapterDataObserver() {
            
            val nextSegmentIndex = segmentIndex + 1
            
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                if (itemCount <= 0) return
                
                val recyclerPositionStart = segment.positionOffset + positionStart
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemRangeChanged(recyclerPositionStart, itemCount)
                }
            }
            
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
                if (itemCount <= 0) return
                
                val recyclerPositionStart = segment.positionOffset + positionStart
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemRangeChanged(recyclerPositionStart, itemCount, payload)
                }
            }
            
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (itemCount <= 0) return
                
                totalItemCount += itemCount
                segment.itemCount += itemCount
                for (i in nextSegmentIndex..segments.lastIndex) {
                    segments[i].positionOffset += itemCount
                }
                
                val recyclerPositionStart = segment.positionOffset + positionStart
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemRangeInserted(recyclerPositionStart, itemCount)
                    if (recyclerPositionStart == 0) {
                        recyclerView?.scrollToPosition(0)
                    }
                }
            }
            
            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                if (itemCount <= 0) return
                
                totalItemCount -= itemCount
                segment.itemCount -= itemCount
                for (i in nextSegmentIndex..segments.lastIndex) {
                    segments[i].positionOffset -= itemCount
                }
                
                val recyclerPositionStart = segment.positionOffset + positionStart
                recyclerView.runOrPostIfComputingLayout {
                    notifyItemRangeRemoved(recyclerPositionStart, itemCount)
                }
            }
            
            override fun onChanged() {
                throw UnsupportedOperationException(
                    "Unsupported operation for segment adapter, use the more specific change events"
                )
            }
            
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                throw UnsupportedOperationException(
                    "Unsupported operation for segment adapter, use the more specific change events"
                )
            }
            
        }
    
}