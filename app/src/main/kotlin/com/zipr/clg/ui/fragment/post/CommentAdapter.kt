package com.zipr.clg.ui.fragment.post

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.data.entity.Comment
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.PagedAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import kotlinx.android.synthetic.main.item_comment.view.*

class CommentAdapter(
    commentsProvider: DataProvider<Comment>,
    lifecycleRegistrar: LifecycleRegistrar,
    private val onAuthorClick: (Comment) -> Unit
) : PagedAdapter<Comment, CommentAdapter.CommentViewHolder>(commentsProvider, lifecycleRegistrar) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CommentViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
    ).apply {
        authorAreaView.setOnClickListener { comment.also(onAuthorClick) }
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) =
        holder.bindComment(getItem(position))

    class CommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val authorAreaView = itemView.authorAreaView as View
        val authorPhotoDv = itemView.authorPhotoDv as SimpleDraweeView
        val authorUsernameTv = itemView.authorUsernameTv as TextView
        val creationDateTv = itemView.creationDateTv as TextView
        val contentTextTv = itemView.contentTextTv as TextView

        lateinit var comment: Comment

        fun bindComment(comment: Comment) {
            this.comment = comment
            authorPhotoDv.setImageURI(comment.author?.photoUrl)
            authorUsernameTv.text = comment.author?.username.orEmpty()
            creationDateTv.text = comment.creationDate?.let {
                "${ClgApplication.dateFormat.format(it)} ${ClgApplication.timeFormat.format(it)}"
            }
            contentTextTv.text = comment.contentText
        }

    }

}