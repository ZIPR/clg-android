package com.zipr.clg.ui.fragment.postcreation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.AsyncResult
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.interactor.PostInteractor
import com.zipr.clg.ui.common.SourceImage
import com.zipr.clg.ui.common.SourceVideo
import com.zipr.clg.util.SingleEventObserver
import com.zipr.clg.util.observeSingleEvent
import javax.inject.Inject

class PostCreationViewModel @Inject constructor(
    private val postInteractor: PostInteractor
) : ViewModel() {
    
    private var creatingPostObserver: SingleEventObserver<*, *>? = null
    
    private val _creatingPostLD = MutableLiveData<AsyncResult<String, Throwable>>()
    val creatingPostLD = _creatingPostLD as LiveData<AsyncResult<String, Throwable>>
    
    fun createPost(
        challengeId: String,
        text: String,
        sourceImageLDList: List<LiveData<AsyncResult<SourceImage, Throwable>>>,
        sourceVideoList: List<SourceVideo>,
        location: Location?
    ) {
        cancel()

        creatingPostObserver = postInteractor.create(
            challengeId,
            text,
            sourceImageLDList,
            sourceVideoList,
            location
        ).observeSingleEvent {
            _creatingPostLD.value = it
            creatingPostObserver = null
        }
    }
    
    fun cancel() {
        creatingPostObserver?.cancel()
        creatingPostObserver = null
        _creatingPostLD.value = null
    }
    
}