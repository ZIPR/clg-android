package com.zipr.clg.ui.fragment.auth

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.zipr.clg.util.firebaseAuth
import javax.inject.Inject

class AuthViewModel @Inject constructor() : ViewModel() {

    private val _authLiveData = MutableLiveData<Task<AuthResult>>()
    val authLiveData get() = _authLiveData as LiveData<Task<AuthResult>>

    fun resetAuth() {
        _authLiveData.value = null
    }

    fun signInWithEmailAndPassword(email: String, password: String) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(_authLiveData::setValue)
    }

    fun signInWithCredential(credential: AuthCredential) {
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(_authLiveData::setValue)
    }

}