package com.zipr.clg.ui.fragment.post

import android.net.Uri
import com.zipr.clg.R
import com.zipr.clg.data.entity.Comment
import com.zipr.clg.data.entity.Location
import com.zipr.clg.data.entity.Post
import com.zipr.clg.data.entity.VotedPost
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.adapter.LayoutAdapter
import com.zipr.clg.ui.adapter.SegmentAdapter
import com.zipr.clg.ui.adapter.post.PostAdapter
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.widget.ThumbnailPlayerView

class PostSegmentAdapter private constructor(
    private val postAdapter: PostAdapter,
    commentAdapter: CommentAdapter,
    private val commentsNoContentLayoutAdapter: LayoutAdapter
): SegmentAdapter(
    postAdapter,
    commentAdapter.withPaginationStatus(16, 16),
    commentsNoContentLayoutAdapter
) {

    fun setPost(post: VotedPost) {
        postAdapter.votedPost = post
    }

    fun setCommentsStatus(isCommentsListEmpty: Boolean) {
        commentsNoContentLayoutAdapter.isVisible = isCommentsListEmpty
    }

    companion object {

        fun create(
            lifecycleRegistrar: LifecycleRegistrar,
            commentProvider: DataProvider<Comment>,
            onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
            onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit,
            onPostAuthorClick: (post: Post) -> Unit,
            onCommentAuthorClick: (comment: Comment) -> Unit,
            onPostLocationClick: (location: Location) -> Unit,
            onPostRatingUp: (post: Post) -> Unit,
            onPostRatingDown: (post: Post) -> Unit,
            onPostCommentsClick: (post: Post) -> Unit
        ) = PostSegmentAdapter(
            PostAdapter(
                onBindPlayerViewTag,
                onClickPlayerView,
                onPostAuthorClick,
                onPostLocationClick,
                onPostRatingUp,
                onPostRatingDown,
                onPostCommentsClick
            ),
            CommentAdapter(
                commentProvider,
                lifecycleRegistrar,
                onCommentAuthorClick
            ),
            LayoutAdapter(R.layout.item_post_comments_no_content)
        )

    }

}