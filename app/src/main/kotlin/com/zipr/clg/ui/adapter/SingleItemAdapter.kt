package com.zipr.clg.ui.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class SingleItemAdapter<VH : RecyclerView.ViewHolder>(
    @param:LayoutRes private val itemLayout: Int
) : RecyclerViewAdapter<VH>() {
    
    var isVisible: Boolean
        get() = dataItemCount == 1
        set(value) {
            dataItemCount = if (value) 1 else 0
        }
    
    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        onCreateViewHolder(
            LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)
        )
    
    final override fun onBindViewHolder(holder: VH, position: Int) = onBindViewHolder(holder)
    
    abstract fun onCreateViewHolder(view: View): VH
    
    abstract fun onBindViewHolder(holder: VH)
    
}