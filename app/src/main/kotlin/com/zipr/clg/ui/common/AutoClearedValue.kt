package com.zipr.clg.ui.common

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent

class AutoClearedValue<T>(lifecycleOwner: LifecycleOwner, var value: T? = null) {
    
    init {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun clearValue() {
                value = null
            }
            
        })
    }
    
}