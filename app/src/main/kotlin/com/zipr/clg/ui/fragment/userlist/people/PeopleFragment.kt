package com.zipr.clg.ui.fragment.userlist.people

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.fragment.userlist.SearchableUserListFragment
import com.zipr.clg.util.firebaseUser
import kotlinx.android.synthetic.main.fragment_searchable_userlist.*
import javax.inject.Inject

@Injectable
class PeopleFragment : SearchableUserListFragment<PeopleViewModel>() {
    
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    
    override val userListViewModel: PeopleViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PeopleViewModel::class.java)
    }
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        userListViewModel.setUser(firebaseUser!!.uid)
        super.onActivityCreated(savedInstanceState)
        
        searchView.queryHint = getString(R.string.search_users)
    }
    
    override fun getErrorMessage(error: DataException): String =
        when(error.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_search_no_users)
            
            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)
            
            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)
            
            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)
            
            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)
            
            else -> error.message ?: getString(R.string.err_unexpected)
        }
    
    companion object {
        
        fun newInstance() = PeopleFragment()
        
    }
    
}