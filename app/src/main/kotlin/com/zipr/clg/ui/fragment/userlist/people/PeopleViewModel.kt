package com.zipr.clg.ui.fragment.userlist.people

import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.data.paging.provider.QueryDataProvider
import com.zipr.clg.ui.fragment.userlist.UserListViewModel
import javax.inject.Inject

class PeopleViewModel @Inject constructor(
    private val userInteractor: UserInteractor
) : UserListViewModel<QueryDataProvider<UserFollower>>(userInteractor) {
    
    private lateinit var userId: String
    
    fun setUser(userId: String) {
        this.userId = userId
    }
    
    override fun getDataProvider(fetchLimit: Long): QueryDataProvider<UserFollower> =
        userInteractor.getUsers(fetchLimit, userId, true).also {
            it.query = ""
        }
    
}
