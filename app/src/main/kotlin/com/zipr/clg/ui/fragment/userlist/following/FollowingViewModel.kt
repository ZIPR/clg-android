package com.zipr.clg.ui.fragment.userlist.following

import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.ui.fragment.userlist.UserListViewModel
import javax.inject.Inject

class FollowingViewModel @Inject constructor(
    private val userInteractor: UserInteractor
) : UserListViewModel<DataProvider<UserFollower>>(userInteractor) {
    
    private lateinit var userId: String
    private lateinit var followerId: String
    
    fun setUser(userId: String, followerId: String) {
        this.userId = userId
        this.followerId = followerId
    }
    
    override fun getDataProvider(fetchLimit: Long): DataProvider<UserFollower> =
        userInteractor.getFollowing(userId, followerId, fetchLimit, true, true)
    
}