package com.zipr.clg.ui.fragment.challengecreation

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.TextView
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.ui.adapter.RecyclerViewAdapter
import com.zipr.clg.ui.common.SelectionInfo
import com.zipr.clg.ui.widget.SelectableEditText
import com.zipr.clg.util.overrideColor
import com.zipr.clg.util.runOrPostIfComputingLayout
import com.zipr.clg.util.set
import com.zipr.clg.util.showValidationToast
import kotlinx.android.synthetic.main.item_tag_editable.view.*
import kotlin.properties.Delegates

private const val TYPE_TAG_ADDED = 0
private const val TYPE_TAG_BUILDING = 1

class TagAdapter(
    tags: List<String>,
    buildingTag: String,
    buildingTagSelectionInfo: SelectionInfo,
    private val onTagsChanged: (List<String>) -> Unit
) : RecyclerViewAdapter<TagAdapter.TagViewHolder>() {

    private val _tags = MutableList(tags.size) { tags[it] }
    val tags get() = _tags as List<String>
    
    private val _buildingTag = StringBuilder(buildingTag)
    val buildingTag get() = _buildingTag.toString()
    
    private val _buildingTagSelectionInfo = buildingTagSelectionInfo.copy()
    val buildingTagSelectionInfo get() = _buildingTagSelectionInfo.copy()

    var isEditable by Delegates.observable(true) { _, _, _ ->
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRangeChanged(0, itemCount)
        }
    }

    var tagsColor by Delegates.observable(Color.BLACK) { _, _, _ ->
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRangeChanged(0, itemCount)
        }
    }

    private val maxTagLength by lazy { ClgApplication.getInteger(R.integer.challenge_tag_length_max) }
    private val maxTagCount by lazy { ClgApplication.getInteger(R.integer.challenge_tag_count_max) }
    private val tagExistsError by lazy { ClgApplication.getString(R.string.err_tag_exists) }
    private val tagCountError by lazy { ClgApplication.getString(R.string.err_tag_count, maxTagCount) }

    init {
        onTagsChanged(tags)
    }

    private fun addTag(tag: String) {
        _tags.add(tag)
        _buildingTag.setLength(0)
        _buildingTagSelectionInfo.apply {
            selectionStart = 0
            selectionEnd = 0
        }
        val index = _tags.size
        recyclerView.runOrPostIfComputingLayout {
            notifyItemChanged(index-1)
            notifyItemInserted(index)
            onTagsChanged(_tags)
            recyclerView?.scrollToPosition(index)
        }
    }

    private fun removeTag(index: Int) {
        _tags.removeAt(index)
        recyclerView.runOrPostIfComputingLayout {
            notifyItemRemoved(index)
            onTagsChanged(_tags)
        }
    }
    
    override fun getItemCount() = tags.size + 1
    
    override fun getItemViewType(position: Int) =
        if (position <= tags.lastIndex) TYPE_TAG_ADDED else TYPE_TAG_BUILDING
    
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        if (_buildingTagSelectionInfo.hasFocus) {
            recyclerView.apply {
                post {
                    scrollToPosition(itemCount - 1)
                }
            }
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TagViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_tag_editable, parent, false)
    ).apply {
        editActionTv.setOnClickListener {
            when (getItemViewType(adapterPosition)) {
                TYPE_TAG_ADDED -> {
                    adapterPosition.also {
                        if (it != RecyclerView.NO_POSITION) {
                            removeTag(it)
                        }
                    }
                }

                TYPE_TAG_BUILDING -> {
                    tagEt.text.toString().trim().also tag@{ tag ->
                        if (tag.isBlank()) {
                            return@tag
                        }
                        if (_tags.size >= maxTagCount) {
                            showValidationToast(it.context, tagCountError)
                            return@tag
                        }
                        if (_tags.any { tag.contentEquals(it) }) {
                            showValidationToast(it.context, tagExistsError)
                            return@tag
                        }
                        addTag(tag)
                    }
                }
            }
        }
        tagEt.also {
            it.filters = arrayOf(
                InputFilter { source, _, _, _, _, _ -> source.toString().toLowerCase() },
                InputFilter.LengthFilter(maxTagLength)
            )
            it.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) editActionTv.callOnClick()
                else false
            }
            it.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable?) {
                    if (getItemViewType(adapterPosition) == TYPE_TAG_BUILDING) {
                        _buildingTag.set(s)
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            })
            it.addOnSelectionChangedListener { selStart: Int, selEnd: Int ->
                if (getItemViewType(adapterPosition) == TYPE_TAG_BUILDING) {
                    _buildingTagSelectionInfo.apply {
                        selectionStart = selStart
                        selectionEnd = selEnd
                    }
                }
            }
            it.setOnFocusChangeListener { _, hasFocus ->
                adapterPosition.also {
                    if (getItemViewType(it) == TYPE_TAG_BUILDING) {
                        _buildingTagSelectionInfo.hasFocus = hasFocus
                        if (hasFocus) recyclerView?.scrollToPosition(it)
                    }
                }
            }
        }
    }
    
    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        holder.apply {
            when (holder.itemViewType) {
                TYPE_TAG_ADDED -> {
                    bind(tags[position])
                    editActionTv.text = "x"
                    tagEt.also {
                        it.isEnabled = false
                        it.background.overrideColor(
                            ClgApplication.getColor(R.color.grey_200)
                        )
                        it.setHintTextColor(
                            ClgApplication.getColor(R.color.grey_400)
                        )
                    }
                    layout.background.overrideColor(
                        ClgApplication.getColor(R.color.grey_400)
                    )
                }

                TYPE_TAG_BUILDING -> {
                    bind(buildingTag, _buildingTagSelectionInfo)
                    editActionTv.text = "+"
                    tagEt.also {
                        it.isEnabled = isEditable
                        it.background.overrideColor(
                            ClgApplication.getColor(R.color.grey_400)
                        )
                        it.setHintTextColor(
                            ClgApplication.getColor(R.color.grey_200)
                        )
                    }
                    layout.background.overrideColor(
                        ClgApplication.getColor(R.color.grey_200)
                    )
                    itemView.visibility = if (isEditable) View.VISIBLE else View.GONE
                }
            }
            if (isEditable) {
                editActionTv.also {
                    it.isEnabled = true
                    it.visibility = View.VISIBLE
                }
            } else {
                editActionTv.also {
                    it.isEnabled = false
                    it.visibility = View.GONE
                }
            }
            tagsColor.also {
                tagEt.setTextColor(it)
                editActionTv.setTextColor(it)
            }
        }
    }
    
    class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val layout = itemView as LinearLayout
        val tagEt = itemView.tagEt as SelectableEditText
        val editActionTv = itemView.editActionTv as TextView

        fun bind(tag: String) {
            tagEt.setText(tag)
        }

        fun bind(tag: String, selectionInfo: SelectionInfo) {
            selectionInfo.copy().apply {
                tagEt.also {
                    it.setText(tag)
                    it.setSelection(selectionStart, selectionEnd)
                    if (hasFocus) it.requestFocus()
                }
            }
        }
        
    }
    
}