package com.zipr.clg.ui.fragment.reg

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.orhanobut.logger.Logger
import com.zipr.clg.R
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.controller.StateAct
import com.zipr.clg.ui.controller.StateController
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.showToast
import kotlinx.android.synthetic.main.fragment_reg.*
import javax.inject.Inject

@Injectable
class RegFragment : ViewLifecycleFragment() {

    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val regViewModel by lazy { viewModelProvider.get(RegViewModel::class.java) }

    private var fullNameError: String
        get() = fullNameTxInL.error.toString()
        set(value) {
            fullNameTxInL.error = value
        }

    private var usernameError: String
        get() = usernameTxInL.error.toString()
        set(value) {
            usernameTxInL.error = value
        }

    private var emailError: String
        get() = emailTxInL.error.toString()
        set(value) {
            emailTxInL.error = value
        }

    private var passwordError: String
        get() = passwordTxInL.error.toString()
        set(value) {
            passwordTxInL.error = value
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_reg, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    
        State.apply { restoreState(savedInstanceState) }
    
        signUpBtn.setOnClickListener {
            signUp()
        }
        signInTV.setOnClickListener {
            activity!!.onBackPressed()
        }
        fullNameTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                !validateFullName(fullNameTxInET.text.toString().trim())
            } else {
                false
            }
        }
        usernameTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                !validateUsername(usernameTxInET.text.toString().trim())
            } else {
                false
            }
        }
        emailTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                !validateEmail(emailTxInET.text.toString().trim())
            } else {
                false
            }
        }
        passwordTxInET.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signUp()
            }
            false
        }

        val regObserver = Observer<Task<*>> {
            it?.also {
                if (it.isSuccessful) {
                    State.apply { setState(State.REGISTERED) }
                } else {
                    State.apply { setState(State.IDLE) }
                    onRegError(it.exception)
                }
            }
        }
        regViewModel.regLiveData.observe(viewLifecycleOwner, regObserver)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }
    }

    private fun signUp() {
        val fullName = fullNameTxInET.text.toString().trim()
        val username = usernameTxInET.text.toString().trim()
        val email = emailTxInET.text.toString().trim()
        val password = passwordTxInET.text.toString().trim()

        if (validateFullName(fullName)
            and validateUsername(username)
            and validateEmail(email)
            and validatePassword(password)
        ) {
            State.apply { setState(State.REG) }
            regViewModel.createUser(fullName, username, email, password)
        }
    }

    private fun onRegError(e: Exception?) {
        Logger.e(e, "reg error")
        when(e) {
            is FirebaseAuthInvalidCredentialsException -> {
                emailError = getString(R.string.err_email_valid)
            }
            is FirebaseAuthUserCollisionException -> {
                emailError = getString(R.string.err_email_in_use)
            }
            is FirebaseAuthWeakPasswordException -> {
                passwordError = getString(R.string.err_weak_password)
            }
            is FirebaseNetworkException -> {
                showToast(context!!, getString(R.string.err_no_network))
            }
            is FirebaseTooManyRequestsException -> {
                showToast(context!!, getString(R.string.err_many_requests))
            }
            else -> {
                showToast(
                    context!!,
                    getString(R.string.err_reg_failed, e?.message)
                )
            }
        }
    }
    
    private fun validateFullName(fullName: String) = fullName.matches(
        Regex(
            getString(
                R.string.pattern_full_name,
                resources.getInteger(R.integer.full_name_length_min),
                resources.getInteger(R.integer.full_name_length_max)
            )
        )
    ).also { fullNameError = if (!it) getString(R.string.err_full_name_valid) else "" }
    
    private fun validateUsername(username: String) = username.matches(
        Regex(
            getString(
                R.string.pattern_username,
                resources.getInteger(R.integer.username_length_min),
                resources.getInteger(R.integer.username_length_max)
            )
        )
    ).also { usernameError = if (!it) getString(R.string.err_username_valid) else "" }
    
    private fun validateEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()
        .also { emailError = if (!it) getString(R.string.err_email_valid) else "" }
    
    private fun validatePassword(password: String) = password.matches(
        Regex(
            getString(
                R.string.pattern_password,
                resources.getInteger(R.integer.password_length_min),
                resources.getInteger(R.integer.password_length_max)
            )
        )
    ).also { passwordError = if (!it) getString(R.string.err_password_valid) else "" }

    private fun setRegEnabled(isEnabled: Boolean) {
        signUpBtn.isEnabled = isEnabled
        fullNameTxInET.isEnabled = isEnabled
        usernameTxInET.isEnabled = isEnabled
        emailTxInET.isEnabled = isEnabled
        passwordTxInET.isEnabled = isEnabled
        fullNameTxInL.clearFocus()
        usernameTxInL.clearFocus()
        emailTxInL.clearFocus()
        passwordTxInL.clearFocus()
    }

    companion object {
        fun newInstance() = RegFragment()
    }

    private enum class State(override val state: RegFragment.() -> Unit) : StateAct<RegFragment> {
        START({
            setState(if (firebaseUser != null) State.REGISTERED else State.IDLE)
        }),
        IDLE({
            regViewModel.resetReg()
            setRegEnabled(true)
        }),
        REG({
            setRegEnabled(false)
        }),
        REGISTERED({
            setRegEnabled(false)
            navigationController.navigateToMain()
        });

        companion object : StateController<RegFragment, State>(
            START,
            { state: String -> State.valueOf(state) }
        )
        
    }

}