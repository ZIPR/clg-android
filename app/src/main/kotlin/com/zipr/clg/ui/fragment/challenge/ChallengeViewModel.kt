package com.zipr.clg.ui.fragment.challenge

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.entity.ChallengeParticipating
import com.zipr.clg.data.entity.Document
import com.zipr.clg.data.entity.VotedPost
import com.zipr.clg.data.interactor.ChallengeInteractor
import com.zipr.clg.data.interactor.PostInteractor
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.util.calculateFetchLimit
import javax.inject.Inject

private const val POST_ITEM_VIEW_MINIMUM_HEIGHT_DP = 140

class ChallengeViewModel @Inject constructor(
    private val challengeInteractor: ChallengeInteractor,
    private val postInteractor: PostInteractor
) : ViewModel() {

    val challengeLD = MediatorLiveData<Document<ChallengeParticipating>>()

    val postsDataProvider: DataProvider<VotedPost> by lazy {
        challengeInteractor.getPosts(
            calculateFetchLimit(POST_ITEM_VIEW_MINIMUM_HEIGHT_DP), challengeId, userId
        )
    }

    private var currentChallengeSource: LiveData<Document<ChallengeParticipating>>? = null
    private val challengeSourceObserver = Observer<Document<ChallengeParticipating>> {
        challengeLD.value = it
    }
    private lateinit var challengeId: String
    private lateinit var userId: String

    fun setChallenge(challengeId: String) {
        if (!this::challengeId.isInitialized || this.challengeId != challengeId) {
            this.challengeId = challengeId
        } else {
            return
        }

        refresh()
    }

    fun setUser(userId: String) {
        this.userId = userId
    }

    fun join() {
        challengeInteractor.join(challengeId, userId)
    }

    fun leave() {
        challengeInteractor.leave(challengeId, userId)
    }

    fun upPostRating(postId: String) {
        postInteractor.upRating(challengeId, postId, userId)
    }

    fun downPostRating(postId: String) {
        postInteractor.downRating(challengeId, postId, userId)
    }

    fun refresh() {
        currentChallengeSource?.also { challengeLD.removeSource(it) }
        currentChallengeSource = challengeInteractor.getChallenge(challengeId).also {
            challengeLD.addSource(it, challengeSourceObserver)
        }
    }

}