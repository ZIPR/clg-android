package com.zipr.clg.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.zipr.clg.R
import com.zipr.clg.data.entity.Location

class LocationAdapter(
    private val onClickLocation: (location: Location) -> Unit
) : SingleItemAdapter<LocationAdapter.LocationViewHolder>(R.layout.item_location) {

    private var location: Location? = null

    fun set(location: Location?) {
        this.location = location
        isVisible = location != null
    }

    override fun onCreateViewHolder(view: View) = LocationViewHolder(view).apply {
        locationTv.setOnClickListener { onClickLocation(location) }
    }

    override fun onBindViewHolder(holder: LocationViewHolder) = holder.bind(location!!)

    class LocationViewHolder(textView: View) : RecyclerView.ViewHolder(textView) {

        val locationTv = textView as TextView

        lateinit var location: Location
            private set

        fun bind(location: Location) {
            this.location = location
            locationTv.text = "${location.name}\n${location.address}"
        }

    }

}