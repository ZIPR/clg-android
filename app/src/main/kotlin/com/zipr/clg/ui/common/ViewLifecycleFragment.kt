package com.zipr.clg.ui.common

import android.arch.lifecycle.Lifecycle.Event
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View

open class ViewLifecycleFragment : Fragment() {
    
    val viewLifecycleOwner: LifecycleOwner by lazy { LifecycleOwner { lifecycleRegistry } }
    private val lifecycleRegistry = LifecycleRegistry(viewLifecycleOwner)
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    
        lifecycleRegistry.handleLifecycleEvent(Event.ON_CREATE)
    }

    /**
     *  Setting ON_START state to let LiveData post value immediately
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        lifecycleRegistry.handleLifecycleEvent(Event.ON_START)
    }
    
    override fun onResume() {
        super.onResume()
    
        lifecycleRegistry.handleLifecycleEvent(Event.ON_RESUME)
    }
    
    override fun onPause() {
        lifecycleRegistry.handleLifecycleEvent(Event.ON_PAUSE)
        
        super.onPause()
    }
    
    override fun onStop() {
        lifecycleRegistry.handleLifecycleEvent(Event.ON_STOP)
        
        super.onStop()
    }
    
    override fun onDestroyView() {
        lifecycleRegistry.handleLifecycleEvent(Event.ON_DESTROY)
        
        super.onDestroyView()
    }
    
}