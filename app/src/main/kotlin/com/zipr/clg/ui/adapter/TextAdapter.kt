package com.zipr.clg.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.zipr.clg.R
import com.zipr.clg.util.runOrPostIfComputingLayout

class TextAdapter : SingleItemAdapter<TextAdapter.TextViewHolder>(R.layout.item_text) {

    private var text: String? = null

    fun set(text: String?) {
        this.text = text
        if (isVisible) {
            recyclerView.runOrPostIfComputingLayout {
                notifyItemChanged(0)
            }
        }
    }
    
    override fun onCreateViewHolder(view: View) = TextViewHolder(view)
    
    override fun onBindViewHolder(holder: TextViewHolder) = holder.bind(text)

    class TextViewHolder(textView: View) : RecyclerView.ViewHolder(textView) {

        val textView = textView as TextView

        fun bind(text: String?) {
            textView.text = text
        }

    }

}