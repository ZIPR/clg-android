package com.zipr.clg.ui.fragment.userlist

import android.arch.lifecycle.ViewModel
import com.zipr.clg.data.entity.UserFollower
import com.zipr.clg.data.interactor.UserInteractor
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.util.calculateFetchLimit

private const val USER_ITEM_VIEW_HEIGHT_DP = 76

abstract class UserListViewModel<DP: DataProvider<UserFollower>> constructor(
    private val userInteractor: UserInteractor
) : ViewModel() {
    
    val dataProvider: DP by lazy {
        getDataProvider(calculateFetchLimit(USER_ITEM_VIEW_HEIGHT_DP))
    }
    
    abstract fun getDataProvider(fetchLimit: Long): DP
    
    fun follow(userId: String, followerId: String) {
        userInteractor.follow(userId, followerId)
    }
    
    fun unfollow(userId: String, followerId: String) {
        userInteractor.unfollow(userId, followerId)
    }
    
}