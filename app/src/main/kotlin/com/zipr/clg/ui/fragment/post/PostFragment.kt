package com.zipr.clg.ui.fragment.post

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.*
import android.view.*
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.zipr.clg.R
import com.zipr.clg.data.DataException
import com.zipr.clg.data.entity.Comment
import com.zipr.clg.data.entity.User
import com.zipr.clg.data.onExoPlayerError
import com.zipr.clg.data.paging.provider.DataProvider
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.common.AutoClearedValue
import com.zipr.clg.ui.common.LifecycleRegistrar
import com.zipr.clg.ui.common.PlayerViewModel
import com.zipr.clg.ui.common.ViewLifecycleFragment
import com.zipr.clg.ui.controller.*
import com.zipr.clg.ui.widget.ThumbnailPlayerView
import com.zipr.clg.util.*
import kotlinx.android.synthetic.main.fragment_post.*
import kotlinx.android.synthetic.main.state_layout.view.*
import javax.inject.Inject

private const val ARG_CHALLENGE_ID = "ARG_CHALLENGE_ID"
private const val ARG_CHALLENGE_TITLE = "ARG_CHALLENGE_TITLE"
private const val ARG_CHALLENGE_COLOR = "ARG_CHALLENGE_COLOR"
private const val ARG_CHALLENGE_DARK_COLOR = "ARG_CHALLENGE_DARK_COLOR"
private const val ARG_POST_ID = "ARG_POST_ID"

private const val BUNDLE_PLAYER_VIEW_TAG = "BUNDLE_PLAYER_VIEW_TAG"

@Injectable
class PostFragment : ViewLifecycleFragment(), Toolbar.OnMenuItemClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var navigationController: NavigationController
    @Inject lateinit var statusBarController: StatusBarController
    @Inject lateinit var bottomNavigationBarController: BottomNavigationBarController

    private val viewModelProvider by lazy { ViewModelProviders.of(this, viewModelFactory) }
    private val postViewModel by lazy { viewModelProvider.get(PostViewModel::class.java) }
    private val playerViewModel by lazy { viewModelProvider.get(PlayerViewModel::class.java) }

    private val lifecycleRegistrar by lazy { LifecycleRegistrar(viewLifecycleOwner) }

    private val currentPlayerView by lazy { AutoClearedValue<ThumbnailPlayerView>(viewLifecycleOwner) }

    private val postRvItemAnimator = DefaultItemAnimator().apply {
        supportsChangeAnimations = false
    }

    private var isPostRvAnimationsEnabled = true
        set(enable) {
            if (enable != field) {
                postRv.post {
                    postRv?.apply { itemAnimator = if (enable) postRvItemAnimator else null }
                }
                field = enable
            }
        }

    private lateinit var postAdapter: PostSegmentAdapter
    private lateinit var shareMenuItem: MenuItem

    private var playerViewTag: String? = null

    private var isPostRvScrollEnabled: Boolean? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_post, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val argChallengeId = arguments!!.getString(ARG_CHALLENGE_ID)
        val argChallengeTitle = arguments!!.getString(ARG_CHALLENGE_TITLE)
        val argChallengeColor = arguments!!.getInt(ARG_CHALLENGE_COLOR)
        val argChallengeDarkColor = arguments!!.getInt(ARG_CHALLENGE_DARK_COLOR)
        val argPostId = arguments!!.getString(ARG_POST_ID)

        savedInstanceState?.getString(BUNDLE_PLAYER_VIEW_TAG)?.also {
            playerViewTag = savedInstanceState.getString(BUNDLE_PLAYER_VIEW_TAG)
        }

        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE or
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        bottomNavigationBarController.isNavigationBarVisible = false
        statusBarController.setStatusBarColor(argChallengeDarkColor)

        toolbar.apply {
            title = argChallengeTitle
            setBackgroundColor(argChallengeColor)
            setNavigationIcon(R.drawable.ic_arrow_back)
            setNavigationOnClickListener { activity!!.onBackPressed() }
            inflateMenu(R.menu.share_menu)
            shareMenuItem = menu.findItem(R.id.action_share)
            setOnMenuItemClickListener(this@PostFragment)
        }

        stateLayout.button.apply {
            text = getString(R.string.retry)
            setOnClickListener { onRetry() }
        }
        sendCommentIb.apply {
            setOnClickListener { onSend() }
            setColorFilter(argChallengeColor)
        }

        State.apply { restoreState(savedInstanceState) }

        playerViewModel.addPlayerEventListener(
            viewLifecycleOwner,
            object : Player.DefaultEventListener() {

                override fun onPlayerError(error: ExoPlaybackException) {
                    setPlayerView(null, null)
                    this@PostFragment.onPlayerError(error)
                }
            }
        )

        postViewModel.setPost(argChallengeId, argPostId, firebaseUser!!.uid)

        if (!::postAdapter.isInitialized) {
            postAdapter = PostSegmentAdapter.create(
                lifecycleRegistrar = lifecycleRegistrar,
                commentProvider = postViewModel.commentsDataProvider,
                onBindPlayerViewTag = { playerView: ThumbnailPlayerView ->
                    playerView.setLifecycleOwner(this)
                    if (playerViewTag != null && playerView.tag == playerViewTag) {
                        setPlayerView(playerView, null)
                    }
                },
                onClickPlayerView = { player: ThumbnailPlayerView, uri: Uri ->
                    setPlayerView(player, uri)
                },
                onPostAuthorClick = {
                    it.author?.id?.also(navigationController::navigateToUserProfile)
                },
                onCommentAuthorClick = {
                    it.author?.id?.also(navigationController::navigateToUserProfile)
                },
                onPostLocationClick = {
                    openLocation(it)
                },
                onPostRatingUp = {
                    it.id?.also(postViewModel::upPostRating)
                },
                onPostRatingDown = {
                    it.id?.also(postViewModel::downPostRating)
                },
                onPostCommentsClick = {}
            )
        }

        postRv.apply {
            adapter = postAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        postViewModel.commentsDataProvider.apply {
            lifecycleRegistrar.registerImmediately(
                object : DataProvider.StatusListener {

                    override fun onStatusChange(
                        isFetching: Boolean,
                        isRefreshing: Boolean,
                        isFullResult: Boolean,
                        isEmpty: Boolean,
                        isError: Boolean
                    ) {
                        postAdapter.setCommentsStatus(isFullResult && isEmpty)
                        if (isFullResult && isPostRvScrollEnabled == null) {
                            postRv.addOnScrollListener(
                                object : RecyclerView.OnScrollListener() {

                                    override fun onScrolled(
                                        recyclerView: RecyclerView?,
                                        dx: Int,
                                        dy: Int
                                    ) {
                                        super.onScrolled(recyclerView, dx, dy)
                                        recyclerView?.also {
                                            isPostRvScrollEnabled = !it.canScrollVertically(1)
                                        }
                                    }

                                }
                            )
                        }
                    }

                },
                ::registerStatusListener,
                ::unregisterStatusListener
            ).registerImmediately(
                object : DataProvider.DataListener {

                    override fun onDataInserted(positionStart: Int, itemCount: Int) {
                        if (isPostRvScrollEnabled == true) {
                            postRv.scrollToPosition(postAdapter.itemCount - 1)
                        }
                    }

                    override fun onDataRemoved(positionStart: Int, itemCount: Int) {

                    }

                    override fun onDataChanged(positionStart: Int, itemCount: Int) {

                    }
                },
                ::registerDataListener,
                ::unregisterDataListener
            )
        }

        postViewModel.postLD.observe(viewLifecycleOwner, Observer {
            it?.data?.also {
                postAdapter.setPost(it)
                State.apply { setState(State.RESULT) }
            } ?: it?.exception?.also {
                onDataError(it)
                State.apply { setState(State.ERROR) }
            }
        })
    }

    override fun onStop() {
        super.onStop()

        if (isRemoving) {
            currentPlayerView.value?.player?.playWhenReady = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        activity!!.window.setDefaultSoftInputMode()
        bottomNavigationBarController.isNavigationBarVisible = true
        statusBarController.setStatusBarColor(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        currentPlayerView.value?.tag?.toString()?.also {
            outState.putString(BUNDLE_PLAYER_VIEW_TAG, it)
        }
    }

    override fun onMenuItemClick(item: MenuItem) = when (item.itemId) {
        R.id.action_share -> {
            postViewModel.postLD.value?.data?.post?.also {
                Intent.createChooser(
                    Intent()
                        .setAction(Intent.ACTION_SEND)
                        .putExtra(Intent.EXTRA_TEXT, it.toShareString())
                        .setType("text/plain"),
                    resources.getText(R.string.share_via)
                ).also {
                    navigationController.startActivity(it)
                }
            }
            true
        }

        else -> false
    }

    private fun setPlayerView(playerView: ThumbnailPlayerView?, uri: Uri?) {
        currentPlayerView.value?.apply {
            if (this != playerView) {
                player?.stop(true)
                player = null
            }
        }
        currentPlayerView.value = playerView

        playerView?.also {
            playerViewModel.startVideo(it, uri)
        }
    }

    private fun onPlayerError(e: ExoPlaybackException) {
        onExoPlayerError(context, e, getString(R.string.err_video))
    }

    private fun onRetry() {
        State.apply { setState(State.LOADING) }
        postViewModel.refresh()
    }

    private fun validateText(text: String) =
        (text.length >= resources.getInteger(R.integer.comment_text_length_min) &&
                text.length <= resources.getInteger(R.integer.comment_text_length_max))

    private fun onSend() {
        val commentText = commentEt.text.trim().toString()
        commentEt.setText(commentText)

        if (validateText(commentText)) {
            postViewModel.addComment(
                Comment(
                    author = User(id = firebaseUser?.uid),
                    contentText = commentText
                )
            )
            commentEt.text.clear()
        } else {
            showValidationToast(context, getString(R.string.err_comment_valid))
        }
    }

    private fun onDataError(e: DataException) {
        stateLayout.textView.text = when (e.type) {
            DataException.Type.NOT_FOUND ->
                getString(R.string.err_no_post)

            DataException.Type.CANCELLED ->
                getString(R.string.err_firestore_cancelled)

            DataException.Type.PERMISSION_DENIED ->
                getString(R.string.err_firestore_permission_denied)

            DataException.Type.UNAUTHENTICATED ->
                getString(R.string.err_firestore_unauthenticated)

            DataException.Type.UNAVAILABLE ->
                getString(R.string.err_firestore_unavailable)

            else -> e.message
        }
    }

    companion object {

        fun newInstance(
            challengeId: String,
            challengeTitle: String,
            challengeColor: Int,
            postId: String
        ) = PostFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_CHALLENGE_ID, challengeId)
                putString(ARG_CHALLENGE_TITLE, challengeTitle)
                putInt(ARG_CHALLENGE_COLOR, challengeColor)
                putInt(ARG_CHALLENGE_DARK_COLOR, darkenColor(challengeColor))
                putString(ARG_POST_ID, postId)
            }
        }

    }

    private enum class State(
        override val state: PostFragment.() -> Unit
    ) : StateAct<PostFragment> {
        RESULT({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.GONE
            postRv.visibility = View.VISIBLE
            inputCl.visibility = View.VISIBLE
            shareMenuItem.isVisible = true
            isPostRvAnimationsEnabled = true
        }),
        LOADING({
            stateLayout.textView.visibility = View.GONE
            stateLayout.button.visibility = View.GONE
            stateLayout.progressBar.visibility = View.VISIBLE
            postRv.visibility = View.INVISIBLE
            inputCl.visibility = View.INVISIBLE
            shareMenuItem.isVisible = false
            isPostRvAnimationsEnabled = false
        }),
        ERROR({
            stateLayout.textView.visibility = View.VISIBLE
            stateLayout.button.visibility = View.VISIBLE
            stateLayout.progressBar.visibility = View.GONE
            postRv.visibility = View.INVISIBLE
            inputCl.visibility = View.INVISIBLE
            shareMenuItem.isVisible = false
            isPostRvAnimationsEnabled = true
        });

        companion object : StateController<PostFragment, State>(
            State.LOADING,
            { state: String -> State.valueOf(state) }
        )

    }

}