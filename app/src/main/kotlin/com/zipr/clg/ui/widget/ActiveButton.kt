package com.zipr.clg.ui.widget

import android.content.Context
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import kotlin.properties.Delegates

private const val INACTIVE_COLOR_ID = R.color.grey_400

class ActiveButton : AppCompatButton {
    
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var activeColorFilter: ColorFilter? = null
    private val inactiveColorFilter = PorterDuffColorFilter(
        ClgApplication.getColor(INACTIVE_COLOR_ID),
        PorterDuff.Mode.SRC_ATOP
    )

    var isActive by Delegates.observable(true) { _, old, new ->
        if (old != new) {
            if (new) {
                background.colorFilter = activeColorFilter
            } else {
                activeColorFilter = background.colorFilter
                background.colorFilter = inactiveColorFilter
            }
        }
    }

}