package com.zipr.clg.ui.common

import android.arch.lifecycle.*
import android.net.Uri
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.FileDataSource
import com.google.android.exoplayer2.upstream.cache.*
import com.zipr.clg.ClgApplication
import javax.inject.Inject

private const val MAX_CACHE_SIZE = 100 * 1024 * 1024L
private const val MAX_FILE_SIZE = 5 * 1024 * 1024L

class PlayerViewModel @Inject constructor() : ViewModel() {

    private var _player: SimpleExoPlayer? = null
    private val player get() = _player
        ?: ClgApplication.getExoPlayer().also {
            _player = it
        }

    private var _cache: Cache? = null
    private val cache get() = _cache
        ?: SimpleCache(
            ClgApplication.getDefaultCacheDir(),
            LeastRecentlyUsedCacheEvictor(MAX_CACHE_SIZE)
        ).also {
            _cache = it
        }

    private var _dataSource: DataSource? = null
    private val dataSource get() = _dataSource
        ?: CacheDataSource(
            cache,
            ClgApplication.getDefaultDataSource().createDataSource(),
            FileDataSource(),
            CacheDataSink(cache, MAX_FILE_SIZE),
            CacheDataSource.FLAG_BLOCK_ON_CACHE or CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
            null
        ).also {
            _dataSource = it
        }

    init {
        ProcessLifecycleOwner.get().lifecycle.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun stopPlayer() {
                player.playWhenReady = false
            }

        })
    }

    fun startVideo(playerView: PlayerView, uri: Uri? = null) {
        val exoPlayer = player

        playerView.player = exoPlayer
        uri?.also {
            val mediaSource = ExtractorMediaSource.Factory { dataSource }
                    .createMediaSource(uri)
            exoPlayer.playWhenReady = true
            exoPlayer.prepare(mediaSource, true, true)
        }
    }

    fun addPlayerEventListener(lifecycleOwner: LifecycleOwner, listener: Player.EventListener) {
        val exoPlayer = player

        LifecycleRegistrar(lifecycleOwner).registerImmediately(
            listener,
            exoPlayer::addListener,
            exoPlayer::removeListener
        )
    }

    override fun onCleared() {
        _player?.also {
            it.release()
            _player = null
        }
        _dataSource?.also {
            it.close()
            _dataSource = null
        }
        _cache?.also {
            it.release()
            _cache = null
        }
    }
    
}