package com.zipr.clg.ui.adapter

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zipr.clg.R
import com.zipr.clg.data.entity.Video
import com.zipr.clg.ui.widget.ThumbnailPlayerView

class VideoAdapter(
    private val onBindPlayerViewTag: (playerView: ThumbnailPlayerView) -> Unit,
    private val onClickPlayerView: (ThumbnailPlayerView, Uri) -> Unit
) : RecyclerViewAdapter<VideoAdapter.VideoViewHolder>() {

    private var vids: List<Video>? = null

    fun set(vids: List<Video>?) {
        this.vids = vids
        dataItemCount = vids?.size ?: 0
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VideoViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
    ).apply {
        playerView.scrimView.setOnClickListener {
            onClickPlayerView(playerView, Uri.parse(videoUrl))
        }
    }
    
    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) =
        holder.apply { bind(vids!![position]) }.run {
            playerView.tag = "${this::class.simpleName}($position)-$videoUrl"
            onBindPlayerViewTag(playerView)
        }

    class VideoViewHolder(playerView: View) : RecyclerView.ViewHolder(playerView) {

        val playerView = playerView as ThumbnailPlayerView

        var videoUrl: String? = null
            private set

        fun bind(video: Video) {
            videoUrl = video.url
            video.thumbnail?.apply {
                if (aspectRatio != null && aspectRatio > 0) {
                    playerView.thumbnailView.aspectRatio = aspectRatio.toFloat()
                }
                playerView.thumbnailView.setImageURI(url)
            }
        }

    }

}