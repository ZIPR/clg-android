package com.zipr.clg.ui.common

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer

abstract class SyncMediatorLiveData<T> : MediatorLiveData<T>() {

    protected abstract val syncValue: T?

    private var syncCount = 0
    private val syncedSet = mutableSetOf<Any>()
    private var isSync = false

    fun <S : Any?> addSource(source: LiveData<S>) {
        syncCount++
        if (value != null) {
            super.setValue(null)
        }
        super.addSource(source, {
            syncedSet.add(source)
            setValueIfSync()
        })
    }

    override fun <S : Any?> addSource(source: LiveData<S>, onChanged: Observer<S>) {
        syncCount++
        if (value != null) {
            super.setValue(null)
        }
        super.addSource(source, {
            onChanged.onChanged(it)
            syncedSet.add(source)
            setValueIfSync()
        })
    }

    override fun <S : Any?> removeSource(toRemote: LiveData<S>) {
        super.removeSource(toRemote)
        syncCount--
        syncedSet.remove(toRemote)
        setValueIfSync()
    }

    fun sync() {
        isSync = true
        setValueIfSync()
    }

    override fun setValue(value: T) {
        throw UnsupportedOperationException("Setting value is unsupported")
    }

    override fun postValue(value: T) {
        throw UnsupportedOperationException("Posting value is unsupported")
    }

    private fun setValueIfSync() {
        if (isSync && syncedSet.size >= syncCount) {
            super.setValue(syncValue)
        }
    }
    
}