package com.zipr.clg.ui.adapter

import android.support.annotation.CallSuper
import android.support.v7.widget.RecyclerView
import com.zipr.clg.util.runOrPostIfComputingLayout
import java.lang.ref.WeakReference

abstract class RecyclerViewAdapter<V : RecyclerView.ViewHolder> : RecyclerView.Adapter<V>() {
    
    open val itemTypesCount: Int = 1
    
    private var _recyclerView: WeakReference<RecyclerView>? = null
    val recyclerView get() = _recyclerView?.get()
    
    protected var dataItemCount = 0
        set(value) {
            val oldValue = field
            field = value
            val diff = oldValue - value
            when {
                diff > 0 -> {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeChanged(0, value)
                        notifyItemRangeRemoved(value, diff)
                    }
                }
                
                diff < 0 -> {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeChanged(0, oldValue)
                        notifyItemRangeInserted(oldValue, -diff)
                    }
                }
                
                else -> {
                    recyclerView.runOrPostIfComputingLayout {
                        notifyItemRangeChanged(0, value)
                    }
                }
            }
        }
    
    override fun getItemCount() = dataItemCount
    
    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        
        this._recyclerView = WeakReference(recyclerView)
    }
    
    @CallSuper
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        
        this._recyclerView?.clear()
    }
    
}