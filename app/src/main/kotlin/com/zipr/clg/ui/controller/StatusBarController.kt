package com.zipr.clg.ui.controller

import android.support.annotation.ColorInt

interface StatusBarController {
    
    val statusBarHeight: Int

    fun setStatusBarColor(@ColorInt color: Int?)

}