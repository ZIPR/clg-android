package com.zipr.clg.ui

import android.app.SearchManager
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat.requestApplyInsets
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.zipr.clg.ClgApplication
import com.zipr.clg.R
import com.zipr.clg.di.Injectable
import com.zipr.clg.ui.controller.*
import com.zipr.clg.ui.fragment.auth.AuthFragment
import com.zipr.clg.ui.fragment.challenges.ChallengesFragment
import com.zipr.clg.ui.fragment.profile.ProfileFragment
import com.zipr.clg.ui.fragment.userlist.people.PeopleFragment
import com.zipr.clg.ui.fragment.userprofile.UserProfileFragment
import com.zipr.clg.util.clearBackStack
import com.zipr.clg.util.firebaseUser
import com.zipr.clg.util.topFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@Injectable
class MainActivity : AppCompatActivity(),
    HasSupportFragmentInjector,
    NavigationController,
    StatusBarController,
    BottomNavigationBarController,
    BottomNavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemReselectedListener {

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private val currentFragment get() = supportFragmentManager.findFragmentById(R.id.mainContainerFl)

    private val navigationMap = mapOf(
        R.id.action_profile to {
            ProfileFragment.newInstance()
        },
        R.id.action_challenges to {
            ChallengesFragment.newInstance()
        },
        R.id.action_people to {
            PeopleFragment.newInstance()
        },
        R.id.action_settings to ::Fragment
    )

    override val statusBarHeight get() = Rect().apply {
        window.decorView.getWindowVisibleDisplayFrame(this)
    }.top

    override var isNavigationBarVisible: Boolean
        get() = bottomNavigation.isVisible
        set(value) {
            bottomNavigation.isVisible = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)

        State.apply { restoreState(savedInstanceState) }

        mainContainerFl.setOnHierarchyChangeListener(object : ViewGroup.OnHierarchyChangeListener {

            override fun onChildViewAdded(parent: View, child: View) {
                requestApplyInsets(child)
            }

            override fun onChildViewRemoved(parent: View, child: View) {

            }

        })

        bottomNavigation.setOnNavigationItemSelectedListener(this)
        bottomNavigation.setOnNavigationItemReselectedListener(this)
    }

    override fun onNewIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            (currentFragment as? SearchQueryHandler)?.apply {
                onSearchQuery(intent.getStringExtra(SearchManager.QUERY))
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        State.apply { saveState(outState) }
    }

    private fun finishCurrentFragment(continueFinish: () -> Unit) {
        (currentFragment as? FinishHandler)?.onFinish(continueFinish) ?: continueFinish()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        navigateToNavFragment(
            navigationMap[item.itemId]!!,
            item.title.toString()
        )
        return true
    }

    override fun onNavigationItemReselected(item: MenuItem) {
        if (supportFragmentManager.topFragment?.name == item.title) {
            (currentFragment as? RefreshStateHandler)?.onRefreshState()
        } else {
            onNavigationItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if ((currentFragment as? BackPressHandler)?.handleBackPress() == true) {
            return
        }
        finishCurrentFragment(::navigateBack)
    }

    override fun navigateBack() {
        if (supportFragmentManager.backStackEntryCount > 1) supportFragmentManager.popBackStack()
        else finish()
    }

    override fun navigateToFragment(fragment: Fragment, name: String?) {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                0, R.anim.fade_out,
                0, R.anim.fade_out
            )
            .replace(R.id.mainContainerFl, fragment)
            .addToBackStack(name)
            .commit()
    }

    override fun replaceWithFragment(fragment: Fragment, name: String?) {
        supportFragmentManager.popBackStack()
        navigateToFragment(fragment, name)
    }

    private fun navigateToNavFragment(factory: () -> Fragment, name: String) {
        finishCurrentFragment {
            if (!supportFragmentManager.popBackStackImmediate(name, 0)) {
                navigateToFragment(factory(), name)
            }
        }
    }

    override fun navigateToAuth() {
        supportFragmentManager.clearBackStack()
        State.apply { setState(State.AUTH) }
    }

    override fun navigateToMain() {
        supportFragmentManager.clearBackStack()
        State.apply { setState(State.INIT) }
    }

    override fun navigateToUserProfile(userId: String) {
        navigateToFragment(
            if (userId == firebaseUser?.uid) {
                ProfileFragment.newInstance()
            } else {
                UserProfileFragment.newInstance(userId)
            }
        )
    }

    override fun setStatusBarColor(@ColorInt color: Int?) {
        window.statusBarColor = color ?: ClgApplication.getColor(R.color.colorPrimaryDark)
    }

    private enum class State(override val state: MainActivity.() -> Unit) : StateAct<MainActivity> {
        START({
            setState(if (firebaseUser != null) State.INIT else State.AUTH)
        }),
        AUTH({
            bottomNavigation.visibility = View.GONE
            navigateToFragment(AuthFragment.newInstance())
        }),
        INIT({
            val defaultItem = bottomNavigation.menu.getItem(0)
            bottomNavigation.visibility = View.VISIBLE
            bottomNavigation.selectedItemId = defaultItem.itemId
            onNavigationItemSelected(defaultItem)
            setState(State.IDLE)
        }),
        IDLE({});

        companion object : StateController<MainActivity, State>(
            START,
            { state: String -> State.valueOf(state) }
        )

    }

}