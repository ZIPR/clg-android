package com.zipr.clg.ui.common

import android.arch.lifecycle.LiveData
import android.graphics.Bitmap
import com.facebook.common.executors.CallerThreadExecutor
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.datasource.DataSubscriber
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.image.CloseableBitmap
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequest
import com.zipr.clg.data.AsyncResult
import com.zipr.clg.util.aspectRatio
import kotlinx.coroutines.experimental.CancellationException

data class SourceImage(val bitmap: Bitmap, val aspectRatio: Float)

class SourceImageLD(
    val imageRequest: ImageRequest
) : LiveData<AsyncResult<SourceImage, Throwable>>() {

    private var dataSource: DataSource<CloseableReference<CloseableImage>>? = null

    private val dataSubscriber = object : DataSubscriber<CloseableReference<CloseableImage>> {

        override fun onNewResult(dataSource: DataSource<CloseableReference<CloseableImage>>) {
            if (!dataSource.isFinished) {
                return
            }

            val sourceImage = (dataSource.result?.get() as? CloseableBitmap)?.run {
                SourceImage(underlyingBitmap, aspectRatio)
            } ?: run {
                CloseableReference.closeSafely(dataSource.result)
                dataSource.close()
                null
            }
            postValue(
                AsyncResult(sourceImage, if (sourceImage == null) IllegalArgumentException() else null)
            )
        }

        override fun onFailure(dataSource: DataSource<CloseableReference<CloseableImage>>) {
            dataSource.close()
            postValue(AsyncResult(error = dataSource.failureCause))
        }

        override fun onCancellation(dataSource: DataSource<CloseableReference<CloseableImage>>?) {
            postValue(AsyncResult(error = CancellationException()))
        }

        override fun onProgressUpdate(dataSource: DataSource<CloseableReference<CloseableImage>>?) {}

    }

    override fun onActive() {
        dataSource = Fresco.getImagePipeline()
            .fetchDecodedImage(imageRequest, null, ImageRequest.RequestLevel.FULL_FETCH).apply {
                subscribe(dataSubscriber, CallerThreadExecutor.getInstance())
            }
    }

    override fun onInactive() {
        dataSource?.also {
            CloseableReference.closeSafely(it.result)
            it.close()
            dataSource = null
            postValue(null)
        }
    }

}