package com.zipr.clg.di.module

import com.zipr.clg.ui.MainActivity
import com.zipr.clg.ui.controller.BottomNavigationBarController
import com.zipr.clg.ui.controller.NavigationController
import com.zipr.clg.ui.controller.StatusBarController
import com.zipr.clg.ui.fragment.auth.AuthFragment
import com.zipr.clg.ui.fragment.challenge.ChallengeFragment
import com.zipr.clg.ui.fragment.challengecreation.ChallengeCreationFragment
import com.zipr.clg.ui.fragment.challenges.ChallengesFragment
import com.zipr.clg.ui.fragment.post.PostFragment
import com.zipr.clg.ui.fragment.postcreation.PostCreationFragment
import com.zipr.clg.ui.fragment.profile.ProfileFragment
import com.zipr.clg.ui.fragment.reg.RegFragment
import com.zipr.clg.ui.fragment.userlist.followers.FollowersFragment
import com.zipr.clg.ui.fragment.userlist.following.FollowingFragment
import com.zipr.clg.ui.fragment.userlist.participants.ParticipantsFragment
import com.zipr.clg.ui.fragment.userlist.people.PeopleFragment
import com.zipr.clg.ui.fragment.userprofile.UserProfileFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersMainModule {

    @Binds
    abstract fun bindNavigationController(activity: MainActivity): NavigationController

    @Binds
    abstract fun bindStatusBarController(activity: MainActivity): StatusBarController

    @Binds
    abstract fun bindBottomNavigationBarController(activity: MainActivity): BottomNavigationBarController

    @ContributesAndroidInjector
    abstract fun contributeAuthFragment(): AuthFragment

    @ContributesAndroidInjector
    abstract fun contributeRegFragment(): RegFragment

    @ContributesAndroidInjector
    abstract fun contributeChallengeFragment(): ChallengeFragment

    @ContributesAndroidInjector
    abstract fun contributeChallengesFragment(): ChallengesFragment

    @ContributesAndroidInjector
    abstract fun contributeChallengeCreationFragment(): ChallengeCreationFragment

    @ContributesAndroidInjector
    abstract fun contributePostCreationFragment(): PostCreationFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeUserProfileFragment(): UserProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeFollowingFragment(): FollowingFragment

    @ContributesAndroidInjector
    abstract fun contributeFollowersFragment(): FollowersFragment

    @ContributesAndroidInjector
    abstract fun contributePeopleFragment(): PeopleFragment

    @ContributesAndroidInjector
    abstract fun contributeParticipantsFragment(): ParticipantsFragment

    @ContributesAndroidInjector
    abstract fun contributePostFragment(): PostFragment

}