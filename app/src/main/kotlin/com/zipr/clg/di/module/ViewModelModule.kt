package com.zipr.clg.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.zipr.clg.di.ViewModelKey
import com.zipr.clg.ui.common.PlayerViewModel
import com.zipr.clg.ui.common.ViewModelFactory
import com.zipr.clg.ui.fragment.auth.AuthViewModel
import com.zipr.clg.ui.fragment.challenge.ChallengeViewModel
import com.zipr.clg.ui.fragment.challengecreation.ChallengeCreationViewModel
import com.zipr.clg.ui.fragment.challenges.ChallengesViewModel
import com.zipr.clg.ui.fragment.post.PostViewModel
import com.zipr.clg.ui.fragment.postcreation.PostCreationViewModel
import com.zipr.clg.ui.fragment.profile.ProfileViewModel
import com.zipr.clg.ui.fragment.reg.RegViewModel
import com.zipr.clg.ui.fragment.userlist.followers.FollowersViewModel
import com.zipr.clg.ui.fragment.userlist.following.FollowingViewModel
import com.zipr.clg.ui.fragment.userlist.participants.ParticipantsViewModel
import com.zipr.clg.ui.fragment.userlist.people.PeopleViewModel
import com.zipr.clg.ui.fragment.userprofile.UserProfileViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds @IntoMap @ViewModelKey(PlayerViewModel::class)
    abstract fun bindPlayerViewModel(playerViewModel: PlayerViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(AuthViewModel::class)
    abstract fun bindAuthViewModel(authViewModel: AuthViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(RegViewModel::class)
    abstract fun bindRegViewModel(regViewModel: RegViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(UserProfileViewModel::class)
    abstract fun bindUserProfileViewModel(userProfileViewModel: UserProfileViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(FollowingViewModel::class)
    abstract fun bindFollowingViewModel(followingViewModel: FollowingViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(FollowersViewModel::class)
    abstract fun bindFollowersViewModel(followersViewModel: FollowersViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(PeopleViewModel::class)
    abstract fun bindPeopleViewModel(peopleViewModel: PeopleViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(ChallengeViewModel::class)
    abstract fun bindChallengeViewModel(challengeViewModel: ChallengeViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(ChallengesViewModel::class)
    abstract fun bindChallengesViewModel(challengesViewModel: ChallengesViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(ChallengeCreationViewModel::class)
    abstract fun bindChallengeCreationViewModel(challengeCreationViewModel: ChallengeCreationViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(PostCreationViewModel::class)
    abstract fun bindPostCreationViewModel(postCreationViewModel: PostCreationViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(ParticipantsViewModel::class)
    abstract fun bindParticipantsViewModel(participantsViewModel: ParticipantsViewModel): ViewModel

    @Binds @IntoMap @ViewModelKey(PostViewModel::class)
    abstract fun bindPostViewModel(postViewModel: PostViewModel): ViewModel

}