package com.zipr.clg.di.module

import com.zipr.clg.data.interactor.ChallengeInteractor
import com.zipr.clg.data.interactor.PostInteractor
import com.zipr.clg.data.interactor.UserInteractor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InteractorModule {

    @Provides @Singleton
    fun provideUserInteractor(): UserInteractor = UserInteractor()

    @Provides @Singleton
    fun provideChallengeInteractor(): ChallengeInteractor = ChallengeInteractor()
    
    @Provides @Singleton
    fun providePostInteractor(): PostInteractor = PostInteractor()
    
}