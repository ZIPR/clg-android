package com.zipr.clg.di.module

import com.zipr.clg.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersMainModule::class])
    abstract fun contributeMainActivity(): MainActivity

}