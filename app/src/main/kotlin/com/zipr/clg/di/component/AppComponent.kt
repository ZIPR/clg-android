package com.zipr.clg.di.component

import android.app.Application
import com.zipr.clg.ClgApplication
import com.zipr.clg.di.module.InteractorModule
import com.zipr.clg.di.module.MainActivityModule
import com.zipr.clg.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    InteractorModule::class,
    ViewModelModule::class,
    MainActivityModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(application: ClgApplication)

}