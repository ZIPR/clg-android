package com.zipr.clg.di

/**
 * Marks an activity / fragment injectable.
 */
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Injectable

fun isInjectable(obj: Any) = obj::class.annotations.any { it is Injectable }