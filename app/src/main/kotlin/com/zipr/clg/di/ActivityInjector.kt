package com.zipr.clg.di

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

object ActivityInjector : Application.ActivityLifecycleCallbacks {
    
    private val fragmentLifecycleCallbacks = object : FragmentManager.FragmentLifecycleCallbacks() {
        
        override fun onFragmentPreAttached(fm: FragmentManager, f: Fragment, context: Context) {
            if (isInjectable(f)) {
                AndroidSupportInjection.inject(f)
            }
        }
        
    }
    
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (isInjectable(activity)) {
            AndroidInjection.inject(activity)
        }
        
        if (activity is FragmentActivity && activity is HasSupportFragmentInjector) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(
                fragmentLifecycleCallbacks,
                true
            )
        }
    }
    
    override fun onActivityStarted(activity: Activity) {}
    
    override fun onActivityResumed(activity: Activity) {}
    
    override fun onActivityPaused(activity: Activity) {}
    
    override fun onActivityStopped(activity: Activity) {}
    
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    
    override fun onActivityDestroyed(activity: Activity) {}
    
}