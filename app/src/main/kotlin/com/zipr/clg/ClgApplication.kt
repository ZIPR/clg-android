package com.zipr.clg

import android.app.Activity
import android.app.Application
import android.app.SearchManager
import android.app.SearchableInfo
import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.IntegerRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v4.os.ConfigurationCompat
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.LogStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.squareup.leakcanary.LeakCanary
import com.zipr.clg.data.MAX_UPLOAD_RETRY_TIME
import com.zipr.clg.data.firebaseStorage
import com.zipr.clg.di.ActivityInjector
import com.zipr.clg.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.fabric.sdk.android.Fabric
import java.io.File
import java.text.DateFormat
import javax.inject.Inject

private const val RESOURCE_NAME_STATUS_BAR_HEIGHT = "status_bar_height"
private const val RESOURCE_TYPE_DIMEN = "dimen"
private const val RESOURCE_PACKAGE_ANDROID = "android"

class ClgApplication : Application(), HasActivityInjector {

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    init {
        ClgApplication.instance = this
    }

    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)

        Logger.addLogAdapter(AndroidLogAdapter(
            PrettyFormatStrategy.newBuilder()
                //.showThreadInfo(false)    // (Optional) Whether to show thread info or not. Default true
                //.methodCount(10)          // (Optional) How many method line to show. Default 2
                //.methodOffset(1)          // (Optional) Hides internal method calls up to offset. Default 5
                .logStrategy(object : LogStrategy {

                    override fun log(priority: Int, tag: String?, message: String) {
                        Log.println(priority, tag(tag), message)
                    }

                    private fun tag(tag: String?) = "${if (isSlash) "/" else "\\"}$tag"
                    private var isSlash = false
                        get() {
                            field = !field
                            return field
                        }

                })                           // (Optional) Changes the log strategy to print out. Default LogCat
                .tag("LOGGER")              // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build()
        ))

        Fabric.with(this, Crashlytics())

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
        registerActivityLifecycleCallbacks(ActivityInjector)

        firebaseStorage.maxUploadRetryTimeMillis = MAX_UPLOAD_RETRY_TIME

        Fresco.initialize(
            this,
            ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build()
        )
    }

    companion object {

        private lateinit var instance: ClgApplication

        val dateFormat: DateFormat by lazy { android.text.format.DateFormat.getDateFormat(instance) }
        val timeFormat: DateFormat by lazy { android.text.format.DateFormat.getTimeFormat(instance) }

        fun getColor(@ColorRes colorId: Int): Int = ContextCompat.getColor(instance, colorId)

        fun getDrawable(@DrawableRes drawableId: Int): Drawable = ContextCompat.getDrawable(instance, drawableId)!!

        fun getInteger(@IntegerRes integerId: Int): Int = instance.resources.getInteger(integerId)

        fun getString(@StringRes stringId: Int): String = instance.getString(stringId)

        fun getString(@StringRes stringId: Int, vararg format: Any): String = String.format(
            ConfigurationCompat.getLocales(instance.resources.configuration)[0],
            getString(stringId),
            *format
        )

        fun getSearchableInfo(activity: Activity): SearchableInfo =
            (instance.getSystemService(Context.SEARCH_SERVICE) as SearchManager)
                .getSearchableInfo(activity.componentName)

        fun getExoPlayer(): SimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(
            DefaultRenderersFactory(instance),
            DefaultTrackSelector(AdaptiveTrackSelection.Factory(DefaultBandwidthMeter())),
            DefaultLoadControl()
        )

        fun getDefaultCacheDir() = File(instance.cacheDir, "media")

        fun getDefaultDataSource() = DefaultBandwidthMeter().let {
            DefaultDataSourceFactory(
                instance,
                it,
                DefaultHttpDataSourceFactory(
                    Util.getUserAgent(instance, getString(R.string.app_name)),
                    it
                )
            )
        }

    }

}